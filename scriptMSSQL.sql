USE [ComEdb]
GO
/****** Object:  Table [dbo].[company]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[company](
	[idCompany] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[isActive] [bit] NOT NULL,
	[deletedAt] [datetime] NULL,
 CONSTRAINT [PK_company] PRIMARY KEY CLUSTERED 
(
	[idCompany] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[branch]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[branch](
	[idBranch] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[createdAt] [datetime] NULL,
	[deletedAt] [datetime] NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_branch] PRIMARY KEY CLUSTERED 
(
	[idBranch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[role]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[role](
	[idRole] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
 CONSTRAINT [PK_role] PRIMARY KEY CLUSTERED 
(
	[idRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[organization]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[organization](
	[idOrganization] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[logo] [varchar](255) NULL,
	[address] [varchar](255) NULL,
 CONSTRAINT [PK_organization_idOrganization] PRIMARY KEY CLUSTERED 
(
	[idOrganization] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[service_type]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[service_type](
	[idServiceType] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[startTime] [datetime] NOT NULL,
	[endTime] [datetime] NOT NULL,
	[price] [float] NOT NULL,
 CONSTRAINT [PK_service_type_idServiceType] PRIMARY KEY CLUSTERED 
(
	[idServiceType] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[work_unit]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[work_unit](
	[idWorkUnit] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NULL,
	[isActive] [bit] NOT NULL,
	[deletedAt] [datetime] NULL,
 CONSTRAINT [PK_work_unit_idWorkUnit] PRIMARY KEY CLUSTERED 
(
	[idWorkUnit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[idUser] [int] IDENTITY(1,1) NOT NULL,
	[dni] [char](8) NOT NULL,
	[password] [varchar](255) NULL,
	[name] [varchar](100) NULL,
	[fatherLastName] [varchar](100) NULL,
	[motherLastName] [varchar](100) NULL,
	[isActive] [bit] NOT NULL,
	[deletedAt] [datetime] NULL,
	[idBranch] [int] NULL,
	[idRole] [int] NOT NULL,
 CONSTRAINT [PK_user_idUser] PRIMARY KEY CLUSTERED 
(
	[idUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[supplies]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[supplies](
	[idSupplies] [int] IDENTITY(1,1) NOT NULL,
	[name] [nchar](100) NULL,
	[stock] [int] NULL,
	[weight] [float] NOT NULL,
	[price] [float] NULL,
	[idBranch] [int] NOT NULL,
	[isActive] [bit] NULL,
	[minStock] [float] NULL,
	[minPrice] [float] NULL,
 CONSTRAINT [PK_Supplies] PRIMARY KEY CLUSTERED 
(
	[idSupplies] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[product](
	[idProduct] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[price] [float] NOT NULL,
	[grantedPrice] [float] NOT NULL,
	[idServiceType] [int] NULL,
	[currentStock] [int] NULL,
	[minimunStock] [int] NULL,
	[withStock] [char](1) NULL,
	[isActive] [bit] NOT NULL,
	[idBranch] [int] NULL,
 CONSTRAINT [PK_product_idProduct] PRIMARY KEY CLUSTERED 
(
	[idProduct] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[customer]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[customer](
	[idCustomer] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](100) NOT NULL,
	[fatherLastname] [varchar](100) NOT NULL,
	[motherLastname] [varchar](100) NOT NULL,
	[dni] [char](10) NOT NULL,
	[isActive] [bit] NOT NULL,
	[activeDate] [date] NOT NULL,
	[hasGrant] [bit] NOT NULL,
	[idWorkUnit] [int] NULL,
	[deletedAt] [datetime] NULL,
	[photo] [varchar](255) NULL,
	[idCompany] [int] NOT NULL,
 CONSTRAINT [PK_customer_idCustomer] PRIMARY KEY CLUSTERED 
(
	[idCustomer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[product_work_unit]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product_work_unit](
	[idProductWorkUnit] [int] IDENTITY(1,1) NOT NULL,
	[idProduct] [int] NOT NULL,
	[idWorkUnit] [int] NOT NULL,
 CONSTRAINT [PK_product_work_unit_idProductWorkUnit] PRIMARY KEY CLUSTERED 
(
	[idProductWorkUnit] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pos]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pos](
	[idPos] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[isActive] [bit] NOT NULL,
	[deletedAt] [datetime] NULL,
	[idUser] [int] NOT NULL,
 CONSTRAINT [PK_pos_idPos] PRIMARY KEY CLUSTERED 
(
	[idPos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[attendance]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[attendance](
	[idAttendance] [int] IDENTITY(1,1) NOT NULL,
	[idPos] [int] NOT NULL,
	[startAt] [datetime] NOT NULL,
	[endAt] [datetime] NULL,
	[startCash] [float] NOT NULL,
	[currentCash] [float] NOT NULL,
 CONSTRAINT [PK_attendance_idAttendance] PRIMARY KEY CLUSTERED 
(
	[idAttendance] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sale]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sale](
	[idSale] [int] IDENTITY(1,1) NOT NULL,
	[idCustomer] [int] NOT NULL,
	[totalGranted] [float] NULL,
	[totalPrice] [float] NULL,
	[cash] [float] NULL,
	[accountPayment] [float] NULL,
	[change] [float] NULL,
	[saleOnCredit] [float] NULL,
	[createdAt] [datetime] NOT NULL,
	[deletedAt] [datetime] NULL,
	[isPaid] [char](1) NULL,
	[idAttendance] [int] NULL,
	[idServiceType] [int] NULL,
 CONSTRAINT [PK_sale_idSale] PRIMARY KEY CLUSTERED 
(
	[idSale] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sale_detail]    Script Date: 02/02/2020 16:52:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sale_detail](
	[idSaleDetail] [int] IDENTITY(1,1) NOT NULL,
	[idSale] [int] NOT NULL,
	[idProduct] [int] NOT NULL,
	[quantity] [int] NOT NULL,
	[totalPrice] [float] NOT NULL,
	[totalGranted] [float] NOT NULL,
 CONSTRAINT [PK_sale_detail_idSaleDetail] PRIMARY KEY CLUSTERED 
(
	[idSaleDetail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__attendanc__endAt__1273C1CD]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[attendance] ADD  CONSTRAINT [DF__attendanc__endAt__1273C1CD]  DEFAULT (NULL) FOR [endAt]
GO
/****** Object:  Default [DF__customer__isActi__1367E606]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF__customer__isActi__1367E606]  DEFAULT (0x01) FOR [isActive]
GO
/****** Object:  Default [DF__customer__hasGra__145C0A3F]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF__customer__hasGra__145C0A3F]  DEFAULT (0x31) FOR [hasGrant]
GO
/****** Object:  Default [DF__customer__idWork__15502E78]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF__customer__idWork__15502E78]  DEFAULT (NULL) FOR [idWorkUnit]
GO
/****** Object:  Default [DF__customer__delete__164452B1]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF__customer__delete__164452B1]  DEFAULT (NULL) FOR [deletedAt]
GO
/****** Object:  Default [DF__customer__photo__173876EA]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer] ADD  CONSTRAINT [DF__customer__photo__173876EA]  DEFAULT (NULL) FOR [photo]
GO
/****** Object:  Default [DF__organizati__logo__182C9B23]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[organization] ADD  CONSTRAINT [DF__organizati__logo__182C9B23]  DEFAULT (NULL) FOR [logo]
GO
/****** Object:  Default [DF__organizat__addre__1920BF5C]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[organization] ADD  CONSTRAINT [DF__organizat__addre__1920BF5C]  DEFAULT (NULL) FOR [address]
GO
/****** Object:  Default [DF__product__idServi__1A14E395]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF__product__idServi__1A14E395]  DEFAULT (NULL) FOR [idServiceType]
GO
/****** Object:  Default [DF__product__current__1B0907CE]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF__product__current__1B0907CE]  DEFAULT (NULL) FOR [currentStock]
GO
/****** Object:  Default [DF__product__minimun__1BFD2C07]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product] ADD  CONSTRAINT [DF__product__minimun__1BFD2C07]  DEFAULT (NULL) FOR [minimunStock]
GO
/****** Object:  Default [DF__sale__totalGrant__1CF15040]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__totalGrant__1CF15040]  DEFAULT (NULL) FOR [totalGranted]
GO
/****** Object:  Default [DF__sale__totalPrice__1DE57479]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__totalPrice__1DE57479]  DEFAULT (NULL) FOR [totalPrice]
GO
/****** Object:  Default [DF__sale__cash__1ED998B2]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__cash__1ED998B2]  DEFAULT (NULL) FOR [cash]
GO
/****** Object:  Default [DF__sale__accountPay__1FCDBCEB]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__accountPay__1FCDBCEB]  DEFAULT (NULL) FOR [accountPayment]
GO
/****** Object:  Default [DF__sale__change__20C1E124]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__change__20C1E124]  DEFAULT (NULL) FOR [change]
GO
/****** Object:  Default [DF__sale__saleOnCred__21B6055D]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__saleOnCred__21B6055D]  DEFAULT (NULL) FOR [saleOnCredit]
GO
/****** Object:  Default [DF__sale__deletedAt__22AA2996]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale] ADD  CONSTRAINT [DF__sale__deletedAt__22AA2996]  DEFAULT (NULL) FOR [deletedAt]
GO
/****** Object:  Default [DF__user__password__239E4DCF]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF__user__password__239E4DCF]  DEFAULT (NULL) FOR [password]
GO
/****** Object:  Default [DF__user__name__24927208]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF__user__name__24927208]  DEFAULT (NULL) FOR [name]
GO
/****** Object:  Default [DF__user__fatherLast__25869641]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF__user__fatherLast__25869641]  DEFAULT (NULL) FOR [fatherLastName]
GO
/****** Object:  Default [DF__user__motherLast__267ABA7A]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF__user__motherLast__267ABA7A]  DEFAULT (NULL) FOR [motherLastName]
GO
/****** Object:  Default [DF__user__isActive__286302EC]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF__user__isActive__286302EC]  DEFAULT (0x01) FOR [isActive]
GO
/****** Object:  Default [DF__user__deletedAt__29572725]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF__user__deletedAt__29572725]  DEFAULT (NULL) FOR [deletedAt]
GO
/****** Object:  Default [DF__work_unit__name__2A4B4B5E]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[work_unit] ADD  CONSTRAINT [DF__work_unit__name__2A4B4B5E]  DEFAULT (NULL) FOR [name]
GO
/****** Object:  ForeignKey [attendance$attendance_ibfk_2]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[attendance]  WITH CHECK ADD  CONSTRAINT [attendance$attendance_ibfk_2] FOREIGN KEY([idPos])
REFERENCES [dbo].[pos] ([idPos])
GO
ALTER TABLE [dbo].[attendance] CHECK CONSTRAINT [attendance$attendance_ibfk_2]
GO
/****** Object:  ForeignKey [customer$customer_ibfk_1]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [customer$customer_ibfk_1] FOREIGN KEY([idWorkUnit])
REFERENCES [dbo].[work_unit] ([idWorkUnit])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [customer$customer_ibfk_1]
GO
/****** Object:  ForeignKey [FK_customer_company]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[customer]  WITH CHECK ADD  CONSTRAINT [FK_customer_company] FOREIGN KEY([idCompany])
REFERENCES [dbo].[company] ([idCompany])
GO
ALTER TABLE [dbo].[customer] CHECK CONSTRAINT [FK_customer_company]
GO
/****** Object:  ForeignKey [FK_pos_user]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[pos]  WITH CHECK ADD  CONSTRAINT [FK_pos_user] FOREIGN KEY([idUser])
REFERENCES [dbo].[user] ([idUser])
GO
ALTER TABLE [dbo].[pos] CHECK CONSTRAINT [FK_pos_user]
GO
/****** Object:  ForeignKey [FK_product_branch]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_branch] FOREIGN KEY([idBranch])
REFERENCES [dbo].[branch] ([idBranch])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_branch]
GO
/****** Object:  ForeignKey [FK_product_service_type]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product]  WITH CHECK ADD  CONSTRAINT [FK_product_service_type] FOREIGN KEY([idServiceType])
REFERENCES [dbo].[service_type] ([idServiceType])
GO
ALTER TABLE [dbo].[product] CHECK CONSTRAINT [FK_product_service_type]
GO
/****** Object:  ForeignKey [product_work_unit$product_work_unit_ibfk_1]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product_work_unit]  WITH CHECK ADD  CONSTRAINT [product_work_unit$product_work_unit_ibfk_1] FOREIGN KEY([idProduct])
REFERENCES [dbo].[product] ([idProduct])
GO
ALTER TABLE [dbo].[product_work_unit] CHECK CONSTRAINT [product_work_unit$product_work_unit_ibfk_1]
GO
/****** Object:  ForeignKey [product_work_unit$product_work_unit_ibfk_2]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[product_work_unit]  WITH CHECK ADD  CONSTRAINT [product_work_unit$product_work_unit_ibfk_2] FOREIGN KEY([idWorkUnit])
REFERENCES [dbo].[work_unit] ([idWorkUnit])
GO
ALTER TABLE [dbo].[product_work_unit] CHECK CONSTRAINT [product_work_unit$product_work_unit_ibfk_2]
GO
/****** Object:  ForeignKey [FK_sale_attendance]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale]  WITH CHECK ADD  CONSTRAINT [FK_sale_attendance] FOREIGN KEY([idAttendance])
REFERENCES [dbo].[attendance] ([idAttendance])
GO
ALTER TABLE [dbo].[sale] CHECK CONSTRAINT [FK_sale_attendance]
GO
/****** Object:  ForeignKey [FK_sale_service_type]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale]  WITH CHECK ADD  CONSTRAINT [FK_sale_service_type] FOREIGN KEY([idServiceType])
REFERENCES [dbo].[service_type] ([idServiceType])
GO
ALTER TABLE [dbo].[sale] CHECK CONSTRAINT [FK_sale_service_type]
GO
/****** Object:  ForeignKey [sale$sale_ibfk_1]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale]  WITH CHECK ADD  CONSTRAINT [sale$sale_ibfk_1] FOREIGN KEY([idCustomer])
REFERENCES [dbo].[customer] ([idCustomer])
GO
ALTER TABLE [dbo].[sale] CHECK CONSTRAINT [sale$sale_ibfk_1]
GO
/****** Object:  ForeignKey [sale_detail$sale_detail_ibfk_1]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale_detail]  WITH CHECK ADD  CONSTRAINT [sale_detail$sale_detail_ibfk_1] FOREIGN KEY([idSale])
REFERENCES [dbo].[sale] ([idSale])
GO
ALTER TABLE [dbo].[sale_detail] CHECK CONSTRAINT [sale_detail$sale_detail_ibfk_1]
GO
/****** Object:  ForeignKey [sale_detail$sale_detail_ibfk_2]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[sale_detail]  WITH CHECK ADD  CONSTRAINT [sale_detail$sale_detail_ibfk_2] FOREIGN KEY([idProduct])
REFERENCES [dbo].[product] ([idProduct])
GO
ALTER TABLE [dbo].[sale_detail] CHECK CONSTRAINT [sale_detail$sale_detail_ibfk_2]
GO
/****** Object:  ForeignKey [FK_Supplies_branch]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[supplies]  WITH CHECK ADD  CONSTRAINT [FK_Supplies_branch] FOREIGN KEY([idBranch])
REFERENCES [dbo].[branch] ([idBranch])
GO
ALTER TABLE [dbo].[supplies] CHECK CONSTRAINT [FK_Supplies_branch]
GO
/****** Object:  ForeignKey [FK_user_branch]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_branch] FOREIGN KEY([idBranch])
REFERENCES [dbo].[branch] ([idBranch])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_branch]
GO
/****** Object:  ForeignKey [FK_user_role]    Script Date: 02/02/2020 16:52:22 ******/
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_role] FOREIGN KEY([idRole])
REFERENCES [dbo].[role] ([idRole])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_role]
GO
