﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.WorkUnit;
using SisCom.Structure.ViewModels.WorkUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class workUnitController : Controller
    {
        private ComEdbEntities _db;
        private WorkUnitMapper _workUnitMapper;
        private IWorkUnitRepository _workUnitRepository;

        public workUnitController
            (
            ComEdbEntities db,
            WorkUnitMapper workUnitMapper,
            IWorkUnitRepository workUnitRepository
            )
        {
            _db = db;
            _workUnitMapper = workUnitMapper;
            _workUnitRepository = workUnitRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(PaginatorGeneric<work_unit, WorkUnitViewModel> paginator)
        {
            List<work_unit> resultsModel = _workUnitRepository.GetWorkUnit();
            var viewmodel = _workUnitMapper.MapperListModelToViewModel(resultsModel);

            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _workUnitRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _workUnitMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var workUnitViewModel = new WorkUnitViewModel();

            return Json(new { workUnitViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateWorkUnitJson(WorkUnitViewModel workUnitViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (workUnitViewModel.name == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                workUnitViewModel.isActive = true;
                workUnitViewModel.deletedAt = "12/12/12";

                var workUnitModel = _workUnitMapper.MapperViewModelToModel(workUnitViewModel);
                _workUnitRepository.Insert(workUnitModel);

                var urlRedirect = Url.Action("Index", "WorkUnit");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, workUnitViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }
        }
        public ActionResult Edit(int idWorkUnit)
        {
            WorkUnitViewModel workUnitViewModel = new WorkUnitViewModel();
            workUnitViewModel.idWorkUnit = idWorkUnit;

            return View(workUnitViewModel);
        }
        public ActionResult EditJson(int idWorkUnit)
        {
            var workUnit = _workUnitRepository.FindById(idWorkUnit);
            var workUnitViewModel = _workUnitMapper.MapperModelToViewModel(workUnit);

            return Json(new { workUnitViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditWorkUnitJson(WorkUnitViewModel workUnitViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (workUnitViewModel.name == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                var workUnitModel = _workUnitMapper.MapperViewModelToModel(workUnitViewModel);
                _workUnitRepository.Update(workUnitModel);

                var urlRedirect = Url.Action("Index", "WorkUnit");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, workUnitViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }
        }
        public ActionResult Detail(int idWorkUnit)
        {
            WorkUnitViewModel workUnitViewModel = new WorkUnitViewModel();
            workUnitViewModel.idWorkUnit = idWorkUnit;

            return View(workUnitViewModel);
        }
        public ActionResult Delete(int idWorkUnit)
        {
            WorkUnitViewModel workUnitViewModel = new WorkUnitViewModel();
            workUnitViewModel.idWorkUnit = idWorkUnit;

            return View(workUnitViewModel);
        }
        [HttpPost]
        public ActionResult DeleteWorkUnitJson(WorkUnitViewModel workUnitViewModel)
        {
            workUnitViewModel.isActive = false;

            var workUnitModel = _workUnitMapper.MapperViewModelToModel(workUnitViewModel);
            _workUnitRepository.Update(workUnitModel);
            var urlRedirect = Url.Action("Index", "WorkUnit");
            var Message = "La unidad de trabajo ha sido actualizada correctamente";

            return Json(new { Message, urlRedirect, workUnitViewModel });
        }
    }
}