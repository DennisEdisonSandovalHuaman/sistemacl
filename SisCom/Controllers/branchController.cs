﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.BrachMapper;
using SisCom.Structure.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class branchController : Controller
    {
        private ComEdbEntities _db;
        private BranchMapper _branchMapper;
        private IBranchRepository _branchRepository;

        public branchController
            (
            ComEdbEntities db,
            BranchMapper branchMapper,
            IBranchRepository branchRepository
            )
        {
            _db = db;
            _branchMapper = branchMapper;
            _branchRepository = branchRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(PaginatorGeneric<branch, BranchViewModel> paginator)
        {
            List<branch> resultsModel = _branchRepository.GetBranch();
            var viewmodel = _branchMapper.MapperListModelToViewModel(resultsModel);
        
            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _branchRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _branchMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var branchViewModel = new BranchViewModel();

            return Json(new { branchViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateBranchJson(BranchViewModel branchViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (branchViewModel.name == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                branchViewModel.isActive = true;
                branchViewModel.createdAt = DateTime.Now.ToString();
                branchViewModel.deletedAt = "12/12/12";

                var branchModel = _branchMapper.MapperViewModelToModel(branchViewModel);
                _branchRepository.Insert(branchModel);

                var urlRedirect = Url.Action("Index", "Branch");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, branchViewModel });
            }
            catch (Exception)
            {              
                return Json(new { state });
            }
        }
        public ActionResult Edit(int idBranch)
        {
            BranchViewModel branchViewModel = new BranchViewModel();
            branchViewModel.idBranch = idBranch;

            return View(branchViewModel);
        }
        public ActionResult EditJson(int idBranch)
        {
            var branch = _branchRepository.FindById(idBranch);
            var branchViewModel = _branchMapper.MapperModelToViewModel(branch);

            return Json(new { branchViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditBranchJson(BranchViewModel branchViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (branchViewModel.name == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                var branchModel = _branchMapper.MapperViewModelToModel(branchViewModel);
                _branchRepository.Update(branchModel);

                var urlRedirect = Url.Action("Index", "Branch");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, branchViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });          
            }         
        }
        public ActionResult Detail(int idBranch)
        {
            BranchViewModel branchViewModel = new BranchViewModel();
            branchViewModel.idBranch = idBranch;

            return View(branchViewModel);
        }
        public ActionResult Delete(int idBranch)
        {
            BranchViewModel branchViewModel = new BranchViewModel();
            branchViewModel.idBranch = idBranch;

            return View(branchViewModel);
        }
        [HttpPost]
        public ActionResult DeleteBranchJson(BranchViewModel branchViewModel)
        {
            branchViewModel.isActive = false;
            branchViewModel.deletedAt = DateTime.Now.ToString();

            var branchModel = _branchMapper.MapperViewModelToModel(branchViewModel);
            _branchRepository.Update(branchModel);
            var urlRedirect = Url.Action("Index", "Branch");
            var Message = "La sucursal ha sido actualizada correctamente";

            return Json(new { Message, urlRedirect, branchViewModel });
        }
        public void checkSession()
        {
            if (Session["usuario"] == null)
            {
                Response.Redirect("~/login/login");
            }
        }
    }
}