﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SisCom.Models;
using SisCom.Structure.ViewModels.Products;
using SisCom.Structure.Mappers.Products;
using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Filters;

namespace SisCom.Controllers
{

    [Authorize(Roles = "Administrador, Cajero")]
    public class productsController : Controller
    {
        private ComEdbEntities _db;
        private IProductRepository _productRepository;
        private ProductMapper _productMapper;
        private IDropdownRepository _dropdownRepository;

        public productsController
            (
            ComEdbEntities db,
            IProductRepository productRepository,
            ProductMapper productMapper,
            IDropdownRepository dropdownRepository
            )
        {
            _db = db;
            _productRepository = productRepository;
            _productMapper = productMapper;
            _dropdownRepository = dropdownRepository;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult IndexJson(ProductsFilters filters)
        {
            List<product> resultsModel = _productRepository.GetProducts();
            var viewmodel = _productMapper.MapperListModelToViewModel(resultsModel);

            if (filters.Page == 0)
            {
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _productRepository.GetAllByPagedFilters(filters);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _productMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var productViewModel = new ProductViewModel();
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, productViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateProductJson(ProductViewModel productViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (productViewModel.name == null ||
                productViewModel.withStock == null ||
                productViewModel.idServiceType == null ||
                productViewModel.idBranch == null )
            {
                return Json(new { formSuccess, msjError });
            }
            if (productViewModel.withStock == "1")
            {
                if (productViewModel.currentStock == null || productViewModel.minimunStock == null)
                {
                    msjError = "*Por favor, ingresar el stock de los productos";
                    return Json(new { formSuccess, msjError });
                }
            }
            try
            {
                productViewModel.isActive = true;

                var productModel = _productMapper.MapperViewModelToModel(productViewModel);
                _productRepository.Insert(productModel);

                var urlRedirect = Url.Action("Index", "Products");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, productViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }        
        }
        public ActionResult Edit(int idProduct)
        {
            ProductViewModel productViewModel = new ProductViewModel();
            productViewModel.idProduct = idProduct;

            return View(productViewModel);
        }
        public ActionResult EditJson(int idProduct)
        {
            var product = _productRepository.FindById(idProduct);
            var productViewModel = _productMapper.MapperModelToViewModel(product);
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, productViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditProductJson(ProductViewModel productViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (productViewModel.name == null ||
                productViewModel.withStock == null ||
                productViewModel.idServiceType == null ||
                productViewModel.idBranch == null)
            {
                return Json(new { formSuccess, msjError });
            }
            if (productViewModel.withStock == "1" )
            {
                if (productViewModel.currentStock == null || productViewModel.minimunStock == null)
                {
                    msjError = "*Por favor, ingresar el stock de los productos";
                    return Json(new { formSuccess, msjError });
                }
            }
            try
            {
                var productModel = _productMapper.MapperViewModelToModel(productViewModel);
                _productRepository.Update(productModel);

                var urlRedirect = Url.Action("Index", "Products");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, productViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }

        }
        public ActionResult Details(int idProduct)
        {
            ProductViewModel productViewModel = new ProductViewModel();
            productViewModel.idProduct = idProduct;

            return View(productViewModel);
        }
        public ActionResult Delete(int idProduct)
        {
            ProductViewModel productViewModel = new ProductViewModel();
            productViewModel.idProduct = idProduct;

            return View(productViewModel);
        }
        [HttpPost]
        public ActionResult DeleteProductJson(ProductViewModel productViewModel)
        {
            productViewModel.isActive = false;
            var productModel = _productMapper.MapperViewModelToModel(productViewModel);
            _productRepository.Update(productModel);
            var urlRedirect = Url.Action("Index", "Products");
            var Message = "El Producto ha sido desactivado correctamente";

            return Json(new { Message, urlRedirect, productViewModel });
        }
        private object GetDropdowns()
        {
            return new
            {
                ServiceType = _dropdownRepository.ServiceType(),
                Branch = _dropdownRepository.Branch(),
            };
        }
        public ActionResult GetActivesAndInactivesProducts()
        {
            var allProducts = _productRepository.GetProducts();
            var activesProducts = allProducts.Where(product => product.isActive).Count();
            var inactivesProducts = allProducts.Where(product => !product.isActive).Count();
            return Json(new { activesProducts, inactivesProducts }, JsonRequestBehavior.AllowGet);
        }
    }
}
