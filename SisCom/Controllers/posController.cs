﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Pos;
using SisCom.Structure.ViewModels.Pos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class posController : Controller
    {
        private ComEdbEntities _db;
        private PosMapper _posMapper;
        private IPosRepository _posRepository;
        private IDropdownRepository _dropdownRepository;

        public posController
            (
            ComEdbEntities db,
            PosMapper posMapper,
            IPosRepository posRepository,
            IDropdownRepository dropdownRepository
            )
        {
            _db = db;
            _posMapper = posMapper;
            _posRepository = posRepository;
            _dropdownRepository = dropdownRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(PaginatorGeneric<pos, PosViewModel> paginator)
        {
            List<pos> resultsModel = _posRepository.GetPos();
            var viewmodel = _posMapper.MapperListModelToViewModel(resultsModel);

            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _posRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _posMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var posViewModel = new PosViewModel();
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, posViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreatePosJson(PosViewModel posViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (posViewModel.name == null || posViewModel.idUser == 0)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                posViewModel.isActive = true;
                posViewModel.deletedAt = "12/12/12";

                var posModel = _posMapper.MapperViewModelToModel(posViewModel);
                _posRepository.Insert(posModel);

                var urlRedirect = Url.Action("Index", "Pos");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, posViewModel });
            }
            catch (Exception e)
            {
                return Json(new { state });
            }          
        }
        public ActionResult Edit(int idPos)
        {
            PosViewModel posViewModel = new PosViewModel();
            posViewModel.idPos = idPos;

            return View(posViewModel);
        }
        public ActionResult EditJson(int idPos)
        {
            var pos = _posRepository.FindById(idPos);
            var posViewModel = _posMapper.MapperModelToViewModel(pos);
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, posViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditPosJson(PosViewModel posViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (posViewModel.name == null || posViewModel.idUser == 0)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                var posModel = _posMapper.MapperViewModelToModel(posViewModel);
                _posRepository.Update(posModel);

                var urlRedirect = Url.Action("Index", "Pos");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, posViewModel });
            }
            catch (Exception e)
            {
                return Json(new { state });
            }
        }
        public ActionResult Detail(int idPos)
        {
            PosViewModel posViewModel = new PosViewModel();
            posViewModel.idPos = idPos;

            return View(posViewModel);
        }
        public ActionResult Delete(int idPos)
        {
            PosViewModel posViewModel = new PosViewModel();
            posViewModel.idPos = idPos;

            return View(posViewModel);
        }
        [HttpPost]
        public ActionResult DeletePosJson(PosViewModel posViewModel)
        {
            posViewModel.isActive = false;
            posViewModel.deletedAt = DateTime.Now.ToString();
            var posModel = _posMapper.MapperViewModelToModel(posViewModel);
            _posRepository.Update(posModel);
            var urlRedirect = Url.Action("Index", "Pos");
            var Message = "El punto de servicio ha sido actualizado correctamente";

            return Json(new { Message, urlRedirect, posViewModel });
        }
        private object GetDropdowns()
        {
            return new
            {
                User = _dropdownRepository.User(),
            };
        }
    }
}