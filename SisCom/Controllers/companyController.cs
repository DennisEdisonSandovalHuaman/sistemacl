﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Company;
using SisCom.Structure.ViewModels.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador,Cajero")]
    public class companyController : Controller
    {
        private ComEdbEntities _db;
        private CompanyMapper _companyMapper;
        private ICompanyRepository _companyRepository;

        public companyController
            (
            ComEdbEntities db,
            CompanyMapper companyMapper,
            ICompanyRepository companyRepository
            )
        {
            _db = db;
            _companyMapper = companyMapper;
            _companyRepository = companyRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(PaginatorGeneric<company, CompanyViewModel> paginator)
        {
            List<company> resultsModel = _companyRepository.GetCompany();
            var viewmodel = _companyMapper.MapperListModelToViewModel(resultsModel);

            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _companyRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _companyMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var companyViewModel = new CompanyViewModel();

            return Json(new { companyViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateCompanyJson(CompanyViewModel companyViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (companyViewModel.name == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                companyViewModel.isActive = true;

                var companyModel = _companyMapper.MapperViewModelToModel(companyViewModel);
                _companyRepository.Insert(companyModel);

                var urlRedirect = Url.Action("Index", "Company");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, companyViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }
        }
        public ActionResult Edit(int idCompany)
        {
            CompanyViewModel companyViewModel = new CompanyViewModel();
            companyViewModel.idCompany = idCompany;

            return View(companyViewModel);
        }
        public ActionResult EditJson(int idCompany)
        {
            var company = _companyRepository.FindById(idCompany);
            var companyViewModel = _companyMapper.MapperModelToViewModel(company);

            return Json(new { companyViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditCompanyJson(CompanyViewModel companyViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (string.IsNullOrEmpty(companyViewModel.name))
            {
                return Json(new { formSuccess, msjError });
            }
            
            var companyModel = _companyMapper.MapperViewModelToModel(companyViewModel);
            _companyRepository.Update(companyModel);

            var urlRedirect = Url.Action("Index", "Company");
            state = true;
            formSuccess = true;

            return Json(new { formSuccess, state, urlRedirect, companyViewModel });

        }
        public ActionResult Detail(int idCompany)
        {
            CompanyViewModel companyViewModel = new CompanyViewModel();
            companyViewModel.idCompany = idCompany;

            return View(companyViewModel);
        }
        public ActionResult Delete(int idCompany)
        {
            CompanyViewModel companyViewModel = new CompanyViewModel();
            companyViewModel.idCompany = idCompany;

            return View(companyViewModel);
        }
        [HttpPost]
        public ActionResult DeleteCompanyJson(CompanyViewModel companyViewModel)
        {
            companyViewModel.isActive = false;
            companyViewModel.deletedAt = DateTime.Now.ToString();

            var companyModel = _companyMapper.MapperViewModelToModel(companyViewModel);
            _companyRepository.Update(companyModel);
            var urlRedirect = Url.Action("Index", "Company");
            var Message = "La empresa ha sido actualizada correctamente";

            return Json(new { Message, urlRedirect, companyViewModel });
        }
    }
}