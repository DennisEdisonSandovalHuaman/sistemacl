﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SisCom.Data.IRepositories;
using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.BrachMapper;
using SisCom.Structure.Mappers.Supplies;
using SisCom.Structure.ViewModels.Supplies;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador, Cajero")]
    public class suppliesController : Controller
    {
        private ISuppliesRepository _suppliesRepository;
        private SuppliesMapper _suppliesMapper;
        private BranchMapper _branchMapper;
        private readonly IBranchRepository _branchRepository;
        private ComEdbEntities db = new ComEdbEntities();

        public suppliesController(ISuppliesRepository suppliesRepository, SuppliesMapper suppliesMapper, BranchMapper branchMapper, IBranchRepository branchRepository)
        {
            _suppliesRepository = suppliesRepository;
            _branchMapper = branchMapper;
            _suppliesMapper = suppliesMapper;
            _branchRepository = branchRepository;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(SuppliesFilters filters)
        {
            List<supplies> resultsModel = _suppliesRepository.GetSupplies();
            var viewmodel = _suppliesMapper.MapperListModelToViewModel(resultsModel);

            if (filters.Page == 0)
            {
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _suppliesRepository.GetAllByPagedFilters(filters);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _suppliesMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            supplies supplies = db.supplies.Find(id);
            if (supplies == null)
            {
                return HttpNotFound();
            }
            return View(supplies);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateJson()
        {
            var supplyVM = new SuppliesViewModel();
            var branchsVM = _branchMapper.MapperListModelToViewModel(_branchRepository.GetBranch());
            return Json(new { supplyVM, branchsVM }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Store(SuppliesViewModel suppliesViewModel)
        {
            try
            {
                if (string.IsNullOrEmpty(suppliesViewModel.name))
                    return Json(new { state = false, message = "Debe ingresar el nombre del insumo ... !!! " }, JsonRequestBehavior.AllowGet);

                if (suppliesViewModel.idBranch == 0)
                    return Json(new { state = false, message = "Debe escoger la sucursal del insumo ... !!! " }, JsonRequestBehavior.AllowGet);

                if (double.IsNaN(suppliesViewModel.stock))
                    return Json(new { state = false, message = "Debe ingresar un numero para el campo stock del insumo ... !!! " }, JsonRequestBehavior.AllowGet);
                
                if (double.IsNaN(suppliesViewModel.minStock))
                    return Json(new { state = false, message = "Debe ingresar un numero para el campo stock minimo del insumo ... !!! " }, JsonRequestBehavior.AllowGet);
                
                if (double.IsNaN(suppliesViewModel.weight))
                    return Json(new { state = false, message = "Debe ingresar un numero para el campo peso del insumo ... !!! " }, JsonRequestBehavior.AllowGet);
                
                if (double.IsNaN(suppliesViewModel.minWeight))
                    return Json(new { state = false, message = "Debe ingresar un numero para el campo peso minimo del insumo ... !!! " }, JsonRequestBehavior.AllowGet);
                
                if (double.IsNaN(suppliesViewModel.price))
                    return Json(new { state = false, message = "Debe ingresar un numero para el campo precio del insumo ... !!! " }, JsonRequestBehavior.AllowGet);

                _suppliesRepository.Insert(_suppliesMapper.MapperViewModelToModel(suppliesViewModel));
                return Json(new { state = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { state = false, message = "Revise los datos ingresados en el formulario" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(int id)
        {
            SuppliesViewModel supplyVM = new SuppliesViewModel();
            supplyVM.idSupplies = id;
            return View(supplyVM);
        }
        public ActionResult EditJson(int idSupplies)
        {
            var supplyVM = _suppliesMapper.MapperModelToViewModel(_suppliesRepository.GetById(idSupplies));
            var branchsVM = _branchMapper.MapperListModelToViewModel(_branchRepository.GetBranch());
            return Json(new { supplyVM, branchsVM }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Update(SuppliesViewModel suppliesViewModel)
        {
            try
            {
                _suppliesRepository.Update(_suppliesMapper.MapperViewModelToModel(suppliesViewModel));
                return Json(new { state = true, typeMessage = "success", message = "El insumo se actualizó exitosamente ... !!! " }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { state = false, typeMessage = "error", message = "Ocurrió un error actualizando... !!! " }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            supplies supplies = db.supplies.Find(id);
            if (supplies == null)
            {
                return HttpNotFound();
            }
            return View(supplies);
        }

        [HttpGet]
        public ActionResult Destroy(int id)
        {
            _suppliesRepository.VirtualDelete(id);
            return Json(new { state = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BringSupplyByID(int id)
        {
            return Json(_suppliesMapper.MapperModelToViewModel(_suppliesRepository.GetById(id)), JsonRequestBehavior.AllowGet);
        }
    }
}
