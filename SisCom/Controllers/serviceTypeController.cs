﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.ServiceType;
using SisCom.Structure.ViewModels.ServiceType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class serviceTypeController : Controller
    {
        private ComEdbEntities _db;
        private ServiceTypeMapper _serviceTypeMapper;
        private IServiceTypeRepository _serviceTypeRepository;

        public serviceTypeController
            (
            ComEdbEntities db,
            ServiceTypeMapper serviceTypeMapper,
            IServiceTypeRepository serviceTypeRepository
            )
        {
            _db = db;
            _serviceTypeMapper = serviceTypeMapper;
            _serviceTypeRepository = serviceTypeRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(PaginatorGeneric<service_type, ServiceTypeViewModel> paginator)
        {
            List<service_type> resultsModel = _serviceTypeRepository.GetServiceType();
            var viewmodel = _serviceTypeMapper.MapperListModelToViewModel(resultsModel);

            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _serviceTypeRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _serviceTypeMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var serviceTypeViewModel = new ServiceTypeViewModel();

            return Json(new { serviceTypeViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateServiceTypeJson(ServiceTypeViewModel serviceTypeViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (serviceTypeViewModel.name == null || serviceTypeViewModel.startTime == null || serviceTypeViewModel.endTime == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                var serviceTypeModel = _serviceTypeMapper.MapperViewModelToModel(serviceTypeViewModel);
                _serviceTypeRepository.Insert(serviceTypeModel);

                var urlRedirect = Url.Action("Index", "ServiceType");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, serviceTypeViewModel });
            }
            catch (Exception e)
            {
                return Json(new { state });
            }
        }
        public ActionResult Edit(int idServiceType)
        {
            ServiceTypeViewModel serviceTypeViewModel = new ServiceTypeViewModel();
            serviceTypeViewModel.idServiceType = idServiceType;
            return View(serviceTypeViewModel);
        }
        public ActionResult EditJson(int idServiceType)
        {
            var serviceType = _serviceTypeRepository.FindById(idServiceType);
            var serviceTypeViewModel = _serviceTypeMapper.MapperModelToViewModel(serviceType);

            return Json(new { serviceTypeViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditServiceTypeJson(ServiceTypeViewModel serviceTypeViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (serviceTypeViewModel.name == null || serviceTypeViewModel.startTime == null || serviceTypeViewModel.endTime == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                var serviceTypeModel = _serviceTypeMapper.MapperViewModelToModel(serviceTypeViewModel);
                _serviceTypeRepository.Update(serviceTypeModel);

                var urlRedirect = Url.Action("Index", "ServiceType");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, serviceTypeViewModel });
            }
            catch (Exception e)
            {
                return Json(new { state });
            }
        }
    }
}