﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SisCom.Models;
using SisCom.Data.IRepositories;
using SisCom.Data.Repositories;
using SisCom.Structure.ViewModels.Sales;
using SisCom.Structure.ViewModels.Customers;
using SisCom.Structure.ViewModels.ResultViewModel;
using SisCom.Structure.Mappers.Sales;
using SisCom.Structure.Mappers.Products;
using SisCom.Structure.Mappers.Customers;
using SisCom.Data;
using SisCom.Structure.Mappers.Attendance;
using SisCom.Helpers;
using SisCom.Structure.Mappers.Users;
using SisCom.Structure.Mappers.ServiceType;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador, Cajero")]
    public class salesController : Controller
    {
        private ResultViewModel resultViewModel = new ResultViewModel();
        private SaleMapper _saleMapper;
        private SaleDetailMapper _saleDetailMapper;
        private CustomersMapper _customerMapper;
        private ProductMapper _productMapper;
        private AttendanceMapper _attendaceMapper;
        private UsersMapper _usersMapper;
        private IGenericRepository<sale_detail> _detailSaleRepository;
        private ISaleRepository _saleRepository;
        private IProductRepository _productRepository;
        private IBranchRepository _branchRepository;
        private IAttendanceRepository _attendaceRepository;
        private ICustomersRepository _customerRepository;
        private IServiceTypeRepository _serviceTypeRepository;
        private IUserRepository _userRepository;

        public salesController(UsersMapper usersMapper, IUserRepository userRepository, ResultViewModel resultViewModel, SaleMapper saleMapper, SaleDetailMapper saleDetailMapper, CustomersMapper customerMapper, ProductMapper productMapper, AttendanceMapper attendaceMapper, IGenericRepository<sale_detail> detailSaleRepository, ISaleRepository saleRepository, IProductRepository productRepository, IBranchRepository branchRepository, IAttendanceRepository attendaceRepository, ICustomersRepository customerRepository, IServiceTypeRepository serviceTypeRepository)
        {
            this.resultViewModel = resultViewModel;
            _saleMapper = saleMapper;
            _usersMapper = usersMapper;
            _saleDetailMapper = saleDetailMapper;
            _customerMapper = customerMapper;
            _productMapper = productMapper;
            _attendaceMapper = attendaceMapper;
            _detailSaleRepository = detailSaleRepository;
            _saleRepository = saleRepository;
            _productRepository = productRepository;
            _branchRepository = branchRepository;
            _attendaceRepository = attendaceRepository;
            _customerRepository = customerRepository;
            _serviceTypeRepository = serviceTypeRepository;
            _userRepository = userRepository;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult IndexJson(PaginatorGeneric<sale, SaleIndexViewModel> paginator)
        {
            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                int idUser = new RecursosUserLogin().getUserById(User.Identity.Name);
                var userVM = _usersMapper.MapperModelToViewModel(_userRepository.GetById(idUser));

                var result = new PaginatorGeneric<sale, SaleIndexViewModel>();


                if (userVM.Role.name.Equals("Administrador", StringComparison.InvariantCultureIgnoreCase))
                {
                    result = _saleRepository.GetAllByPaged(paginator);
                }
                else
                {
                    var attendace = _attendaceRepository.GetCurrentAttendaceByUserID(idUser);

                    result = _saleRepository.GetAllByPagedAndUserID(paginator, idUser);

                    if (attendace == null)
                        return Json(new { list = new PaginatorGeneric<sale, SaleIndexViewModel>(), state = false, titleError = "La caja es necesaria !!!", message = "Caja cerrada, por favor abra una para poder realizar ventas" }, JsonRequestBehavior.AllowGet);

                }

                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _saleMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;

                var list = paginator;

                return Json(new { state = true, list }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Details(long? idSale)
        {
            return View(_saleMapper.ConvertModelToViewModel(_saleRepository.GetById((int)idSale)));
        }

        public ActionResult DetailJson(long? idSale)
        {
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateJson()
        {
            var serviceTypeVM = _serviceTypeRepository.GetCurrentServiceType();
            var saleVM = new SaleCreateViewModel();
            var customerVM = new CustomersViewModel();

            if (serviceTypeVM == null)
            {
                resultViewModel.Message = "El servicio para esta hora del día no está asignado ...";
                resultViewModel.MessageTitle = "Servicio no asignado!!!";
                resultViewModel.State = false;
                resultViewModel.MessageType = "error";

                return Json(new { resultViewModel, saleVM, customerVM }, JsonRequestBehavior.AllowGet);
            }
            int idUser = new RecursosUserLogin().getUserById(User.Identity.Name);
            var user = _userRepository.GetById(idUser);
            var branch = _branchRepository.GetById(user.idBranch.Value);
            var productsModels = _productRepository.GetProductsToSaleByBranchID(branch.idBranch);
            var products = _productMapper.MapperListModelToViewModel(productsModels);

            var customers = _customerMapper.MapperListModelToViewModel(_customerRepository.GetCustomers());
            var customersToTypeHead = customers.Select(x => x.name + " " + x.fatherLastname + " " + x.motherLastname + ":" + x.dni).ToList();

            return Json(new { resultViewModel, serviceTypeVM, saleVM, customerVM, products, customersToTypeHead }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetDNICustomer(string document)
        {
            var customer = _customerMapper.MapperModelToViewModel(_customerRepository.GetByDNI(document.Trim(' ')));
            return Json(customer);
        }

        [HttpPost]
        public ActionResult StoreFast(SaleCreateViewModel saleCreateViewModel, string dni)
        {
            try
            {
                #region FIND CUSTOMER

                var customerModel = _customerRepository.GetByDNI(dni);
                if (customerModel == null)
                    return Json(new { state = false, titleError = "Cliente inexistente !!!", message = "El cliente que busca no existe, por favor revise el código ingresado ... ", typeResponse = "error" }, JsonRequestBehavior.AllowGet);

                else if (!customerModel.isActive)
                    return Json(new { state = false, titleError = "Cliente inactivo !!!", message = $"El cliente {customerModel.name} {customerModel.fatherLastname} está temporalmente inactivo, no se puede entregar servicio ... ", typeResponse = "warning" }, JsonRequestBehavior.AllowGet);

                else if (!customerModel.hasGrant)
                    return Json(new { state = false, titleError = "Cliente sin subvención !!!", message = $"El cliente {customerModel.name} {customerModel.fatherLastname} no cuenta con servicios subvencionados ... ", typeResponse = "warning" }, JsonRequestBehavior.AllowGet);

                if (_customerRepository.VerifyDeliveryOfFoolToCustomerById(customerModel.idCustomer))
                    return Json(new { state = false, titleError = "Cliente ya recibió !!!", message = $"El cliente {customerModel.name} {customerModel.fatherLastname} ya ha sido registrado para este servicio de comida ... ", typeResponse = "error" }, JsonRequestBehavior.AllowGet);

                #endregion

                #region GET CURRENT ATTENDANCE

                int idUser = new RecursosUserLogin().getUserById(User.Identity.Name);
                var attendace = _attendaceRepository.GetCurrentAttendaceByUserID(idUser);
                if (attendace == null)
                {
                    return Json(new { state = false, titleError = "Caja inexistente !!!", message = "No existe una caja para poder realizar la venta, porfavor abra una ... ", typeResponse = "error" }, JsonRequestBehavior.AllowGet);
                }
                var attendaceVM = _attendaceMapper.MapperModelToViewModel(attendace);

                #endregion

                #region GET CURRENT SERVICE
                var serviceTypeVM = _serviceTypeRepository.GetCurrentServiceType();
                #endregion

                if (serviceTypeVM == null)
                    if (serviceTypeVM == null) return Json(new { state = false, titleError = "Servicio no asignado !!!", message = "El servicio para esta hora del día no está asignado ..." }, JsonRequestBehavior.AllowGet);

                saleCreateViewModel.idCustomer = customerModel.idCustomer;
                saleCreateViewModel.idAttendace = attendace.idAttendance;
                saleCreateViewModel.accountPayment = (float)serviceTypeVM.price;
                saleCreateViewModel.totalPrice = (float)serviceTypeVM.price;
                saleCreateViewModel.totalGranted = (float)serviceTypeVM.price;
                saleCreateViewModel.idServiceType = serviceTypeVM.idServiceType;
                saleCreateViewModel.totalGranted = saleCreateViewModel.totalPrice;

                saleCreateViewModel.verifiyIsPaidOrNotPaid();

                var sale = _saleMapper.ConvertViewModelToModel(saleCreateViewModel);

                _saleRepository.Insert(sale);

                _attendaceRepository.IncrementCurrentCash(attendaceVM.idAttendance, sale.accountPayment);

                return Json(new { state = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                return Json(new
                {
                    state = false,
                    titleError = "Error en el sistema !!!",
                    message = exception.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult Store(SaleCreateViewModel saleCreateViewModel, List<DetailToSaleViewModel> items)
        {
            try
            {
                int idUser = new RecursosUserLogin().getUserById(User.Identity.Name);
                var attendace = _attendaceRepository.GetCurrentAttendaceByUserID(idUser);
                if (attendace == null) return Json(new { status = false, titleError = "La caja es necesaria !!!", message = "Caja cerrada, por favor abra una para poder realizar ventas" }, JsonRequestBehavior.AllowGet);
                var attendaceVM = _attendaceMapper.MapperModelToViewModel(attendace);

                saleCreateViewModel.idAttendace = attendace.idAttendance;
                saleCreateViewModel.verifiyIsPaidOrNotPaid();

                var saleModel = _saleMapper.ConvertViewModelToModel(saleCreateViewModel);
                saleModel.idServiceType = null;

                items.ForEach(item => item.calculateTotalPriceAndTotalGranted());
                var detailsModels = _saleDetailMapper.ConvertViewModelsToModels(items);

                var sale = _saleRepository.Insert(saleModel);
                if (sale.accountPayment != null) _attendaceRepository.IncrementCurrentCash(attendaceVM.idAttendance, sale.accountPayment);

                foreach (var detail in detailsModels)
                {
                    detail.idSale = sale.idSale;
                    _productRepository.DecrementStockByIdProduct(detail.idProduct, detail.quantity);
                    _detailSaleRepository.Insert(detail);
                }
                return Json(new { state = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                return Json(new
                {
                    state = false,
                    titleError = "Error en el sistema !!!",
                    message = exception.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult FindCustomer(string dni)
        {
            using (var db = new ComEdbEntities())
            {
                var customerModel = _customerRepository.GetByDNI(dni);
                if (customerModel == null)
                    return Json(new { state = false, titleError = "Cliente inexistente !!!", message = "El usuario que busca no existe, por favor revise el código ingresado ... " }, JsonRequestBehavior.AllowGet);

                var customerVM = _customerMapper.MapperModelToViewModel(customerModel);

                return Json(new { customerVM, state = true }, JsonRequestBehavior.AllowGet); ;
            }
        }

        [HttpPost]
        public ActionResult SaveUntimelySale(CustomersViewModel customerVM, string date, int idServiceType)
        {
            try
            {
                var listDate = date.Split('/');
                var day = Convert.ToInt32(listDate[0]);
                var month = Convert.ToInt32(listDate[1]);
                var year = Convert.ToInt32(listDate[2]);

                var newDate = new DateTime(year, month, day);

                var serviceTypeVM = _serviceTypeRepository.FindById(idServiceType);
                var customerModel = _customerRepository.FindById(customerVM.idCustomer);
                int idUser = new RecursosUserLogin().getUserById(User.Identity.Name);
                var attendace = _attendaceRepository.GetCurrentAttendaceByUserID(idUser);
                SaleCreateViewModel saleCreateViewModel = new SaleCreateViewModel();

                if (attendace == null) return Json(new { state = false, titleError = "La caja es necesaria !!!", message = "Caja cerrada, por favor abra una para poder realizar ventas" }, JsonRequestBehavior.AllowGet);

                saleCreateViewModel.idCustomer = customerModel.idCustomer;
                saleCreateViewModel.idAttendace = attendace.idAttendance;
                saleCreateViewModel.accountPayment = (float)serviceTypeVM.price;
                saleCreateViewModel.totalPrice = (float)serviceTypeVM.price;
                saleCreateViewModel.totalGranted = (float)serviceTypeVM.price;
                saleCreateViewModel.idServiceType = serviceTypeVM.idServiceType;
                saleCreateViewModel.totalGranted = saleCreateViewModel.totalPrice;
                saleCreateViewModel.createdAt = newDate.ToString("d/M/yy") + " " + serviceTypeVM.startTime.ToString("HH:mm");

                saleCreateViewModel.verifiyIsPaidOrNotPaid();

                var sale = _saleMapper.ConvertViewModelToModel_WithoutCreateDate(saleCreateViewModel);

                _saleRepository.Insert(sale);
                _attendaceRepository.IncrementCurrentCash(attendace.idAttendance, sale.accountPayment);

                return Json(new { state = true }, JsonRequestBehavior.AllowGet); ;
            }
            catch (Exception e)
            {
                return Json(new { state = false, titleError = "¡¡ Ocurrió un error, intente más tarde !!", message = "Ocurrió un error imprevisto al intentar guardar la venta ... ", TrueError = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FastCreate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FastCreateJson()
        {
            var saleVM = new SaleCreateViewModel();
            var serviceTypes = new ServiceTypeMapper().MapperListModelToViewModel(_serviceTypeRepository.GetServiceType());
            var customerVM = new CustomersViewModel();

            var customers = _customerMapper.MapperListModelToViewModel(_customerRepository.GetCustomers());
            var customersToTypeHead = customers.Select(x => x.name + " " + x.fatherLastname + " " + x.motherLastname + ":" + x.dni).ToList();

            return Json(new { resultViewModel, saleVM, serviceTypes, customers, customersToTypeHead, customerVM }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int id)
        {
            SaleCreateViewModel saleVM = new SaleCreateViewModel();
            saleVM.idSale = id;
            return View(saleVM);
        }
        public ActionResult EditJson(int idSale)
        {
            var saleVM = _saleMapper.ConvertModelToViewModel(_saleRepository.GetById(idSale));
            return Json(new { saleVM }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Update(SaleCreateViewModel saleVM)
        {
            try
            {
                _saleRepository.Update(_saleMapper.ConvertViewModelToModel(saleVM));
                return Json(new { state = true, typeMessage = "success", message = "La venta se actualizó exitosamente ... !!! " }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { state = false, typeMessage = "error", message = "Ocurrió un error actualizando... !!! " }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GetActivesAndInactivesSales()
        {
            var sales = _saleRepository.GetSales();
            var canceledSales = sales.Where(sale => sale.deletedAt != null).Count();
            var fastSalesMade = sales.Where(sale => sale.idServiceType != null).Count();
            var normalSalesMade = sales.Where(sale => sale.idServiceType == null).Count();

            return Json(new { canceledSales, fastSalesMade, normalSalesMade }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Delete(int id)
        {
            if (_saleRepository.VirtualDelete(id))
            {
                int idUser = new RecursosUserLogin().getUserById(User.Identity.Name);
                var attendace = _attendaceRepository.GetCurrentAttendaceByUserID(idUser);
                if (attendace == null)
                    return Json(new { status = false, titleError = "La caja es necesaria !!!", message = "Caja cerrada, por favor abra una para poder anular ventas" }, JsonRequestBehavior.AllowGet);
                var attendaceVM = _attendaceMapper.MapperModelToViewModel(attendace);

                var sale = _saleRepository.GetById(id);
                _attendaceRepository.DecrementCurrentCash(attendaceVM.idAttendance, sale.accountPayment.Value);
                return Json(new { state = true, message = "La venta ha sido anulado correctamente ... " }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { state = false, message = "Ha ocurrido un error en la solicitud" }, JsonRequestBehavior.AllowGet);
        }
    }
}
