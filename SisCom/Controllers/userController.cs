﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Users;
using SisCom.Structure.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class userController : Controller
    {
        private ComEdbEntities _db;
        private UsersMapper _usersMapper;
        private IUserRepository _userRepository;
        private IDropdownRepository _dropdownRepository;

        public userController
            (
            ComEdbEntities db,
            UsersMapper usersMapper,
            IUserRepository userRepository,
            IDropdownRepository dropdownRepository
            )
        {
            _db = db;
            _usersMapper = usersMapper;
            _userRepository = userRepository;
            _dropdownRepository = dropdownRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(PaginatorGeneric<user, UsersViewModel> paginator)
        {
            List<user> resultsModel = _userRepository.GetUsers();
            var viewmodel = _usersMapper.MapperListModelToViewModel(resultsModel);

            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _userRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _usersMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var usersViewModel = new UsersViewModel();
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, usersViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateUserJson(UsersViewModel usersViewModel)
        {          
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (usersViewModel.name == null ||
                usersViewModel.fatherLastName == null ||
                usersViewModel.motherLastName == null ||
                usersViewModel.dni == null ||
                usersViewModel.password == null ||
                usersViewModel.idRole == 0 ||
                usersViewModel.idBranch == 0)
            {
                return Json(new { formSuccess, msjError });
            } 
            else if (usersViewModel.dni.Length < 8 || usersViewModel.dni.Length > 12)
            {
                msjError = "*El DNI/CE no puede ser mayor a 12 o menor a 8 dígitos";

                return Json(new { formSuccess, msjError });
            }
            try
            {
                usersViewModel.isActive = true;
                usersViewModel.deletedAt = "12/12/12";

                var userModel = _usersMapper.MapperViewModelToModel(usersViewModel);
                _userRepository.Insert(userModel);

                var urlRedirect = Url.Action("Index", "User");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, usersViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }
        }
        public ActionResult Edit(int idUser)
        {
            UsersViewModel usersViewModel = new UsersViewModel();
            usersViewModel.idUser = idUser;

            return View(usersViewModel);
        }
        public ActionResult EditJson(int idUser)
        {
            var user = _userRepository.FindById(idUser);
            var usersViewModel = _usersMapper.MapperModelToViewModel(user);
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, usersViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditUserJson(UsersViewModel usersViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (usersViewModel.name == null ||
                usersViewModel.fatherLastName == null ||
                usersViewModel.motherLastName == null ||
                usersViewModel.dni == null ||
                usersViewModel.password == null ||
                usersViewModel.idRole == 0 ||
                usersViewModel.idBranch == 0)
            {
                return Json(new { formSuccess, msjError });
            }
            else if (usersViewModel.dni.Length != 8)
            {
                msjError = "*El DNI no puede ser mayor o menor a los 8 caracteres";

                return Json(new { formSuccess, msjError });
            }
            try
            {
                var userModel = _usersMapper.MapperViewModelToModel(usersViewModel);
                _userRepository.Update(userModel);

                var urlRedirect = Url.Action("Index", "User");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, usersViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }
        }
        public ActionResult Detail(int idUser)
        {
            UsersViewModel usersViewModel = new UsersViewModel();
            usersViewModel.idUser = idUser;

            return View(usersViewModel);
        }
        public ActionResult Delete(int idUser)
        {
            UsersViewModel usersViewModel = new UsersViewModel();
            usersViewModel.idUser = idUser;

            return View(usersViewModel);
        }
        [HttpPost]
        public ActionResult DeleteUserJson(UsersViewModel usersViewModel)
        {
            usersViewModel.isActive = false;
            usersViewModel.deletedAt = DateTime.Now.ToString();

            var userModel = _usersMapper.MapperViewModelToModel(usersViewModel);
            _userRepository.Update(userModel);
            var urlRedirect = Url.Action("Index", "User");
            var Message = "El usuario ha sido actualizado correctamente";

            return Json(new { Message, urlRedirect, usersViewModel });
        }
        private object GetDropdowns()
        {
            return new
            {
                Branch = _dropdownRepository.Branch(),
                Role = _dropdownRepository.Role(),
            };
        }

        public ActionResult GetUsersToHomeIndex()
        {
            var users = _usersMapper.MapperListModelToViewModel(_userRepository.GetUsers());
            return Json(new { users }, JsonRequestBehavior.AllowGet);
        }
    }
}