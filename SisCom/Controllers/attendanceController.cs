﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Attendance;
using SisCom.Structure.Mappers.Pos;
using SisCom.Structure.ViewModels.Attendance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    public class attendanceController : Controller
    {
        private ComEdbEntities _db;
        private AttendanceMapper _attendanceMapper;
        private IAttendanceRepository _attendanceRepository;
        private IUserRepository _userRepository;
        private IDropdownRepository _dropdownRepository;
        private IPosRepository _posRepository;

        public attendanceController
            (
            ComEdbEntities db,
            AttendanceMapper attendanceMapper,
            IAttendanceRepository attendanceRepository,
            IUserRepository userRepository,
            IDropdownRepository dropdownRepository,
            IPosRepository posRepository
            )
        {
            _db = db;
            _attendanceMapper = attendanceMapper;
            _attendanceRepository = attendanceRepository;
            _userRepository = userRepository;
            _dropdownRepository = dropdownRepository;
            _posRepository = posRepository;
        }
        [Authorize(Roles = "Cajero")]
        public ActionResult Index(int? idAttendance)
        {
            if (idAttendance > 0)
            {
                return RedirectToAction("IndexDetail", new { idAttendance = idAttendance });
            }
            else if (GetUser.getOpenAttendance(User.Identity.Name) > 0)
            {
                return RedirectToAction("IndexDetail", new { idAttendance = GetUser.getOpenAttendance(User.Identity.Name) });
            }
            AttendanceViewModel attendanceViewModel = new AttendanceViewModel();
            attendanceViewModel.idAttendance = Convert.ToInt32(idAttendance);

            return View(attendanceViewModel);
        }
        [Authorize(Roles = "Cajero")]
        public ActionResult IndexJson(int? idAttendance)
        {
            AttendanceViewModel attendanceViewModel = new AttendanceViewModel();
            attendanceViewModel.idAttendance = Convert.ToInt32(idAttendance);
            var dropdowns = GetDropdowns(new RecursosUserLogin().getUserById(User.Identity.Name));
            var userName = new RecursosUserLogin().getUserByName(User.Identity.Name);

            return Json(new { userName, dropdowns, attendanceViewModel }, JsonRequestBehavior.AllowGet);                          
        }
        [Authorize(Roles = "Cajero")]
        public ActionResult IndexDetail(int? idAttendance)
        {
            if (idAttendance == null)
            {
                return RedirectToAction("Index", new { idAttendance = idAttendance });
            }
            AttendanceViewModel attendanceViewModel = new AttendanceViewModel();
            attendanceViewModel.idAttendance = Convert.ToInt32(idAttendance);

            return View(attendanceViewModel);
        }
        [Authorize(Roles = "Cajero")]
        public ActionResult IndexDetailJson(int? idAttendance)
        {
            AttendanceViewModel attendanceViewModel = new AttendanceViewModel();
            attendanceViewModel = _attendanceMapper.MapperModelToViewModel(_attendanceRepository.FindById(idAttendance));

            return Json(new { attendanceViewModel }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Cajero")]
        [HttpPost]
        public ActionResult OpenAttendanceJson(AttendanceViewModel attendanceViewModel)
        {
            bool state = false;
            string Message = "Ocurrió un error al abrir la caja, actualizar la página, si el error persiste, comunicarse con el administrador del Sistema";
            try {
                var urlRedirect = Url.Action("Index", "Sales");
                attendanceViewModel.startAt = DateTime.Now.ToString();
                attendanceViewModel.currentCash = attendanceViewModel.startCash;

                _attendanceRepository.Insert(_attendanceMapper.MapperViewModelToModel(attendanceViewModel));
                state = true;
                Message = "La caja se abrió con éxito";

                return Json(new { state, Message, urlRedirect }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Message, state }, JsonRequestBehavior.AllowGet);
            }          
        }
        [HttpPost]
        public ActionResult CloseAttendanceJson(AttendanceViewModel attendanceViewModel)
        {
            bool state = false;
            string Message = "Ocurrió un error al cerrar la caja, actualizar la página, si el error persiste, comunicarse con el administrador del Sistema";
            try
            {
                var urlRedirect = Url.Action("Index", "Attendance");
                attendanceViewModel.endAt = DateTime.Now.ToString();

                _attendanceRepository.Update(_attendanceMapper.MapperViewModelToModel(attendanceViewModel));
                state = true;
                Message = "La caja se cerro con éxito";

                return Json(new { state, Message, urlRedirect }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { Message, state }, JsonRequestBehavior.AllowGet);
            }
        }
        private object GetDropdowns(int idUser)
        {
            return new
            {
                PosByUser = _dropdownRepository.PosByUser(idUser),
            };
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult RegistroAtenciones()
        {
            return View();
        }
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public JsonResult RegistroAtencionesJson(PaginatorGeneric<attendance, AttendanceViewModel> paginator)
        {
            List<attendance> resultsModel = _attendanceRepository.GetAttendance();
            var viewmodel = _attendanceMapper.MapperListModelToViewModel(resultsModel);

            if (paginator.Page == 0)
            {
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _attendanceRepository.GetAllByPaged(paginator);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _attendanceMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = paginator;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}