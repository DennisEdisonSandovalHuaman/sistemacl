﻿using Newtonsoft.Json;
using SisCom.Authentication;
using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Attendance;
using SisCom.Structure.Mappers.Users;
using SisCom.Structure.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SisCom.Controllers
{
    public class HomeController : Controller
    {
        private ComEdbEntities _db;
        private UsersMapper _usersMapper;
        private IUserRepository _userRepository;
        private IAttendanceRepository _attendanceRepository;
        private IPosRepository _posRepository;
        private AttendanceMapper _attendanceMapper;

        public HomeController
            (
            ComEdbEntities db,
            UsersMapper usersMapper,
            IUserRepository userRepository,
            IAttendanceRepository attendanceRepository,
            IPosRepository posRepository,
            AttendanceMapper attendanceMapper
            )
        {
            _db = db;
            _usersMapper = usersMapper;
            _userRepository = userRepository;
            _attendanceRepository = attendanceRepository;
            _posRepository = posRepository;
            _attendanceMapper = attendanceMapper;
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult Index()
        {
            ViewBag.nombres = new RecursosUserLogin().getUserByName(User.Identity.Name);

            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Cajero"))
                {
                    return RedirectToAction("Index", "attendance");
                }
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult perfil(int idUser)
        {
            UsersViewModel usersViewModel = new UsersViewModel();
            usersViewModel.idUser = idUser;

            return View(usersViewModel);
        }
        public ActionResult PerfilJson(int idUser)
        {
            var user = _userRepository.FindById(idUser);
            var userViewModel = _usersMapper.MapperModelToViewModel(user);

            return Json(new { userViewModel }, JsonRequestBehavior.AllowGet);
        }
        [Authorize(Roles = "Administrador, Cajero")]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Administrador"))
                {
                    return RedirectToAction("Index", "Home");
                } else
                {
                    return RedirectToAction("Index", "attendance");
                }
            }
            int id = new RecursosUserLogin().getUserById(User.Identity.Name);
            user user = _db.user.Find(id);
            string nombres = "", rol = "";
            if (user != null)
            {
                nombres = user.name + " " + user.fatherLastName + " " + user.motherLastName;
                rol = user.role.name;
            }
            ViewBag.nombres = nombres;
            ViewBag.rol = rol;
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {
                    var user = (CustomMembershipUser)Membership.GetUser(model.UserName, false);
                    if (user != null)
                    {
                        CustomSerializeModel userModel = new CustomSerializeModel()
                        {
                            UserId = user.UserId,
                            FirstName = user.FirstName,
                            LastName = user.LastName,
                            RoleName = user.role
                        };

                        string userData = JsonConvert.SerializeObject(userModel);
                        FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket
                            (
                            1, model.UserName, DateTime.Now, DateTime.Now.AddMonths(1), false, userData
                            );

                        string enTicket = FormsAuthentication.Encrypt(authTicket);
                        HttpCookie faCookie = new HttpCookie("Cookie1", enTicket);
                        //faCookie.Expires = model.RememberMe ? DateTime.Now.AddDays(30) : DateTime.Now.AddMinutes(15);
                        Response.Cookies.Add(faCookie);
                    }

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        var data = user.role;
                        if (data.Contains("Administrador"))
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        else
                        {
                            return RedirectToAction("Index", "attendance");
                        }
                    }
                }
            }
            TempData["CustomError"] = "Las credenciales son incorrectas.";
            ModelState.AddModelError(string.Empty, TempData["CustomError"].ToString());

            return View();
        }
        [Authorize(Roles = "Administrador, Cajero")]
        public ActionResult SignOut()
        {
            HttpCookie cookie = new HttpCookie("Cookie1", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie);
            FormsAuthentication.SignOut();

            var attendanceModel = _attendanceRepository.FindById(GetUser.getOpenAttendance(User.Identity.Name));
            if (attendanceModel != null )
            {
                var attendanceViewModel = _attendanceMapper.MapperModelToViewModel(attendanceModel);
                attendanceViewModel.endAt = DateTime.Now.ToString();
                _attendanceRepository.Update(_attendanceMapper.MapperViewModelToModel(attendanceViewModel));
            }
            return RedirectToAction("Login", "Home");
        }
        [Authorize(Roles = "Administrador")]
        public ActionResult GetOpenAttendance()
        {
            var openAttendances = _posRepository.GetAllOpenPos();

            return Json(new { openAttendances }, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult LogOut()
        //{
        //    bool state = false;
        //    string Message = "Hubo un problema al intentar cerrar sesión";
        //    try
        //    {
        //        Session["usuario"] = null;
        //        var urlRedirect = Url.Action("Index", "Home");
        //        state = true;
        //        return Json(new { state, urlRedirect }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new { state, Message }, JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
    public class CustomSerializeModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string RoleName { get; set; }
    }
}