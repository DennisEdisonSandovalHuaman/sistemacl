﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SisCom.Data.IRepositories;
using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Customers;
using SisCom.Structure.ViewModels.Customers;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador, Cajero")]

    public class CustomersController : Controller
    {
        private ComEdbEntities _db;
        private CustomersMapper _customerMapper;
        private ICustomersRepository _customersRepository;
        private IDropdownRepository _dropdownRepository;

        public CustomersController
            (
            ComEdbEntities db,
            ICustomersRepository customersRepository,
            CustomersMapper customerMapper,
            IDropdownRepository dropdownRepository
            )
        {
            _db = db;
            _customerMapper = customerMapper;
            _customersRepository = customersRepository;
            _dropdownRepository = dropdownRepository;
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson(CustomersFilters filters)
        {
            List<customer> resultsModel = _customersRepository.GetCustomers();
            var viewmodel = _customerMapper.MapperListModelToViewModel(resultsModel);

            if (filters.Page == 0)
            {
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _customersRepository.GetAllByPagedFilters(filters);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _customerMapper.MapperPaginatorModelToViewModel(result);
                result.PageCount = (int)Math.Ceiling(pageCount);
                result.ResultsModel = null;
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        public ActionResult CreateJson()
        {
            var customersViewModel = new CustomersViewModel();
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, customersViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CreateCustomerJson(CustomersViewModel customersViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (customersViewModel.name == null || customersViewModel.fatherLastname == null || customersViewModel.motherLastname == null || customersViewModel.dni == null)
            {
                return Json(new { formSuccess, msjError });
            }
            else if (customersViewModel.dni.Length < 8 || customersViewModel.dni.Length > 12)
            {
                msjError = "*El DNI/CE no puede ser mayor a 12 o menor a 8 dígitos";

                return Json(new { formSuccess, msjError });
            }
            try
            {
                customersViewModel.activeDate = DateTime.Now.ToString();
                customersViewModel.deletedAt = "12/12/12";
                customersViewModel.isActive = true;

                var customersModel = _customerMapper.MapperViewModelToModel(customersViewModel);
                _customersRepository.Insert(customersModel);

                var urlRedirect = Url.Action("Index", "Customers");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, customersViewModel });
            }
            catch (Exception)
            {
                return Json(new { state });
            }
        }
        public ActionResult Edit(int idCustomer)
        {
            CustomersViewModel customersViewModel = new CustomersViewModel();
            customersViewModel.idCustomer = idCustomer;

            return View(customersViewModel);
        }
        public ActionResult EditJson(int idCustomer)
        {
            var customer = _customersRepository.FindById(idCustomer);
            var customersViewModel = _customerMapper.MapperModelToViewModel(customer);
            var dropdowns = GetDropdowns();

            return Json(new { dropdowns, customersViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditCustomerJson(CustomersViewModel customersViewModel)
        {
            bool state = false;
            bool formSuccess = false;
            var msjError = "*Por favor, llenar todor los campos";

            if (customersViewModel.name == null || customersViewModel.fatherLastname == null || customersViewModel.motherLastname == null || customersViewModel.dni == null)
            {
                return Json(new { formSuccess, msjError });
            }
            try
            {
                var customerModel = _customerMapper.MapperViewModelToModel(customersViewModel);
                _customersRepository.Update(customerModel);

                var urlRedirect = Url.Action("Index", "Customers");
                state = true;
                formSuccess = true;

                return Json(new { formSuccess, state, urlRedirect, customersViewModel });
            }
            catch (Exception e)
            {
                return Json(new { state });
            }          
        }
        public ActionResult Detail(int idCustomer)
        {
            CustomersViewModel customersViewModel = new CustomersViewModel();
            customersViewModel.idCustomer = idCustomer;

            return View(customersViewModel);
        }
        public ActionResult Delete(int idCustomer)
        {
            CustomersViewModel customersViewModel = new CustomersViewModel();
            customersViewModel.idCustomer = idCustomer;

            return View(customersViewModel);
        }
        [HttpPost]
        public ActionResult DeleteCustomerJson(CustomersViewModel customersViewModel)
        {
            customersViewModel.isActive = false;
            customersViewModel.deletedAt = DateTime.Now.ToString();
            var customerModel = _customerMapper.MapperViewModelToModel(customersViewModel);
            _customersRepository.Update(customerModel);
            var urlRedirect = Url.Action("Index", "Customers");
            var Message = "El cliente ha sido desactivado correctamente";

            return Json(new { Message, urlRedirect, customersViewModel });
        }

        public ActionResult GetActivesAndInactivesCustomers()
        {
            var customers = _customersRepository.GetCustomers();
            var allCustomers = customers.Count();
            var activesCustomers = customers.Where(customer => customer.isActive).Count();
            var inactivesCustomers = customers.Where(customer => !customer.isActive).Count();

            return Json(new { allCustomers, activesCustomers, inactivesCustomers }, JsonRequestBehavior.AllowGet);
        }
        private object GetDropdowns()
        {
            return new
            {
                WorkUnit = _dropdownRepository.WorkUnit(),
                Company = _dropdownRepository.Company(),
            };
        }
    }
}
