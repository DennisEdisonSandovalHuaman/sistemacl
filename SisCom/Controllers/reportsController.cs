﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Attendance;
using SisCom.Structure.Mappers.Company;
using SisCom.Structure.Mappers.Customers;
using SisCom.Structure.Mappers.Pos;
using SisCom.Structure.Mappers.Products;
using SisCom.Structure.Mappers.Sales;
using SisCom.Structure.Mappers.ServiceType;
using SisCom.Structure.Mappers.WorkUnit;
using SisCom.Structure.ViewModels.Reports;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador, Cajero")]
    public class reportsController : Controller
    {
        private SaleMapper _saleMapper;
        private SaleDetailMapper _saleDetailMapper;
        private CustomersMapper _customerMapper;
        private ProductMapper _productMapper;
        private ServiceTypeMapper _serviceTypeMapper;
        private PosMapper _posMapper;
        private WorkUnitMapper _workUnitMapper;
        private AttendanceMapper _attendaceMapper;
        private CompanyMapper _companyMapper;
        private IGenericRepository<sale_detail> _detailSaleRepository;
        private ISaleRepository _saleRepository;
        private IProductRepository _productRepository;
        private IAttendanceRepository _attendaceRepository;
        private ICustomersRepository _customerRepository;
        private IServiceTypeRepository _serviceTypeRepository;
        private IPosRepository _posRepository;
        private IWorkUnitRepository _workUnitRepository;
        private ICompanyRepository _companyRepository;

        public reportsController(CompanyMapper companyMapper, ICompanyRepository companyRepository, WorkUnitMapper workUnitMapper, IWorkUnitRepository workUnitRepository, SaleMapper saleMapper, SaleDetailMapper saleDetailMapper, CustomersMapper customerMapper, ProductMapper productMapper, ServiceTypeMapper serviceTypeMapper, PosMapper posMapper, AttendanceMapper attendaceMapper, IGenericRepository<sale_detail> detailSaleRepository, ISaleRepository saleRepository, IProductRepository productRepository, IAttendanceRepository attendaceRepository, ICustomersRepository customerRepository, IServiceTypeRepository serviceTypeRepository, IPosRepository posRepository)
        {
            _companyMapper = companyMapper;
            _companyRepository = companyRepository;
            _workUnitMapper = workUnitMapper;
            _workUnitRepository = workUnitRepository;
            _saleMapper = saleMapper;
            _saleDetailMapper = saleDetailMapper;
            _customerMapper = customerMapper;
            _productMapper = productMapper;
            _serviceTypeMapper = serviceTypeMapper;
            _posMapper = posMapper;
            _attendaceMapper = attendaceMapper;
            _detailSaleRepository = detailSaleRepository;
            _saleRepository = saleRepository;
            _productRepository = productRepository;
            _attendaceRepository = attendaceRepository;
            _customerRepository = customerRepository;
            _serviceTypeRepository = serviceTypeRepository;
            _posRepository = posRepository;
        }

        public ActionResult Sales()
        {
            return View();
        }
        public ActionResult SalesJson()
        {
            var listServiceTypes = _serviceTypeMapper.MapperListModelToViewModel(_serviceTypeRepository.GetServiceType());
            var listPos = _posMapper.MapperListModelToViewModel(_posRepository.GetPos());
            var listWorkUnits = _workUnitMapper.MapperListModelToViewModel(_workUnitRepository.GetWorkUnit());
            var listCompanies = _companyMapper.MapperListModelToViewModel(_companyRepository.GetCompany());

            return Json(new { listServiceTypes, listPos, listWorkUnits, listCompanies }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FastSalesReportJson(FastSaleFilterViewModel filters)
        {
            if (filters.Page == 0)
            {
                var list = filters;

                return Json(new { list }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = _saleRepository.GetEmployeSalesFastToReportByPagedFilters(filters);
                var pageCount = (double)result.TotalRow / result.PageSize;

                result.Results = _saleMapper.PaginatorConvertSaleDetailModelToReportViewModels(result);
                result.PageCount = (int)Math.Ceiling(pageCount);

                result.ResultsModel = null;

                var resultW = _saleRepository.GetWorkerSalesFastToReportByPagedFilters(filters);
                var pageCountW = (double)resultW.TotalRowW / resultW.PageSizeW;

                resultW.ResultsW = _saleMapper.PaginatorConvertSaleDetailModelToReportViewModels2(resultW);
                resultW.PageCountW = (int)Math.Ceiling(pageCountW);

                resultW.ResultsModelW = null;

                var list = filters;
                var employSales = filters.Results;

                var listW = filters;
                var workerSales = filters.ResultsW;

                var totalPriceEmploy = result.totalPriceEmploy;
                var totalAccountPaymentEmploy = result.totalAccountPaymentEmploy;

                var totalPriceWorker = resultW.totalPriceWorker;
                var totalAccountPaymentWorker = resultW.totalAccountPaymentWorker;

                return Json(new { list, listW, employSales, workerSales, totalPriceEmploy, totalAccountPaymentEmploy, totalPriceWorker, totalAccountPaymentWorker }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public FileResult DownLoadFastExcel(FastSaleFilterViewModel filters)
        {
            var salesDetails = _saleRepository.GetSalesFastToReport(filters);
            var salesToReportVM = _saleMapper.ConvertSaleDetailModelToSaleToReportViewModels(salesDetails);
            ReportSaleExcel excelHelper = new ReportSaleExcel();
            byte[] fileBytes = excelHelper.CreateExcelAndReturnBytesArray(salesToReportVM, filters.initDate, filters.endDate);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, "Reporte.xlsx");
        }

        public ActionResult Products()
        {
            return View();
        }
    }
}