﻿using SisCom.Models;
using SisCom.Structure.Mappers.Organization;
using System.Web.Mvc;
using SisCom.Data.IRepositories;
using SisCom.Structure.ViewModels.Organization;
using System.Collections.Generic;

namespace SisCom.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class organizationsController : Controller
    {
        private ComEdbEntities _db;
        private OrganizationMapper _organizationMapper;
        private IOrganizationRepository _organizationRepository;

        public organizationsController
            (
            ComEdbEntities db,
            OrganizationMapper organizationMapper,
            IOrganizationRepository organizationRepository
            )
        {
            _db = db;
            _organizationMapper = organizationMapper;
            _organizationRepository = organizationRepository;
        } 
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult IndexJson()
        {
            List<organization> resultsModel = _organizationRepository.GetOrganizations();
            var viewmodel = _organizationMapper.MapperListModelToViewModel(resultsModel);

            return Json(viewmodel, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int idOrganization)
        {
            OrganizationViewModel organizationViewModel = new OrganizationViewModel();
            organizationViewModel.idOrganization = idOrganization;

            return View(organizationViewModel);
        }
        public ActionResult EditJson(int idOrganization)
        {
            var organization = _organizationRepository.FindById(idOrganization);
            var organizationViewModel = _organizationMapper.MapperModelToViewModel(organization);

            return Json(new { organizationViewModel }, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult EditOrganizationJson(OrganizationViewModel organizationViewModel)
        {
            var organizationModel = _organizationMapper.MapperViewModelToModel(organizationViewModel);
            _organizationRepository.Update(organizationModel);
            var urlRedirect = Url.Action("Index", "Organizations");
            var Message = "La organización ha sido actualizada correctamente";

            return Json(new { Message, urlRedirect, organizationViewModel });
        }
        public ActionResult Detail(int idOrganization)
        {
            OrganizationViewModel organizationViewModel = new OrganizationViewModel();
            organizationViewModel.idOrganization = idOrganization;

            return View(organizationViewModel);
        }
    }
}