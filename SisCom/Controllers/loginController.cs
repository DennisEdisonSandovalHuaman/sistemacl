﻿using SisCom.Extensions;
using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SisCom.Controllers
{
    public class loginController : Controller
    {
        private ComEdbEntities _db;

        public loginController
            (
            ComEdbEntities db
            )
        {
            _db = db;
        }

        public ActionResult login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult loginJson(string dni, string password)
        {
            var Message = "";
            bool state = false;
            try
            {              
                int session = Login.Session(dni, password);
                if (session == 0)
                {
                    Message = "DNI o contraseña incorrectos";
                }
                var usuario = _db.user.Where(x => x.dni == dni).Select(x => x).First();
                //Session["usuario"] = usuario.name + " " + usuario.fatherLastName + " " + usuario.motherLastName;
                Session["usuario"] = usuario.idUser;
                var urlRedirect = Url.Action("Index", "Home");
                state = true;

                return Json(new { state, session, Message, urlRedirect });
            }
            catch (Exception e)
            {
                return Json(new { state });
            }
        }
    }
}