﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Supplies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Filters
{
    public class SuppliesFilters : PaginatorGeneric<supplies, SuppliesViewModel>
    {
        public string FilterByName { get; set; }
        public string FilterByBranch { get; set; }
    }
}