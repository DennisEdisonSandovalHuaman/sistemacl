﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Filters
{
    public class CustomersFilters : PaginatorGeneric<customer, CustomersViewModel>
    {
        public string FilterByName { get; set; }
        public string FilterByFatherLastName { get; set; }
        public string FilterByMotherLastName { get; set; }
        public string FilterByDNI { get; set; }
        public string FilterByCompany{ get; set; }
    }
}