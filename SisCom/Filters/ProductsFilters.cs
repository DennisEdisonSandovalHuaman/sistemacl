﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Filters
{
    public class ProductsFilters : PaginatorGeneric<product, ProductViewModel>
    {
        public string FilterByName { get; set; }
        public string FilterByServiceType { get; set; }
    }
}