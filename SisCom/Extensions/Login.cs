﻿using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Extensions
{
    public class Login
    {
        public static int Session(string dni, string password)
        {
            ComEdbEntities _db = new ComEdbEntities();
            int usuarioId = 0;
            var query = _db.user.Where(x => x.dni == dni && x.password == password).Select(x => x);

            if (query.Count() > 0)
            {
                usuarioId = query.First().idUser;
            }

            return usuarioId; 
        }
    }
}