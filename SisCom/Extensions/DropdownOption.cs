﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Extensions
{
    public class DropdownOption
    {
        public DropdownOption()
        {

        }

        public int Value { get; set; }
        public string Text { get; set; }
        public int? ParentId { get; set; }
        public int? CategoryId { get; set; }
        public string TextComplete { get; set; }
        public string Discriminator { get; set; }
        public bool RequiredDescription = false;
    }
}