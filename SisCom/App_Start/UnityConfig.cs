using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using SisCom.Data.IRepositories;
using SisCom.Data.Repositories;

namespace SisCom
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            container.RegisterType(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            container.RegisterType(typeof(ISaleRepository), typeof(SaleRepository));
            container.RegisterType(typeof(IAttendanceRepository), typeof(AttendanceRepository));
            container.RegisterType(typeof(IProductRepository), typeof(ProductRepository));
            container.RegisterType(typeof(ICustomersRepository), typeof(CustomersRepository));
            container.RegisterType(typeof(IDropdownRepository), typeof(DropdownRepository));
            container.RegisterType(typeof(IOrganizationRepository), typeof(OrganizationRepository));
            container.RegisterType(typeof(IServiceTypeRepository), typeof(ServiceTypeRepository));
            container.RegisterType(typeof(IWorkUnitRepository), typeof(WorkUnitRepository));
            container.RegisterType(typeof(IUserRepository), typeof(UserRepository));
            container.RegisterType(typeof(IPosRepository), typeof(PosRepository));
            container.RegisterType(typeof(IBranchRepository), typeof(BranchRepository));
            container.RegisterType(typeof(IRoleRepository), typeof(RoleRepository));
            container.RegisterType(typeof(ISuppliesRepository), typeof(SuppliesRepository));
            container.RegisterType(typeof(ICompanyRepository), typeof(CompanyRepository));
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}