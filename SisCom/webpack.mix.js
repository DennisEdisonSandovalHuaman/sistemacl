let mix = require('laravel-mix');
//Layout
mix.js('Content/Layout/js/layout.js', 'dist/js');
mix.sass('Content/Layout/scss/app.scss', 'dist/css');
mix.sass('Content/Layout/scss/preloader/preloader.scss', 'dist/css/preloader');

//product
mix.js('Content/Layout/js/cruds/product/index.js', 'dist/js/product');
mix.js('Content/Layout/js/cruds/product/create.js', 'dist/js/product');
mix.js('Content/Layout/js/cruds/product/edit.js', 'dist/js/product');
mix.js('Content/Layout/js/cruds/product/detail.js', 'dist/js/product');

//supplies
mix.js('Content/Layout/js/cruds/supplies/index.js', 'dist/js/supplies');
mix.js('Content/Layout/js/cruds/supplies/create.js', 'dist/js/supplies');
mix.js('Content/Layout/js/cruds/supplies/edit.js', 'dist/js/supplies');
mix.js('Content/Layout/js/cruds/supplies/detail.js', 'dist/js/supplies');

//employed
mix.js('Content/Layout/js/cruds/employed/index.js', 'dist/js/employed');
mix.js('Content/Layout/js/cruds/employed/create.js', 'dist/js/employed');

//cashdesk
mix.js('Content/Layout/js/cruds/cashdesk/index.js', 'dist/js/cashdesk');
mix.js('Content/Layout/js/cruds/cashdesk/create.js', 'dist/js/cashdesk');

//sale
mix.js('Content/Layout/js/cruds/sale/index.js', 'dist/js/sale');
mix.js('Content/Layout/js/cruds/sale/create.js', 'dist/js/sale');
mix.js('Content/Layout/js/cruds/sale/fastCreate.js', 'dist/js/sale');
mix.js('Content/Layout/js/cruds/sale/edit.js', 'dist/js/sale');
mix.js('Content/Layout/js/cruds/sale/detail.js', 'dist/js/sale');

//order
mix.js('Content/Layout/js/cruds/order/index.js', 'dist/js/order');
mix.js('Content/Layout/js/cruds/order/create.js', 'dist/js/order');
mix.js('Content/Layout/js/cruds/order/edit.js', 'dist/js/order');
mix.js('Content/Layout/js/cruds/order/detail.js', 'dist/js/order');

//customer
mix.js('Content/Layout/js/cruds/customer/index.js', 'dist/js/customer');
mix.js('Content/Layout/js/cruds/customer/create.js', 'dist/js/customer');
mix.js('Content/Layout/js/cruds/customer/edit.js', 'dist/js/customer');

//report
mix.js('Content/Layout/js/cruds/report/sales.js', 'dist/js/report');
mix.js('Content/Layout/js/cruds/report/products.js', 'dist/js/report');
mix.sass('Content/Layout/scss/report/report.scss', 'dist/css/report');

//users
mix.js('Content/Layout/js/cruds/user/index.js', 'dist/js/user');
mix.js('Content/Layout/js/cruds/user/create.js', 'dist/js/user');
mix.js('Content/Layout/js/cruds/user/edit.js', 'dist/js/user');
mix.js('Content/Layout/js/cruds/user/details.js', 'dist/js/user');

//users
mix.js('Content/Layout/js/cruds/home/home.js', 'dist/js/home');

//organizations
mix.js('Content/Layout/js/cruds/organizations/index.js', 'dist/js/organizations');
mix.js('Content/Layout/js/cruds/organizations/edit.js', 'dist/js/organizations');

//pos
mix.js('Content/Layout/js/cruds/pos/index.js', 'dist/js/pos');
mix.js('Content/Layout/js/cruds/pos/create.js', 'dist/js/pos');
mix.js('Content/Layout/js/cruds/pos/edit.js', 'dist/js/pos');

//serviceType
mix.js('Content/Layout/js/cruds/serviceType/index.js', 'dist/js/serviceType');
mix.js('Content/Layout/js/cruds/serviceType/create.js', 'dist/js/serviceType');
mix.js('Content/Layout/js/cruds/serviceType/edit.js', 'dist/js/serviceType');

//workUnit
mix.js('Content/Layout/js/cruds/workUnit/index.js', 'dist/js/workUnit');
mix.js('Content/Layout/js/cruds/workUnit/create.js', 'dist/js/workUnit');
mix.js('Content/Layout/js/cruds/workUnit/edit.js', 'dist/js/workUnit');

//branch
mix.js('Content/Layout/js/cruds/branch/index.js', 'dist/js/branch');
mix.js('Content/Layout/js/cruds/branch/create.js', 'dist/js/branch');
mix.js('Content/Layout/js/cruds/branch/edit.js', 'dist/js/branch');

//login
mix.js('Content/Layout/js/login/login.js', 'dist/js/login');

//home
mix.js('Content/Layout/js/home/perfil.js', 'dist/js/home');
mix.js('Content/Layout/js/home/login.js', 'dist/js/home');

//attenance
mix.js('Content/Layout/js/cruds/attendance/index.js', 'dist/js/attendance');
mix.js('Content/Layout/js/cruds/attendance/registroAtenciones.js', 'dist/js/attendance');
mix.js('Content/Layout/js/cruds/attendance/indexDetail.js', 'dist/js/attendance');

//company
mix.js('Content/Layout/js/cruds/company/index.js', 'dist/js/company');
mix.js('Content/Layout/js/cruds/company/create.js', 'dist/js/company');
mix.js('Content/Layout/js/cruds/company/edit.js', 'dist/js/company');