﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Users;
using SisCom.Structure.ViewModels.Pos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.Pos
{
    public class PosMapper
    {
        IUserRepository _userRepository;
        IBranchRepository _branchRepository;
        IRoleRepository _roleRepository;
        private ComEdbEntities _db;

        public PosMapper
        (
        IUserRepository userRepository,
        IBranchRepository branchRepository,
        IRoleRepository roleRepository,
        ComEdbEntities db
        )
        {
            _userRepository = userRepository;
            _branchRepository = branchRepository;
            _roleRepository = roleRepository;
            _db = db;
        }
        public pos MapperViewModelToModel(PosViewModel _posViewModel)
        {
            return new pos
            {
                idPos = _posViewModel.idPos,
                idUser = _posViewModel.idUser,
                name = _posViewModel.name,
                isActive = _posViewModel.isActive,
                deletedAt = Convert.ToDateTime(_posViewModel.deletedAt),
            };
        }
        public PosViewModel MapperModelToViewModel(pos _pos)
        {
            PosViewModel _posViewModel = new PosViewModel();
            _posViewModel.idPos = _pos.idPos;
            _posViewModel.idUser = _pos.idUser;
            _posViewModel.name = _pos.name;
            _posViewModel.isActive = _pos.isActive;
            _posViewModel.deletedAt = _pos.deletedAt.ToString();
            _posViewModel.User = new UsersMapper(_branchRepository, _roleRepository).MapperModelToViewModel(_userRepository.FindById(Convert.ToInt32(_pos.idUser)));

            return _posViewModel;
        }
        public List<PosViewModel> MapperListModelToViewModel(List<pos> _pos)
        {
            return _pos.Select(x => new PosViewModel
            {
                idPos = x.idPos,
                idUser = x.idUser,
                name = x.name,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
                User = new UsersMapper(_branchRepository, _roleRepository).MapperModelToViewModel(_userRepository.FindById(Convert.ToInt32(x.idUser)))
            }).ToList();
        }
        public List<PosViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<pos, PosViewModel> result)
        {
            return result.ResultsModel.Select(x => new PosViewModel
            {
                idPos = x.idPos,
                idUser = x.idUser,
                name = x.name,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
                User = new UsersMapper(_branchRepository, _roleRepository).MapperModelToViewModel(_userRepository.FindById(Convert.ToInt32(x.idUser)))
            }).ToList();
        }
    }
}