﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Models;
using SisCom.Structure.ViewModels.Products;
using SisCom.Structure.Mappers.Products;
using SisCom.Data.IRepositories;
using SisCom.Structure.Mappers.ServiceType;
using SisCom.Structure.Mappers.BrachMapper;
using SisCom.Data.Repositories;
using SisCom.Helpers;

namespace SisCom.Structure.Mappers.Products
{
    public class ProductMapper
    {
        IServiceTypeRepository _serviceTypeRepository;
        ComEdbEntities db = new ComEdbEntities();
        IBranchRepository _branchRepository;

        public ProductMapper(IServiceTypeRepository serviceTypeRepository, IBranchRepository branchRepository)
        {
            _serviceTypeRepository = serviceTypeRepository;
            _branchRepository = new BranchRepository(db);
        }
        public product MapperViewModelToModel(ProductViewModel _productViewModel)
        {
            return new product
            {
                idProduct = _productViewModel.idProduct,
                name = _productViewModel.name,
                price = _productViewModel.price,
                grantedPrice = _productViewModel.grantedPrice,
                idServiceType = _productViewModel.idServiceType,
                idBranch = _productViewModel.idBranch,
                currentStock = _productViewModel.currentStock,
                minimunStock = _productViewModel.minimunStock,
                withStock = _productViewModel.withStock,
                isActive = _productViewModel.isActive,
            };
        }
        public ProductViewModel MapperModelToViewModel(product _product)
        {
            ProductViewModel _productViewModel = new ProductViewModel();
            _productViewModel.idProduct = _product.idProduct;
            _productViewModel.name = _product.name;
            _productViewModel.price = _product.price;
            _productViewModel.grantedPrice = _product.grantedPrice;
            _productViewModel.idServiceType = _product.idServiceType;
            _productViewModel.idBranch = _product.idBranch;
            _productViewModel.currentStock = _product.currentStock;
            _productViewModel.minimunStock = _product.minimunStock;
            _productViewModel.withStock = _product.withStock;
            _productViewModel.isActive = _product.isActive;
            _productViewModel.ServiceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.FindById(Convert.ToInt32(_product.idServiceType)));
            _productViewModel.Branch = new BranchMapper().MapperModelToViewModel(_branchRepository.FindById(Convert.ToInt32(_product.idBranch)));
            return _productViewModel;
        }
        public List<ProductViewModel> MapperListModelToViewModel(List<product> _product)
        {
            return _product.Select(x => new ProductViewModel
            {
                idProduct = x.idProduct,
                name = x.name,
                price = x.price,
                grantedPrice = x.grantedPrice,
                idServiceType = x.idServiceType,
                idBranch = x.idBranch,
                currentStock = x.currentStock,
                minimunStock = x.minimunStock,
                withStock = x.withStock,
                isActive = x.isActive,
                ServiceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.FindById(Convert.ToInt32(x.idServiceType))),
                Branch = new BranchMapper().MapperModelToViewModel(_branchRepository.FindById(Convert.ToInt32(x.idBranch))),
            }).ToList();
        }
        public List<ProductViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<product, ProductViewModel> result)
        {
            return result.ResultsModel.Select(x => new ProductViewModel
            {
                idProduct = x.idProduct,
                name = x.name,
                price = x.price,
                grantedPrice = x.grantedPrice,
                idServiceType = x.idServiceType,
                idBranch = x.idBranch,
                currentStock = x.currentStock,
                minimunStock = x.minimunStock,
                withStock = x.withStock,
                isActive = x.isActive,
                ServiceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.FindById(Convert.ToInt32(x.idServiceType))),
                Branch = new BranchMapper().MapperModelToViewModel(_branchRepository.FindById(Convert.ToInt32(x.idBranch))),
            }).ToList();
        }
    }
}