﻿using SisCom.Models;
using SisCom.Structure.ViewModels.Organization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Structure.Mappers.Organization
{
    public class OrganizationMapper
    {
        public organization MapperViewModelToModel(OrganizationViewModel _organizationViewModel)
        {
            return new organization
            {
                idOrganization = _organizationViewModel.idOrganization,
                name = _organizationViewModel.name,
                logo = _organizationViewModel.logo,
                address = _organizationViewModel.address,            
            };
        }
        public OrganizationViewModel MapperModelToViewModel(organization _organization)
        {
            OrganizationViewModel _organizationViewModel = new OrganizationViewModel();
            _organizationViewModel.idOrganization = _organization.idOrganization;
            _organizationViewModel.name = _organization.name;
            _organizationViewModel.logo = _organization.logo;
            _organizationViewModel.address = _organization.address;
      
            return _organizationViewModel;
        }
        public List<OrganizationViewModel> MapperListModelToViewModel(List<organization> _organization)
        {
            return _organization.Select(x => new OrganizationViewModel
            {
                idOrganization = x.idOrganization,
                name = x.name,
                logo = x.logo,
                address = x.address,
            }).ToList();
        }
    }
}