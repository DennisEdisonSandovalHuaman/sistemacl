﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.BrachMapper;
using SisCom.Structure.ViewModels.Supplies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.Supplies
{
    public class SuppliesMapper
    {

        IBranchRepository _branchRepository;

        public SuppliesMapper(IBranchRepository branchRepository)
        {
            _branchRepository = branchRepository;
        }

        public List<SuppliesViewModel> MapperListModelToViewModel(List<supplies> supplies)
        {
            return supplies.Select(supply => MapperModelToViewModel(supply)).ToList();
        }
        public SuppliesViewModel MapperModelToViewModel(supplies supply)
        {
            return new SuppliesViewModel()
            {
                idSupplies = supply.idSupplies,
                idBranch = supply.idBranch,
                name = supply.name,
                price = supply.price.Value,
                stock = supply.stock.Value,
                weight = supply.weight,
                isActive = supply.isActive.Value,
                minStock = supply.minStock.Value,
                minWeight = supply.minPrice.Value,
                branch = new BranchMapper().MapperModelToViewModel(_branchRepository.GetById(supply.idBranch)),
            };
        }
        public supplies MapperViewModelToModel(SuppliesViewModel supplyVM)
        {
            return new supplies()
            {
                idSupplies = supplyVM.idSupplies,
                idBranch = supplyVM.idBranch,
                name = supplyVM.name,
                price = supplyVM.price,
                stock = supplyVM.stock,
                weight = supplyVM.weight,
                isActive = supplyVM.isActive,
                minStock = supplyVM.minStock,
                minPrice = supplyVM.minWeight,
            };
        }
        public List<SuppliesViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<supplies, SuppliesViewModel> result)
        {
            return result.ResultsModel.Select(supply => MapperModelToViewModel(supply)).ToList();
        }
    }
}