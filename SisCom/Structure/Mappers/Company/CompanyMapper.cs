﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.Company
{
    public class CompanyMapper
    {
        public company MapperViewModelToModel(CompanyViewModel _companyViewModel)
        {
            return new company
            {
                idCompany = _companyViewModel.idCompany,
                name = _companyViewModel.name,
                isActive = _companyViewModel.isActive,
            };
        }
        public CompanyViewModel MapperModelToViewModel(company _company)
        {
            CompanyViewModel _companyViewModel = new CompanyViewModel();
            _companyViewModel.idCompany = Convert.ToInt32(_company.idCompany);
            _companyViewModel.name = _company.name;
            _companyViewModel.isActive = _company.isActive;
            _companyViewModel.deletedAt = _company.deletedAt.ToString();

            return _companyViewModel;
        }
        public List<CompanyViewModel> MapperListModelToViewModel(List<company> _company)
        {
            return _company.Select(x => new CompanyViewModel
            {
                idCompany = Convert.ToInt32(x.idCompany),
                name = x.name,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
            }).ToList();
        }
        public List<CompanyViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<company, CompanyViewModel> result)
        {
            return result.ResultsModel.Select(x => new CompanyViewModel
            {
                idCompany = Convert.ToInt32(x.idCompany),
                name = x.name,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
            }).ToList();
        }
    }
}