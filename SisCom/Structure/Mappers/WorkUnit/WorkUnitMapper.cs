﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.WorkUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.WorkUnit
{
    public class WorkUnitMapper
    {
        public work_unit MapperViewModelToModel(WorkUnitViewModel _workUnitViewModel)
        {
            return new work_unit
            {
                idWorkUnit = _workUnitViewModel.idWorkUnit,
                name = _workUnitViewModel.name,
                isActive = _workUnitViewModel.isActive,
                deletedAt = Convert.ToDateTime(_workUnitViewModel.deletedAt),
            };
        }
        public WorkUnitViewModel MapperModelToViewModel(work_unit _work_unit)
        {
            WorkUnitViewModel _workUnitViewModel = new WorkUnitViewModel();
            _workUnitViewModel.idWorkUnit = Convert.ToInt32(_work_unit.idWorkUnit);
            _workUnitViewModel.name = _work_unit.name;
            _workUnitViewModel.isActive = _work_unit.isActive;
            _workUnitViewModel.deletedAt = _work_unit.deletedAt.ToString();

            return _workUnitViewModel;
        }
        public List<WorkUnitViewModel> MapperListModelToViewModel(List<work_unit> _work_unit)
        {
            return _work_unit.Select(x => new WorkUnitViewModel
            {
                idWorkUnit = Convert.ToInt32(x.idWorkUnit),
                name = x.name,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
            }).ToList();
        }
        public List<WorkUnitViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<work_unit, WorkUnitViewModel> result)
        {
            return result.ResultsModel.Select(x => new WorkUnitViewModel
            {
                idWorkUnit = Convert.ToInt32(x.idWorkUnit),
                name = x.name,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
            }).ToList();
        }
    }
}