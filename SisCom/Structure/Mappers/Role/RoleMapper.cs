﻿using SisCom.Models;
using SisCom.Structure.ViewModels.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.Role
{
    public class RoleMapper
    {
        public role MapperViewModelToModel(RoleViewModel _roleViewModel)
        {
            return new role
            {
                idRole = _roleViewModel.idRole,
                name = _roleViewModel.name,
            };
        }
        public RoleViewModel MapperModelToViewModel(role _role)
        {
            RoleViewModel _roleViewModel = new RoleViewModel();
            _roleViewModel.idRole = _role.idRole;
            _roleViewModel.name = _role.name;

            return _roleViewModel;
        }
    }
}