﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.BrachMapper;
using SisCom.Structure.Mappers.Role;
using SisCom.Structure.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.Users
{
    public class UsersMapper
    {       
        IBranchRepository _branchRepository;
        IRoleRepository _roleRepository;

        public UsersMapper
            (
            IBranchRepository branchRepository,
            IRoleRepository roleRepository
            )
        {
            _branchRepository = branchRepository;
            _roleRepository = roleRepository;
        }
        public user MapperViewModelToModel(UsersViewModel _usersViewModel)
        {
            return new user
            {
                idUser = _usersViewModel.idUser,
                dni = _usersViewModel.dni,
                password = _usersViewModel.password,
                name = _usersViewModel.name,
                fatherLastName = _usersViewModel.fatherLastName,
                motherLastName = _usersViewModel.motherLastName,
                isActive = _usersViewModel.isActive,
                idBranch = _usersViewModel.idBranch,
                idRole = _usersViewModel.idRole,
                deletedAt = Convert.ToDateTime(_usersViewModel.deletedAt),
            };
        }
        public UsersViewModel MapperModelToViewModel(user _user)
        {
            UsersViewModel _usersViewModel = new UsersViewModel();
            _usersViewModel.idUser = _user.idUser;
            _usersViewModel.dni = _user.dni;
            _usersViewModel.password = _user.password;
            _usersViewModel.name = _user.name;
            _usersViewModel.fatherLastName = _user.fatherLastName;
            _usersViewModel.motherLastName = _user.motherLastName;
            _usersViewModel.isActive = _user.isActive;
            _usersViewModel.deletedAt = _user.deletedAt.ToString();
            _usersViewModel.idBranch = _user.idBranch;
            _usersViewModel.idRole = _user.idRole;
            _usersViewModel.Branch = new BranchMapper().MapperModelToViewModel(_branchRepository.FindById(Convert.ToInt32(_user.idBranch)));
            _usersViewModel.Role = new RoleMapper().MapperModelToViewModel(_roleRepository.FindById(Convert.ToInt32(_user.idRole)));

            return _usersViewModel;
        }
        public List<UsersViewModel> MapperListModelToViewModel(List<user> _user)
        {
            return _user.Select(x => new UsersViewModel
            {
                idUser = x.idUser,
                dni = x.dni,
                password = x.password,
                name = x.name,
                fatherLastName = x.fatherLastName,
                motherLastName = x.motherLastName,
                idRole = x.idRole,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
                idBranch = x.idBranch,
                Branch = new BranchMapper().MapperModelToViewModel(_branchRepository.FindById(Convert.ToInt32(x.idBranch))),
                Role = new RoleMapper().MapperModelToViewModel(_roleRepository.FindById(Convert.ToInt32(x.idRole)))
            }).ToList();
        }
        public List<UsersViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<user, UsersViewModel> result)
        {
            return result.ResultsModel.Select(x => new UsersViewModel
            {
                idUser = x.idUser,
                dni = x.dni,
                password = x.password,
                name = x.name,
                fatherLastName = x.fatherLastName,
                motherLastName = x.motherLastName,
                idRole = x.idRole,
                isActive = x.isActive,
                deletedAt = x.deletedAt.ToString(),
                idBranch = x.idBranch,
                Branch = new BranchMapper().MapperModelToViewModel(_branchRepository.FindById(Convert.ToInt32(x.idBranch))),
                Role = new RoleMapper().MapperModelToViewModel(_roleRepository.FindById(Convert.ToInt32(x.idRole)))
            }).ToList();
        }
    }
}