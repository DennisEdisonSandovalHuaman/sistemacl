﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Models;
using SisCom.Structure.ViewModels.Sales;
using SisCom.Structure.ViewModels.Reports;
using SisCom.Structure.Mappers.Customers;
using SisCom.Structure.Mappers.ServiceType;
using SisCom.Structure.ViewModels.Customers;
using SisCom.Data.IRepositories;
using SisCom.Structure.Mappers.Attendance;
using SisCom.Structure.Mappers.Pos;
using SisCom.Helpers;

namespace SisCom.Structure.Mappers.Sales
{
    public class SaleMapper
    {
        ICustomersRepository _customersRepository;
        IWorkUnitRepository _workUnitRepository;
        ISaleRepository _saleRepository;
        IServiceTypeRepository _serviceTypeRepository;
        IProductRepository _productRepository;
        ICompanyRepository _companyRepository;
        IAttendanceRepository _attendanceRepository;
        PosMapper _posMapper;
        AttendanceMapper _attendanceMapper;
        CustomersMapper _customersMapper;
        SaleDetailMapper _saleDetailMapper;

        public SaleMapper
            (
            ICustomersRepository customersRepository, 
            IWorkUnitRepository workUnitRepository, 
            ISaleRepository saleRepository, 
            IServiceTypeRepository serviceTypeRepository, 
            IProductRepository productRepository, 
            IAttendanceRepository attendanceRepository, 
            PosMapper posMapper, 
            AttendanceMapper attendanceMapper, 
            ICompanyRepository companyRepository,
            CustomersMapper customersMapper,
            SaleDetailMapper saleDetailMapper
            )
        {
            _customersRepository = customersRepository;
            _workUnitRepository = workUnitRepository;
            _saleRepository = saleRepository;
            _serviceTypeRepository = serviceTypeRepository;
            _productRepository = productRepository;
            _attendanceRepository = attendanceRepository;
            _posMapper = posMapper;
            _attendanceMapper = attendanceMapper;
            _companyRepository = companyRepository;
            _customersMapper = customersMapper;
            _saleDetailMapper = saleDetailMapper;
        }

        public sale ConvertViewModelToModel(SaleCreateViewModel saleCreateViewModel)
        {
            sale saleModel = new sale()
            {
                accountPayment = saleCreateViewModel.accountPayment,
                idSale = saleCreateViewModel.idSale,
                idCustomer = saleCreateViewModel.idCustomer,
                totalGranted = saleCreateViewModel.totalGranted,
                totalPrice = saleCreateViewModel.totalPrice,
                cash = saleCreateViewModel.cash,
                change = saleCreateViewModel.change,
                isPaid = saleCreateViewModel.isPaid,
                saleOnCredit = saleCreateViewModel.saleOnCredit,
                createdAt = DateTime.Now,
                deletedAt = null,
                idAttendance = saleCreateViewModel.idAttendace,
                idServiceType = saleCreateViewModel.idServiceType,
            };


            return saleModel;
        }
        public sale ConvertViewModelToModel_WithoutCreateDate(SaleCreateViewModel saleCreateViewModel)
        {
            sale saleModel = new sale()
            {
                accountPayment = saleCreateViewModel.accountPayment,
                idSale = saleCreateViewModel.idSale,
                idCustomer = saleCreateViewModel.idCustomer,
                totalGranted = saleCreateViewModel.totalGranted,
                totalPrice = saleCreateViewModel.totalPrice,
                cash = saleCreateViewModel.cash,
                change = saleCreateViewModel.change,
                isPaid = saleCreateViewModel.isPaid,
                saleOnCredit = saleCreateViewModel.saleOnCredit,
                createdAt = Convert.ToDateTime(saleCreateViewModel.createdAt),
                deletedAt = null,
                idAttendance = saleCreateViewModel.idAttendace,
                idServiceType = saleCreateViewModel.idServiceType,
            };


            return saleModel;
        }
        public List<SaleIndexViewModel> ConvertModelsToViewModels(List<sale> listSaleModel)
        {
            List<SaleIndexViewModel> listSalesViewModels = new List<SaleIndexViewModel>();

            foreach (var sale in listSaleModel)
            {
                SaleIndexViewModel saleViewModel = new SaleIndexViewModel()
                {
                    accountPayment = sale.accountPayment,
                    idSale = sale.idSale,
                    idCustomer = sale.idCustomer,
                    totalGranted = sale.totalGranted,
                    totalPrice = sale.totalPrice,
                    cash = sale.cash,
                    change = sale.change,
                    isPaid = sale.isPaid,
                    saleOnCredit = sale.saleOnCredit,
                    createdAt = sale.createdAt.ToString("t") + " " + sale.createdAt.ToString("d"),
                    deletedAt = sale.deletedAt.ToString(),
                    idServiceType = sale.idServiceType,
                    customer = new CustomersMapper(_workUnitRepository, _companyRepository).MapperModelToViewModel(_customersRepository.GetById(sale.idCustomer)),
                    attendance = _attendanceMapper.MapperModelToViewModel(_attendanceRepository.FindById(sale.idAttendance)),
                };
                if (sale.idServiceType != null)
                {
                    saleViewModel.serviceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.GetById(sale.idServiceType.Value));
                }
                listSalesViewModels.Add(saleViewModel);
            }
            return listSalesViewModels;
        }
        public SaleDetailViewModel ConvertModelToViewModel(sale saleModel)
        {
            return new SaleDetailViewModel()
            {
                totalPrice = saleModel.totalPrice.Value,
                totalGranted = saleModel.totalGranted.Value,
                accountPayment = saleModel.accountPayment.Value,
                cash = saleModel.cash.Value,
                change = saleModel.change.Value,
                createdAt = saleModel.createdAt.ToString("D"),
                deletedAt = saleModel.deletedAt,
                idAttendace = saleModel.idAttendance.Value,
                idCustomer = saleModel.idCustomer,
                idSale = saleModel.idSale,
                idServiceType = saleModel.idServiceType,
                isPaid = saleModel.isPaid,
                saleOnCredit = saleModel.saleOnCredit.Value,
                customer = new CustomersMapper(_workUnitRepository, _companyRepository).MapperModelToViewModel(_customersRepository.FindById(Convert.ToInt32(saleModel.idCustomer))),
                details = new SaleDetailMapper(_productRepository, _serviceTypeRepository).ConvertModelsToViewModels(_saleRepository.GetDetailsByIdSale(saleModel.idSale)),
            };
        }

        public List<SaleToReportViewModel> ConvertSaleDetailModelToSaleToReportViewModels(List<sale> listSaleModel)
        {
            List<SaleToReportViewModel> listSalesViewModels = new List<SaleToReportViewModel>();

            foreach (var sale in listSaleModel)
            {
                SaleToReportViewModel saleViewModel = new SaleToReportViewModel()
                {
                    accountPayment = sale.accountPayment,
                    idSale = sale.idSale,
                    idCustomer = sale.idCustomer,
                    totalGranted = sale.totalGranted,
                    totalPrice = sale.totalPrice,
                    cash = sale.cash,
                    change = sale.change,
                    isPaid = sale.isPaid,
                    saleOnCredit = sale.saleOnCredit,
                    createdAt = sale.createdAt.ToLongDateString(),
                    deletedAt = sale.deletedAt,
                    idServiceType = sale.idServiceType,
                    customer = new CustomersMapper(_workUnitRepository, _companyRepository).MapperModelToViewModel(_customersRepository.GetById(sale.idCustomer)),
                    details = new SaleDetailMapper(_productRepository, _serviceTypeRepository).ConvertModelsToViewModels(_saleRepository.GetDetailsByIdSale(Convert.ToInt32(sale.idSale))),
                };
                if (sale.idServiceType != null)
                {
                    saleViewModel.serviceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.GetById(sale.idServiceType.Value));
                }
                listSalesViewModels.Add(saleViewModel);
            }
            return listSalesViewModels;
        }

        public SaleToReportViewModel ConvertSaleDetailModelToSaleToReportViewModel(sale saleModel)
        {
            return new SaleToReportViewModel()
            {
                totalPrice = saleModel.totalPrice.Value,
                totalGranted = saleModel.totalGranted.Value,
                accountPayment = saleModel.accountPayment.Value,
                cash = saleModel.cash.Value,
                change = saleModel.change.Value,
                createdAt = saleModel.createdAt.ToString("D"),
                deletedAt = saleModel.deletedAt,
                idAttendace = saleModel.idAttendance.Value,
                idCustomer = saleModel.idCustomer,
                idSale = saleModel.idSale,
                idServiceType = saleModel.idServiceType,
                isPaid = saleModel.isPaid,
                saleOnCredit = saleModel.saleOnCredit.Value,
                customer = new CustomersMapper(_workUnitRepository, _companyRepository).MapperModelToViewModel(_customersRepository.FindById(Convert.ToInt32(saleModel.idCustomer))),
                details = new SaleDetailMapper(_productRepository, _serviceTypeRepository).ConvertModelsToViewModels(_saleRepository.GetDetailsByIdSale(Convert.ToInt32(saleModel.idSale))),
            };
        }
        public List<SaleIndexViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<sale, SaleIndexViewModel> result)
        {
            List<SaleIndexViewModel> listSalesViewModels = new List<SaleIndexViewModel>();

            foreach (var sale in result.ResultsModel)
            {
                SaleIndexViewModel saleViewModel = new SaleIndexViewModel()
                {
                    accountPayment = sale.accountPayment,
                    idSale = sale.idSale,
                    idCustomer = sale.idCustomer,
                    totalGranted = sale.totalGranted,
                    totalPrice = sale.totalPrice,
                    cash = sale.cash,
                    change = sale.change,
                    isPaid = sale.isPaid,
                    saleOnCredit = sale.saleOnCredit,
                    createdAt = sale.createdAt.ToString("t") + " " + sale.createdAt.ToString("d"),
                    deletedAt = sale.deletedAt.ToString(),
                    idServiceType = sale.idServiceType,
                    customer = new CustomersMapper(_workUnitRepository, _companyRepository).MapperModelToViewModel(_customersRepository.GetById(sale.idCustomer)),
                    attendance = _attendanceMapper.MapperModelToViewModel(_attendanceRepository.FindById(sale.idAttendance)),
                };
                if (sale.idServiceType != null)
                {
                    saleViewModel.serviceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.GetById(sale.idServiceType.Value));
                }
                listSalesViewModels.Add(saleViewModel);
            }
            return listSalesViewModels;
        }
        public List<SaleToReportViewModel> PaginatorConvertSaleDetailModelToReportViewModels(PaginatorGeneric<sale, SaleToReportViewModel> result)
        {
            List<SaleToReportViewModel> listSalesViewModels = new List<SaleToReportViewModel>();

            foreach (var sale in result.ResultsModel)
            {
                SaleToReportViewModel saleViewModel = new SaleToReportViewModel()
                {
                    accountPayment = sale.accountPayment,
                    idSale = sale.idSale,
                    idCustomer = sale.idCustomer,
                    totalGranted = sale.totalGranted,
                    totalPrice = sale.totalPrice,
                    cash = sale.cash,
                    change = sale.change,
                    isPaid = sale.isPaid,
                    saleOnCredit = sale.saleOnCredit,
                    createdAt = sale.createdAt.ToLongDateString(),
                    deletedAt = sale.deletedAt,
                    idServiceType = sale.idServiceType,
                    customer = _customersMapper.MapperModelToViewModel(_customersRepository.GetById(sale.idCustomer)),
                    details = _saleDetailMapper.ConvertModelsToViewModels(_saleRepository.GetDetailsByIdSale(Convert.ToInt32(sale.idSale))),
                };
                if (sale.idServiceType != null)
                {
                    saleViewModel.serviceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.GetById(sale.idServiceType.Value));
                }
                listSalesViewModels.Add(saleViewModel);
            }
            return listSalesViewModels;
        }
        public List<SaleToReportViewModel> PaginatorConvertSaleDetailModelToReportViewModels2(FastSaleFilterViewModel result)
        {
            List<SaleToReportViewModel> listSalesViewModels = new List<SaleToReportViewModel>();

            foreach (var sale in result.ResultsModelW)
            {
                SaleToReportViewModel saleViewModel = new SaleToReportViewModel()
                {
                    accountPayment = sale.accountPayment,
                    idSale = sale.idSale,
                    idCustomer = sale.idCustomer,
                    totalGranted = sale.totalGranted,
                    totalPrice = sale.totalPrice,
                    cash = sale.cash,
                    change = sale.change,
                    isPaid = sale.isPaid,
                    saleOnCredit = sale.saleOnCredit,
                    createdAt = sale.createdAt.ToLongDateString(),
                    deletedAt = sale.deletedAt,
                    idServiceType = sale.idServiceType,
                    customer = _customersMapper.MapperModelToViewModel(_customersRepository.GetById(sale.idCustomer)),
                    details = _saleDetailMapper.ConvertModelsToViewModels(_saleRepository.GetDetailsByIdSale(Convert.ToInt32(sale.idSale))),
                };
                if (sale.idServiceType != null)
                {
                    saleViewModel.serviceType = new ServiceTypeMapper().MapperModelToViewModel(_serviceTypeRepository.GetById(sale.idServiceType.Value));
                }
                listSalesViewModels.Add(saleViewModel);
            }
            return listSalesViewModels;
        }
    }
}