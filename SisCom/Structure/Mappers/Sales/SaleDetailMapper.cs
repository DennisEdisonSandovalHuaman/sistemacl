﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Models;
using SisCom.Structure.ViewModels.Sales;
using SisCom.Structure.Mappers.Products;
using SisCom.Data.IRepositories;

namespace SisCom.Structure.Mappers.Sales
{
    public class SaleDetailMapper
    {
        IProductRepository _productRepository;
        IServiceTypeRepository _serviceTypeRepository;

        public SaleDetailMapper(IProductRepository productRepository, IServiceTypeRepository serviceTypeRepository)
        {
            _productRepository = productRepository;
            _serviceTypeRepository = serviceTypeRepository;
        }

        public List<sale_detail> ConvertViewModelsToModels(List<DetailToSaleViewModel> detailToSaleViewModels)
        {
            var listSaleDetail = new List<sale_detail>();

            foreach (var detail in detailToSaleViewModels)
            {
                listSaleDetail.Add(ConvertViewModelToModel(detail));
            }
            return listSaleDetail;
        }
        public List<DetailToSaleViewModel> ConvertModelsToViewModels(List<sale_detail> detailToSaleModels)
        {
            var listSaleDetail = new List<DetailToSaleViewModel>();

            foreach (var detail in detailToSaleModels)
            {
                listSaleDetail.Add(ConvertModelToViewModel(detail));
            }
            return listSaleDetail;
        }

        private DetailToSaleViewModel ConvertModelToViewModel(sale_detail detail)
        {
            return new DetailToSaleViewModel()
            {
                idSale = detail.idSale,
                totalGranted = detail.totalGranted,
                idProduct = detail.idProduct,
                totalPrice = detail.totalPrice,
                quantity = detail.quantity,
                product = new ProductMapper(_serviceTypeRepository, null).MapperModelToViewModel(_productRepository.GetById(detail.idProduct)),
            };
        }

        public sale_detail ConvertViewModelToModel(DetailToSaleViewModel detailToSaleViewModel)
        {
            return new sale_detail()
            {
                idProduct = detailToSaleViewModel.idProduct,
                totalGranted = detailToSaleViewModel.totalGranted,
                quantity = detailToSaleViewModel.quantity,
                totalPrice = detailToSaleViewModel.totalPrice,
                idSale = detailToSaleViewModel.idSale,
            };
        }
    }
}