﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.ServiceType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.ServiceType
{
    public class ServiceTypeMapper
    {
        public service_type MapperViewModelToModel(ServiceTypeViewModel _serviceTypeViewModel)
        {
            return new service_type
            {
                idServiceType = _serviceTypeViewModel.idServiceType,
                name = _serviceTypeViewModel.name,
                startTime = Convert.ToDateTime(_serviceTypeViewModel.startTime),
                endTime = Convert.ToDateTime(_serviceTypeViewModel.endTime),
                price = _serviceTypeViewModel.price,
            };
        }
        public ServiceTypeViewModel MapperModelToViewModel(service_type _service_type)
        {
            ServiceTypeViewModel _serviceTypeViewModel = new ServiceTypeViewModel();
            if (_service_type != null)
            {
                _serviceTypeViewModel.idServiceType = _service_type.idServiceType;
                _serviceTypeViewModel.name = _service_type.name;
                _serviceTypeViewModel.startTime = _service_type.startTime.ToString();
                _serviceTypeViewModel.endTime = _service_type.endTime.ToString();
                _serviceTypeViewModel.price = _service_type.price;

            }
            return _serviceTypeViewModel;
        }
        public List<ServiceTypeViewModel> MapperListModelToViewModel(List<service_type> _service_type)
        {
            return _service_type.Select(x => new ServiceTypeViewModel
            {
                idServiceType = x.idServiceType,
                name = x.name,
                startTime = x.startTime.ToString(),
                endTime = x.endTime.ToString(),
                price = x.price,
            }).ToList();
        }
        public List<ServiceTypeViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<service_type, ServiceTypeViewModel> result)
        {
            return result.ResultsModel.Select(x => new ServiceTypeViewModel
            {
                idServiceType = x.idServiceType,
                name = x.name,
                startTime = x.startTime.ToShortTimeString().ToString(),
                endTime = x.endTime.ToShortTimeString().ToString(),
                price = x.price,
            }).ToList();
        }
    }
}