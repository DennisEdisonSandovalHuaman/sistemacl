﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Models;
using SisCom.Structure.ViewModels.Sales;


namespace SisCom.Structure.Mappers.SaleDetails
{
    public class SaleDetailMapper
    {

        public List<DetailToSaleViewModel> ConvertModelsToViewModels(List<sale_detail> sale_details)
        {
            var List = new List<DetailToSaleViewModel>();
            foreach (var detail in sale_details)
            {
                List.Add(ConvertModelToViewModel(detail));
            }
            return List;

        }
        public DetailToSaleViewModel ConvertModelToViewModel(sale_detail sale_detail)
        {
            return new DetailToSaleViewModel()
            {
                totalGranted = sale_detail.totalGranted,
                totalPrice = sale_detail.totalPrice,
                idProduct = sale_detail.idProduct,
                idSale = sale_detail.idSale,
                quantity = sale_detail.quantity,
            };

        }

    }
}