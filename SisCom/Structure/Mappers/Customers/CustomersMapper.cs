﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Company;
using SisCom.Structure.Mappers.WorkUnit;
using SisCom.Structure.ViewModels.Company;
using SisCom.Structure.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Structure.Mappers.Customers
{
    public class CustomersMapper
    {
        IWorkUnitRepository _workUnitRepository;
        ICompanyRepository _companyRepository;

        public CustomersMapper(IWorkUnitRepository workUnitRepository, ICompanyRepository companyRepository)
        {
            _workUnitRepository = workUnitRepository;
            _companyRepository = companyRepository;
        }
        public customer MapperViewModelToModel(CustomersViewModel _customersViewModel)
        {
            return new customer
            {
                idCustomer = _customersViewModel.idCustomer,
                name = _customersViewModel.name,
                fatherLastname = _customersViewModel.fatherLastname,
                motherLastname = _customersViewModel.motherLastname,
                dni = _customersViewModel.dni,
                isActive = _customersViewModel.isActive,
                activeDate = Convert.ToDateTime(_customersViewModel.activeDate),
                hasGrant = _customersViewModel.hasGrant,
                idWorkUnit = _customersViewModel.idWorkUnit,
                idCompany = _customersViewModel.idCompany,
                deletedAt = Convert.ToDateTime(_customersViewModel.deletedAt),
                photo = _customersViewModel.photo,
            };
        }
        public CustomersViewModel MapperModelToViewModel(customer _customer)
        {
            CustomersViewModel _customersViewModel = new CustomersViewModel();
            _customersViewModel.idCustomer = _customer.idCustomer;
            _customersViewModel.name = _customer.name;
            _customersViewModel.fatherLastname = _customer.fatherLastname;
            _customersViewModel.motherLastname = _customer.motherLastname;
            _customersViewModel.dni = _customer.dni;
            _customersViewModel.isActive = _customer.isActive;
            _customersViewModel.activeDate = _customer.activeDate.ToString();
            _customersViewModel.hasGrant = _customer.hasGrant;
            _customersViewModel.idWorkUnit = Convert.ToInt32(_customer.idWorkUnit);
            _customersViewModel.idCompany = Convert.ToInt32(_customer.idCompany);
            _customersViewModel.deletedAt = _customer.deletedAt.ToString();
            _customersViewModel.photo = _customer.photo;
            _customersViewModel.WorkUnit = new WorkUnitMapper().MapperModelToViewModel(_workUnitRepository.FindById(Convert.ToInt32(_customer.idWorkUnit)));
            _customersViewModel.Company = new CompanyMapper().MapperModelToViewModel(_companyRepository.FindById(Convert.ToInt32(_customer.idCompany)));

            return _customersViewModel;
        }
        public List<CustomersViewModel> MapperListModelToViewModel(List<customer> _customer)
        {
            return _customer.Select(x => new CustomersViewModel
            {
                idCustomer = x.idCustomer,
                name = x.name,
                fatherLastname = x.fatherLastname,
                motherLastname = x.motherLastname,
                dni = x.dni,
                isActive = x.isActive,
                activeDate = x.activeDate.ToString(),
                hasGrant = x.hasGrant,
                idWorkUnit = Convert.ToInt32(x.idWorkUnit),
                idCompany = Convert.ToInt32(x.idCompany),
                deletedAt = x.deletedAt.ToString(),
                photo = x.photo,
                WorkUnit = new WorkUnitMapper().MapperModelToViewModel(_workUnitRepository.FindById(Convert.ToInt32(x.idWorkUnit))),
                Company = new CompanyMapper().MapperModelToViewModel(_companyRepository.FindById(Convert.ToInt32(x.idCompany))),
            }).ToList();
        }
        public List<CustomersViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<customer, CustomersViewModel> result)
        {
            return result.ResultsModel.Select(x => new CustomersViewModel
            {
                idCustomer = x.idCustomer,
                name = x.name,
                fatherLastname = x.fatherLastname,
                motherLastname = x.motherLastname,
                dni = x.dni,
                isActive = x.isActive,
                activeDate = x.activeDate.ToString(),
                hasGrant = x.hasGrant,
                idWorkUnit = Convert.ToInt32(x.idWorkUnit),
                idCompany = Convert.ToInt32(x.idCompany),
                deletedAt = x.deletedAt.ToString(),
                photo = x.photo,
                WorkUnit = new WorkUnitMapper().MapperModelToViewModel(_workUnitRepository.FindById(Convert.ToInt32(x.idWorkUnit))),
                Company = new CompanyMapper().MapperModelToViewModel(_companyRepository.FindById(Convert.ToInt32(x.idCompany))),
            }).ToList();
        }
    }
}