﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.Mappers.BrachMapper
{
    public class BranchMapper
    {
        public branch MapperViewModelToModel(BranchViewModel _branchViewModel)
        {
            return new branch
            {
                idBranch = _branchViewModel.idBranch,
                name = _branchViewModel.name,
                createdAt = Convert.ToDateTime(_branchViewModel.createdAt),
                deletedAt = Convert.ToDateTime(_branchViewModel.deletedAt),
                isActive = _branchViewModel.isActive,
            };
        }
        public BranchViewModel MapperModelToViewModel(branch _branch)
        {
            BranchViewModel _branchViewModel = new BranchViewModel();
            _branchViewModel.idBranch = _branch.idBranch;
            _branchViewModel.name = _branch.name;
            _branchViewModel.createdAt = _branch.createdAt.ToString();
            _branchViewModel.deletedAt = _branch.deletedAt.ToString();
            _branchViewModel.isActive = _branch.isActive;

            return _branchViewModel;
        }
        public List<BranchViewModel> MapperListModelToViewModel(List<branch> _branch)
        {
            return _branch.Select(x => new BranchViewModel
            {
                idBranch = x.idBranch,
                name = x.name,
                createdAt = x.createdAt.ToString(),
                deletedAt = x.deletedAt.ToString(),
                isActive = x.isActive,
            }).ToList();
        }
        public List<BranchViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<branch, BranchViewModel> result)
        {
            return result.ResultsModel.Select(x => new BranchViewModel
            {
                idBranch = x.idBranch,
                name = x.name,
                createdAt = x.createdAt.ToString(),
                deletedAt = x.deletedAt.ToString(),
                isActive = x.isActive,
            }).ToList();
        }
    }
}