﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Pos;
using SisCom.Structure.ViewModels.Attendance;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Structure.Mappers.Attendance
{
    public class AttendanceMapper
    {
        private readonly PosMapper _posMapper;
        private readonly IPosRepository _posRepository;


        public AttendanceMapper(PosMapper posMapper, IPosRepository posRepository)
        {
            _posMapper = posMapper;
            _posRepository = posRepository;
        }

        public attendance MapperViewModelToModel(AttendanceViewModel _attendanceViewModel)
        {
            if (_attendanceViewModel.endAt == null)
            {
                return new attendance
                {
                    idAttendance = _attendanceViewModel.idAttendance,
                    idPos = _attendanceViewModel.idPos,
                    startAt = DateTime.Parse(_attendanceViewModel.startAt),
                    startCash = _attendanceViewModel.startCash,
                    currentCash = _attendanceViewModel.currentCash,
                };
            }
            else
            {
                return new attendance
                {
                    idAttendance = _attendanceViewModel.idAttendance,
                    idPos = _attendanceViewModel.idPos,
                    startAt = DateTime.Parse(_attendanceViewModel.startAt),
                    endAt = DateTime.Parse(_attendanceViewModel.endAt),
                    startCash = _attendanceViewModel.startCash,
                    currentCash = _attendanceViewModel.currentCash,
                };
            }

        }
        public AttendanceViewModel MapperModelToViewModel(attendance _attendance)
        {
            AttendanceViewModel _attendanceViewModel = new AttendanceViewModel();
            _attendanceViewModel.idAttendance = _attendance.idAttendance;
            _attendanceViewModel.idPos = _attendance.idPos;
            _attendanceViewModel.startAt = _attendance.startAt.ToString();
            _attendanceViewModel.endAt = _attendance.endAt.ToString();
            _attendanceViewModel.startCash = _attendance.startCash;
            _attendanceViewModel.currentCash = _attendance.currentCash;
            _attendanceViewModel.pos = _posMapper.MapperModelToViewModel(_attendance.pos);
            return _attendanceViewModel;
        }
        public List<AttendanceViewModel> MapperListModelToViewModel(List<attendance> _attendance)
        {
            return _attendance.Select(x => new AttendanceViewModel
            {
                idAttendance = x.idAttendance,
                idPos = x.idPos,
                startAt = x.startAt.ToString(),
                endAt = x.endAt.ToString(),
                startCash = x.startCash,
                currentCash = x.currentCash,
                pos = _posMapper.MapperModelToViewModel(x.pos)
            }).ToList();
        }
        public List<AttendanceViewModel> MapperPaginatorModelToViewModel(PaginatorGeneric<attendance, AttendanceViewModel> result)
        {
            return result.ResultsModel.Select(x => new AttendanceViewModel
            {
                idAttendance = x.idAttendance,
                idPos = x.idPos,
                startAt = x.startAt.ToString(),
                endAt = x.endAt.ToString(),
                startCash = x.startCash,
                currentCash = x.currentCash,
                pos = _posMapper.MapperModelToViewModel(_posRepository.FindById(x.idPos))
            }).ToList();
        }
    }
}