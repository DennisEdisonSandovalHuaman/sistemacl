﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Company
{
    public class CompanyViewModel
    {
        public CompanyViewModel()
        {
            stateToFilter = true;
        }
        public int idCompany { get; set; }
        public string name { get; set; }
        public bool isActive { get; set; }
        public bool stateToFilter { get; set; }
        public string deletedAt { get; set; }
    }
}