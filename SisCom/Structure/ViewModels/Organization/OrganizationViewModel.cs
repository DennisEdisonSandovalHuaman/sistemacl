﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Organization
{
    public class OrganizationViewModel
    {
        public OrganizationViewModel()
        {

        }
        public int idOrganization { get; set; }
        public string name { get; set; }
        public string logo { get; set; }
        public string address { get; set; }
    }
}