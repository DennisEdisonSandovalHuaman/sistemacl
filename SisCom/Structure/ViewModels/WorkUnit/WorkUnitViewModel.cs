﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.WorkUnit
{
    public class WorkUnitViewModel
    {
        public WorkUnitViewModel()
        {

        }
        public int idWorkUnit { get; set; } 
        public string name { get; set; }
        public bool isActive { get; set; }
        public string deletedAt { get; set; }
    }
}