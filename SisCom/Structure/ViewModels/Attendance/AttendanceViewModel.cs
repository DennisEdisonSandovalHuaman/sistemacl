﻿using SisCom.Structure.ViewModels.Pos;

namespace SisCom.Structure.ViewModels.Attendance
{
    public class AttendanceViewModel
    {
        public AttendanceViewModel()
        {
            pos = new PosViewModel();
        }
        public int idAttendance { get; set; }
        public int idPos { get; set; }
        public string startAt { get; set; }
        public string endAt { get; set; }
        public double startCash { get; set; }
        public double currentCash { get; set; }

        public PosViewModel pos { get; set; }
    }
}