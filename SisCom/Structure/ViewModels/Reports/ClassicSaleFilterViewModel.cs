﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Reports
{
    public class ClassicSaleFilterViewModel
    {
        public DateTime initDate { get; set; }
        public DateTime endDate { get; set; }
        public int idPos { get; set; }
    }
}