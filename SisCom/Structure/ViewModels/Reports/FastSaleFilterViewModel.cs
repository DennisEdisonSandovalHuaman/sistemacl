﻿using SisCom.Helpers;
using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Reports
{
    public class FastSaleFilterViewModel : PaginatorGeneric<sale, SaleToReportViewModel>
    {
        public FastSaleFilterViewModel()
        {
            ResultsModelW = new List<sale>();
            ResultsW = new List<SaleToReportViewModel>();
        }
        public DateTime initDate { get; set; }
        public DateTime endDate { get; set; }
        public int idPos { get; set; }
        public int idServiceType { get; set; }
        public int idWorkUnit { get; set; }
        public int PageW { get; set; }
        public int PageSizeW { get; set; }
        public int TotalRowW { get; set; }
        public int PageCountW { get; set; }
        public List<sale> ResultsModelW { get; set; }
        public List<int> idCompanies { get; set; }
        public List<SaleToReportViewModel> ResultsW { get; set; }

        public double totalPriceWorker { get; set; }
        public double totalAccountPaymentWorker { get; set; }
    }
}