﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.ResultViewModel
{
    public class ResultViewModel
    {
        public ResultViewModel()
        {
            State = true;
            MessageType = "success";
            MessageTitle = "Operación Exitosa !!!";
            Message = "La operación se ha realizado exitosamente ... !!!";
        }
        public bool State { get; set; }
        public string Message { get; set; }
        public string MessageType { get; set; }
        public string MessageTitle { get; set; }

        public void GetError(string MessageTitle = "Operación Fallida !!!", string Message = "La operación falló en algún punto ... !!!")
        {
            this.State = false;
            this.MessageType = "error";
            this.MessageTitle = MessageTitle;
            this.Message = Message;
        }
        public void GetWarning(string MessageTitle = "Precaución en la Operación !!!", string Message = "Tenga cuidado en la operación, se ha detectado un inconveniente ... !!!")
        {
            this.State = false;
            this.MessageType = "warning";
            this.MessageTitle = MessageTitle;
            this.Message = Message;
        }
        public void GetInfo(string MessageTitle = "Asistente de Información !!!", string Message = "Por favor revise los datos ingresados ... !!!")
        {
            this.State = false;
            this.MessageType = "info";
            this.MessageTitle = MessageTitle;
            this.Message = Message;
        }
    }
}