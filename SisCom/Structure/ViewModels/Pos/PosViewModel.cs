﻿using SisCom.Structure.ViewModels.Users;

namespace SisCom.Structure.ViewModels.Pos
{
    public class PosViewModel
    {
        public PosViewModel()
        {
            User = new UsersViewModel();
        }
        public int idPos { get; set; }
        public int idUser { get; set; }
        public string name { get; set; }
        public bool isActive { get; set; }
        public string deletedAt { get; set; }
        public bool isOpen { get; set; }

        public UsersViewModel User { get; set; }
    }
    public class AllOpenPos
    {
        public int idAttendance;
        public int idpos;
        public string dni;
        public string user;
        public string pos;
        public string openHour;
        public bool state;
    }
}