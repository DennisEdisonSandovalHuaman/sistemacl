﻿using SisCom.Structure.ViewModels.Role;
using System;

namespace SisCom.Structure.ViewModels.Users
{
    public class UsersViewModel
    {
        public UsersViewModel()
        {
            Branch = new BranchViewModel();
            Role = new RoleViewModel();
        }
        public int idUser { get; set; }
        public string dni { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string fatherLastName { get; set; }
        public string motherLastName { get; set; }
        public bool isActive { get; set; }
        public string deletedAt { get; set; }
        public int? idBranch { get; set; }
        public int idRole { get; set; }

        public BranchViewModel Branch { get; set; }
        public RoleViewModel Role { get; set; }
    }
}