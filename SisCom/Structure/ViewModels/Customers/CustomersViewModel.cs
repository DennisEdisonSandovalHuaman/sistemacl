﻿using SisCom.Structure.ViewModels;
using SisCom.Structure.ViewModels.Company;
using SisCom.Structure.ViewModels.WorkUnit;
using System;

namespace SisCom.Structure.ViewModels.Customers
{
    public class CustomersViewModel
    {
        public CustomersViewModel()
        {
            WorkUnit = new WorkUnitViewModel();
            Company = new CompanyViewModel();
        }

        public int idCustomer { get; set; }
        public string name { get; set; }
        public string fatherLastname { get; set; }
        public string motherLastname { get; set; }
        public string dni { get; set; }
        public bool isActive { get; set; }
        public string activeDate { get; set; }
        public bool hasGrant { get; set; }
        public int idWorkUnit { get; set; }
        public int idCompany { get; set; }
        public string deletedAt { get; set; }
        public string photo { get; set; }

        public WorkUnitViewModel WorkUnit { get; set; }
        public CompanyViewModel Company { get; set; }
    }
}