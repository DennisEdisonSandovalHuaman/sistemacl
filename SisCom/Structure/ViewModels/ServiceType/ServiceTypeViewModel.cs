﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.ServiceType
{
    public class ServiceTypeViewModel
    {
        public ServiceTypeViewModel()
        {

        }
        public int idServiceType { get; set; }
        public string name { get; set; }
        public string startTime { get; set; }
        public string endTime { get; set; }
        public double price { get; set; }
    }
}