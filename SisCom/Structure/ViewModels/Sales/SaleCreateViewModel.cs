﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Sales
{
    public class SaleCreateViewModel
    {
        DateTime dateInstance = DateTime.Today;
        public SaleCreateViewModel()
        {
            idSale = 0;
            idCustomer = 0;
            idAttendace = 0;
            idServiceType = 0;
            totalGranted = 0;
            totalPrice = 0;
            cash = 0;
            accountPayment = 0;
            change = 0;
            isPaid = "0";
            saleOnCredit = 0;
            createdAt = dateInstance.ToString("D");
            deletedAt = dateInstance.ToString("D");
        }
        public int idSale { get; set; }
        public int idCustomer { get; set; }
        public int idAttendace { get; set; }
        public int idServiceType { get; set; }
        public float totalGranted { get; set; }
        public float totalPrice { get; set; }
        public float cash { get; set; }
        public float accountPayment { get; set; }
        public float change { get; set; }
        public string isPaid { get; set; }
        public float saleOnCredit { get; set; }
        public string createdAt { get; set; }
        public string deletedAt { get; set; }
        public void verifiyIsPaidOrNotPaid()
        {
            if (this.idServiceType == null)
                this.isPaid = "1";
            if (this.accountPayment == this.totalPrice - this.totalGranted)
                this.isPaid = "1";
            else
                this.isPaid = "0";
        }
    }
}