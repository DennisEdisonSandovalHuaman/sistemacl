﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Structure.ViewModels.Products;
using SisCom.Structure.Mappers.Products;

namespace SisCom.Structure.ViewModels.Sales
{
    public class DetailToSaleViewModel
    {
        public DetailToSaleViewModel()
        {
            idProduct = 0;
            quantity = 0;
            grantedPrice = 0;
            unitPrice = 0;
            totalPrice = 0;
            totalGranted = 0;
            idSale = 0;
            product = new ProductViewModel();
        }
        public int idProduct { get; set; }
        public double totalPrice { get; set; }
        public double unitPrice { get; set; }
        public int quantity { get; set; }
        public double grantedPrice { get; set; }
        public double totalGranted { get; set; }
        public int idSale { get; set; }
        public ProductViewModel product { get; set; }
        public void calculateTotalPriceAndTotalGranted()
        {
            this.totalPrice = this.unitPrice * this.quantity;
            this.totalGranted = this.grantedPrice * this.quantity;
        }
    }
}