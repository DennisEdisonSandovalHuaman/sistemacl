﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Structure.ViewModels.Attendance;
using SisCom.Structure.ViewModels.Customers;
using SisCom.Structure.ViewModels.ServiceType;

namespace SisCom.Structure.ViewModels.Sales
{
    public class SaleIndexViewModel
    {
        public SaleIndexViewModel()
        {
            customer = new CustomersViewModel();
            serviceType = new ServiceTypeViewModel();
        }
        public int idSale { get; set; }
        public int idCustomer { get; set; }
        public int? idServiceType { get; set; }
        public double? totalGranted { get; set; }
        public double? totalPrice { get; set; }
        public double? cash { get; set; }
        public double? accountPayment { get; set; }
        public double? change { get; set; }
        public string isPaid { get; set; }
        public double? saleOnCredit { get; set; }
        public string createdAt { get; set; }
        public string deletedAt { get; set; }
        public CustomersViewModel customer { get; set; }
        public ServiceTypeViewModel serviceType { get; set; }
        public AttendanceViewModel attendance { get; set; }
    }
}