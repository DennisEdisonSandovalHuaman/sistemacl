﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SisCom.Structure.ViewModels.Customers;

namespace SisCom.Structure.ViewModels.Sales
{
    public class SaleDetailViewModel
    {
        public SaleDetailViewModel()
        {
            customer = new CustomersViewModel();
            details = new List<DetailToSaleViewModel>();
        }

        public int idSale { get; set; }
        public int idCustomer { get; set; }
        public int? idAttendace { get; set; }
        public int? idServiceType { get; set; }
        public double? totalGranted { get; set; }
        public double? totalPrice { get; set; }
        public double? cash { get; set; }
        public double? accountPayment { get; set; }
        public double? change { get; set; }
        public string isPaid { get; set; }
        public double? saleOnCredit { get; set; }
        public string createdAt { get; set; }
        public DateTime? deletedAt { get; set; }
        public CustomersViewModel customer { get; set; }
        public List<DetailToSaleViewModel> details { get; set; }
    }
}