﻿using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Supplies
{
    public class SuppliesViewModel
    {

        public SuppliesViewModel()
        {
            this.branch = new BranchViewModel();
            this.isActive = true;
        }

        public int idSupplies { get; set; }
        public string name { get; set; }
        public int stock { get; set; }
        public double minStock { get; set; }
        public double weight { get; set; }
        public double minWeight { get; set; }
        public double price { get; set; }
        public bool isActive { get; set; }
        public int idBranch { get; set; }
        public BranchViewModel branch { get; set; }

    }
}