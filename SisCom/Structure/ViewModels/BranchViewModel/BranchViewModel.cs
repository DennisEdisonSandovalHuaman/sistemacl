﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels
{
    public class BranchViewModel
    {
        public BranchViewModel()
        {

        }
        public int idBranch { get; set; }
        public string name { get; set; }
        public string createdAt { get; set; }
        public string deletedAt { get; set; }
        public bool isActive { get; set; }
    }
}