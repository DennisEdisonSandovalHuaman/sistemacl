﻿using SisCom.Structure.ViewModels.ServiceType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Products
{
    public class ProductViewModel
    {
        public ProductViewModel()
        {
            ServiceType = new ServiceTypeViewModel();
            Branch = new BranchViewModel();
            currentStock = 0;
            minimunStock = 0;
        }
        public int idProduct { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public double grantedPrice { get; set; }
        public int? idServiceType { get; set; }
        public int? idBranch { get; set; }
        public int? currentStock { get; set; }
        public int? minimunStock { get; set; }
        public string withStock { get; set; }
        public bool isActive { get; set; }

        public ServiceTypeViewModel ServiceType { get; set; }
        public BranchViewModel Branch { get; set; }
    }
}