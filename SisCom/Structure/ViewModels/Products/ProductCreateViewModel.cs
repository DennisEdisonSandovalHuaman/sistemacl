﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Products
{
    public class ProductCreateViewModel
    {
        public ProductCreateViewModel()
        {
            idProduct = 0;
            name = null;
            price = 0;
            grantedPrice = 0;
            idServiceType = 0;
            currentStock = 0;
            minimunStock = 0;
        }
        public int idProduct { get; set; }
        public string name { get; set; }
        public double price { get; set; }
        public double grantedPrice { get; set; }
        public int idServiceType { get; set; }
        public int currentStock { get; set; }
        public int? minimunStock { get; set; }
    }
}