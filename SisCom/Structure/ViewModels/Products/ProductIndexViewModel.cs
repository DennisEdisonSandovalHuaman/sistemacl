﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Products
{
    public class ProductIndexViewModel
    {
        public int idProduct { get; set; }
        public string name { get; set; }
        public double? price { get; set; }
        public double? grantedPrice { get; set; }
        public int? idServiceType { get; set; }
        public double? currentStock { get; set; }
        public double? minimunStock { get; set; }
    }
}