﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Structure.ViewModels.Role
{
    public class RoleViewModel
    {
        public int idRole { get; set; }
        public string name { get; set; }
    }
}