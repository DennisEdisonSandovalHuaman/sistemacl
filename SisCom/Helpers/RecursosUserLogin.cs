﻿using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Helpers
{
    public class RecursosUserLogin
    {
        private ComEdbEntities _db = new ComEdbEntities();

        public int getUserById(string dni)
        {
            try
            {
                if (string.IsNullOrEmpty(dni) || string.IsNullOrWhiteSpace(dni)) return 0;
                user user = _db.user.Where(x => x.dni == dni).FirstOrDefault();
                return user.idUser;
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public string getUserByName(string dni)
        {
            try
            {
                if (string.IsNullOrEmpty(dni) || string.IsNullOrWhiteSpace(dni)) return "";
                user user = _db.user.Where(x => x.dni == dni).FirstOrDefault();
                return user.name + " " + user.fatherLastName + " " + user.motherLastName; ;
            }
            catch (Exception)
            {
                return "";
            }
        }
        public string getUserByRol(string dni)
        {
            try
            {
                if (string.IsNullOrEmpty(dni) || string.IsNullOrWhiteSpace(dni)) return "";
                user user = _db.user.Where(x => x.dni == dni).FirstOrDefault();
                return user.role.name;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}