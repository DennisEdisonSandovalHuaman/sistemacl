﻿using System.Collections.Generic;

namespace SisCom.Helpers
{
    public class PaginatorGeneric<TResultsModel, TResults>
    {
        public PaginatorGeneric()
        {
            ResultsModel = new List<TResultsModel>();
            Results = new List<TResults>();
        }
        public string Username { get; set; }
        public bool IsPending { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int TotalRow { get; set; }
        public int PageCount { get; set; }
        public List<TResultsModel> ResultsModel { get; set; }
        public List<TResults> Results { get; set; }

        public double totalPriceEmploy { get; set; }
        public double totalAccountPaymentEmploy { get; set; }
    }
}
