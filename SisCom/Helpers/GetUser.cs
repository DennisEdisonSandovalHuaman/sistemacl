﻿using SisCom.Data.IRepositories;
using SisCom.Data.Repositories;
using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Helpers
{
    public static class GetUser
    {
        private static ComEdbEntities db = new ComEdbEntities();

        public static int getIDuser(string usuario)
        {
            try
            {
                if (string.IsNullOrEmpty(usuario) || string.IsNullOrWhiteSpace(usuario)) return 0;
                user user = db.user.Where(x => x.dni == usuario).FirstOrDefault();
                return user.idUser;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static int? getIDCargouser(string usuario)
        {
            try
            {
                if (string.IsNullOrEmpty(usuario) || string.IsNullOrWhiteSpace(usuario)) return 0;
                user user = db.user.Where(x => x.dni == usuario).FirstOrDefault();
                return user.idBranch;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public static string getRolesUser(string usuario)
        {
            string role = "";
            try
            {
                if (string.IsNullOrEmpty(usuario) || string.IsNullOrWhiteSpace(usuario)) return role;
                user user = db.user.Where(x => x.dni == usuario).FirstOrDefault();
                role = user.role.name;
                return role;
            }
            catch (Exception)
            {
                return role;
            }
        }
        public static int getOpenAttendance(string dni)
        {
            int idAttendance = 0;
            try
            {
                var attendanceModel = db.attendance.Where(attend => attend.pos.user.dni == dni && attend.endAt == null).FirstOrDefault();
                idAttendance = attendanceModel.idAttendance;
                return idAttendance;
            }
            catch (Exception)
            {
                return idAttendance;
            }
        }
    }
}