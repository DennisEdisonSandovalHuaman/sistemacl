﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Configuration;

namespace SisCom.Helpers
{
    public class MailWithAttachmentHelper
    {
        string EmailOrigin = ConfigurationManager.AppSettings["Correo"];
        string Password = ConfigurationManager.AppSettings["Password"];
        string Domain = ConfigurationManager.AppSettings["Domain"];

        public void SendEmailWithAttachmets(string emailToDestiny, List<string> pathsToAttachments, string subject, string htmlBody)
        {
            MailMessage emailToSend = new MailMessage(EmailOrigin, emailToDestiny, subject, htmlBody);
            foreach (var pathAttach in pathsToAttachments)
            {
                emailToSend.Attachments.Add(new Attachment(pathAttach));
            }
            emailToSend.IsBodyHtml = true;
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Host = ConfigurationManager.AppSettings["Host"];
            smtpClient.EnableSsl = ConfigurationManager.AppSettings["enableSSL"].Equals("True", StringComparison.InvariantCultureIgnoreCase) ? true : false;
            smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
            smtpClient.Credentials = new System.Net.NetworkCredential(EmailOrigin, Password, Domain);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.Send(emailToSend);
            emailToSend.Dispose();
        }
    }
}
