﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using SisCom.Structure.ViewModels.Sales;
using SisCom.Structure.ViewModels.Products;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using SisCom.Structure.ViewModels.Reports;
using System;

namespace SisCom.Helpers
{
    public class ReportSaleExcel
    {
        int rowIndex = 2;
        ExcelRange cell;
        ExcelFill fill;
        Border border;
        Color tableTitleColor = Color.Cornsilk;
        Color tableHeaderColor = Color.LightSkyBlue;
        Color cellsColorYellow = Color.Yellow;
        Color tableBodyColor = Color.AliceBlue;
        ExcelHorizontalAlignment alignHorizontalCenter = ExcelHorizontalAlignment.Center;
        ExcelHorizontalAlignment alignHorizontalRight = ExcelHorizontalAlignment.Right;
        ExcelVerticalAlignment alignVerticalCenter = ExcelVerticalAlignment.Center;

        public byte[] CreateExcelAndReturnBytesArray(List<SaleToReportViewModel> listViewModels, DateTime initDate, DateTime endDate)
        {
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "Concesionaria R&O SRL";
                excelPackage.Workbook.Properties.Title = "Concesionaria R&O SRL";
                var sheet = excelPackage.Workbook.Worksheets.Add("Hoja de reporte");
                sheet.Name = "Reporte en Excel";
                sheet.Column(1).Width = 10;
                sheet.Column(2).Width = 30;
                sheet.Column(3).Width = 20;
                sheet.Column(4).Width = 40;
                sheet.Column(5).Width = 25;
                sheet.Column(6).Width = 30;
                sheet.Column(7).Width = 30;
                sheet.Column(8).Width = 20;
                sheet.Column(9).Width = 15;
                sheet.Column(10).Width = 20;
                sheet.Column(11).Width = 20;
                sheet.Column(12).Width = 15;

                #region EXCEL HEADER
                sheet.Cells[rowIndex, 2, rowIndex, 8].Merge = true;
                cell = sheet.Cells[rowIndex, 2];
                cell.Value = "Reporte de Ventas de Servicios";
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15;
                cell.Style.HorizontalAlignment = alignHorizontalCenter;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(tableTitleColor);
                rowIndex = rowIndex + 1;

                sheet.Cells[rowIndex, 2, rowIndex, 8].Merge = true;
                cell = sheet.Cells[rowIndex, 2];
                cell.Value = $"El reporte es del día {initDate.ToString("D")} a {endDate.ToString("D")}";
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 12;
                cell.Style.HorizontalAlignment = alignHorizontalCenter;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(tableTitleColor);
                rowIndex = rowIndex + 2;
                #endregion


                cell = sheet.Cells[rowIndex, 2];
                cell.Value = "LISTA DE OPERARIOS";
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 11;
                cell.Style.HorizontalAlignment = alignHorizontalCenter;
                cell.Style.VerticalAlignment = alignVerticalCenter;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(tableHeaderColor);
                border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex = rowIndex + 1;

                #region Table Header
                var index = 0;
                List<string> headers = new List<string>();
                headers.Add("Fecha");
                headers.Add("Servicio");
                headers.Add("Cliente");
                headers.Add("Empresa Cliente");
                headers.Add("Unidad de Trabajo");
                headers.Add("Monto subvencionado");
                headers.Add("Precio Total");


                foreach (var header in headers)
                {
                    cell = sheet.Cells[rowIndex, 2 + index];
                    cell.Value = header;
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 11;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableHeaderColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    index++;
                }
                rowIndex = rowIndex + 1;

                #endregion

                #region Table Body

                if (listViewModels.Count > 0)
                {
                    List<SaleToReportViewModel> listToWorkers = new List<SaleToReportViewModel>();
                    List<SaleToReportViewModel> listToEmployes = new List<SaleToReportViewModel>();

                    foreach (var viewModel in listViewModels) if (viewModel.customer.WorkUnit.name.Equals("operario", StringComparison.InvariantCultureIgnoreCase)) listToWorkers.Add(viewModel);
                    foreach (var viewModel in listViewModels) if (viewModel.customer.WorkUnit.name.Equals("empleado", StringComparison.InvariantCultureIgnoreCase) || viewModel.customer.WorkUnit.name.Equals("practicante", StringComparison.InvariantCultureIgnoreCase)) listToEmployes.Add(viewModel);

                    double totalPriceWorkers = 0;
                    double totalPriceEmployes = 0;

                    double accountPaymentWorkers = 0;
                    double accountPaymentEmployes = 0;

                    foreach (var viewModel in listToWorkers)
                    {
                        totalPriceWorkers += viewModel.totalPrice.Value;
                        accountPaymentWorkers += viewModel.totalGranted.Value;

                        cell = sheet.Cells[rowIndex, 2];
                        cell.Value = viewModel.createdAt;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3];
                        cell.Value = viewModel.serviceType.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 4];
                        cell.Value = viewModel.customer.name + " " + viewModel.customer.fatherLastname + " " + viewModel.customer.motherLastname;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 5];
                        cell.Value = viewModel.customer.Company.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 6];
                        cell.Value = viewModel.customer.WorkUnit.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 7];
                        cell.Value = "s/. " + viewModel.totalGranted;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 8];
                        cell.Value = "s/. " + viewModel.totalPrice;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        rowIndex = rowIndex + 1;
                    }
                    #endregion Table Body

                    rowIndex = rowIndex + 2;

                    cell = sheet.Cells[rowIndex, 2];
                    cell.Value = "LISTA DE EMPLEADOS";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 11;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableHeaderColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex = rowIndex + 1;

                    var index2 = 0;
                    foreach (var header in headers)
                    {
                        cell = sheet.Cells[rowIndex, 2 + index2];
                        cell.Value = header;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 11;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableHeaderColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                        index2++;
                    }
                    rowIndex = rowIndex + 1;

                    foreach (var viewModel in listToEmployes)
                    {

                        totalPriceEmployes += viewModel.totalPrice.Value;
                        accountPaymentEmployes += viewModel.totalGranted.Value;

                        cell = sheet.Cells[rowIndex, 2];
                        cell.Value = viewModel.createdAt;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3];
                        cell.Value = viewModel.serviceType.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 4];
                        cell.Value = viewModel.customer.name + " " + viewModel.customer.fatherLastname + " " + viewModel.customer.motherLastname;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 5];
                        cell.Value = viewModel.customer.Company.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 6];
                        cell.Value = viewModel.customer.WorkUnit.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 7];
                        cell.Value = "s/. " + viewModel.totalGranted;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 8];
                        cell.Value = "s/. " + viewModel.totalPrice;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        rowIndex = rowIndex + 1;
                    }

                    rowIndex = rowIndex + 2;

                    #region Final Description Table

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = "Numero de servicios";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "Precio promedio por servicio";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "Monto subvencionado total";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    cell = sheet.Cells[rowIndex, 4];
                    cell.Value = "Datos de obreros: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = listToWorkers.Count + " unidades";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "s/. " + Math.Round(accountPaymentWorkers / listToWorkers.Count, 2);
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "s/. " + accountPaymentWorkers;
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    cell = sheet.Cells[rowIndex, 4];
                    cell.Value = "Datos de empleados: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = listToEmployes.Count + " unidades";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "s/. " + Math.Round(accountPaymentEmployes / listToEmployes.Count, 2);
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "s/. " + accountPaymentEmployes;
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    cell = sheet.Cells[rowIndex, 4];
                    cell.Value = "Numero de servicios total: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = listToEmployes.Count + listToWorkers.Count + " unidades";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "Pago total: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "s/. " + Math.Round(totalPriceEmployes + totalPriceWorkers, 2);
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    #endregion Final Description Table

                }

                sheet.Protection.AllowInsertRows = false;
                sheet.Protection.AllowSort = true;
                sheet.Protection.AllowSelectUnlockedCells = true;
                sheet.Protection.AllowAutoFilter = true;
                sheet.Protection.IsProtected = true;

                return excelPackage.GetAsByteArray();
            }
        }


        public void CreateClassicExcelInPath(string filepath, string filename, List<SaleToReportViewModel> listViewModels, DateTime initDate, DateTime endDate)
        {
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "Concesionaria R&O SRL";
                excelPackage.Workbook.Properties.Title = "Concesionaria R&O SRL";
                var sheet = excelPackage.Workbook.Worksheets.Add("Hoja de reporte");
                sheet.Name = "Reporte en Excel";
                sheet.Column(1).Width = 10;
                sheet.Column(2).Width = 30;
                sheet.Column(3).Width = 30;
                sheet.Column(4).Width = 40;
                sheet.Column(5).Width = 25;
                sheet.Column(6).Width = 30;
                sheet.Column(7).Width = 30;
                sheet.Column(8).Width = 10;
                sheet.Column(9).Width = 15;
                sheet.Column(10).Width = 20;
                sheet.Column(11).Width = 20;
                sheet.Column(12).Width = 15;

                #region EXCEL HEADER
                sheet.Cells[rowIndex, 2, rowIndex, 7].Merge = true;
                cell = sheet.Cells[rowIndex, 2];
                cell.Value = "Reporte de Ventas de Servicios";
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 15;
                cell.Style.HorizontalAlignment = alignHorizontalCenter;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(tableTitleColor);
                rowIndex = rowIndex + 1;

                sheet.Cells[rowIndex, 2, rowIndex, 8].Merge = true;
                cell = sheet.Cells[rowIndex, 2];
                cell.Value = $"El reporte es del día {initDate.ToString("D")} a {endDate.ToString("D")}";
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 12;
                cell.Style.HorizontalAlignment = alignHorizontalCenter;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(tableTitleColor);
                rowIndex = rowIndex + 2;
                #endregion


                cell = sheet.Cells[rowIndex, 2];
                cell.Value = "LISTA DE OBREROS";
                cell.Style.Font.Bold = true;
                cell.Style.Font.Size = 11;
                cell.Style.HorizontalAlignment = alignHorizontalCenter;
                cell.Style.VerticalAlignment = alignVerticalCenter;
                fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(tableHeaderColor);
                border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                rowIndex = rowIndex + 1;

                #region Table Header
                var index = 0;
                List<string> headers = new List<string>();
                headers.Add("Fecha");
                headers.Add("Servicio");
                headers.Add("Cliente");
                headers.Add("Unidad de Trabajo");
                headers.Add("Monto subvencionado");
                headers.Add("Precio Total");


                foreach (var header in headers)
                {
                    cell = sheet.Cells[rowIndex, 2 + index];
                    cell.Value = header;
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 11;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableHeaderColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    index++;
                }
                rowIndex = rowIndex + 1;

                #endregion

                #region Table Body

                if (listViewModels.Count > 0)
                {
                    List<SaleToReportViewModel> listToWorkers = new List<SaleToReportViewModel>();
                    List<SaleToReportViewModel> listToEmployes = new List<SaleToReportViewModel>();

                    foreach (var viewModel in listViewModels) if (viewModel.customer.WorkUnit.name == "obreros") listToWorkers.Add(viewModel);
                    foreach (var viewModel in listViewModels) if (viewModel.customer.WorkUnit.name == "empleados") listToEmployes.Add(viewModel);

                    double totalPriceWorkers = 0;
                    double totalPriceEmployes = 0;

                    double accountPaymentWorkers = 0;
                    double accountPaymentEmployes = 0;

                    foreach (var viewModel in listToWorkers)
                    {
                        totalPriceWorkers += viewModel.totalPrice.Value;
                        accountPaymentWorkers += viewModel.totalGranted.Value;

                        cell = sheet.Cells[rowIndex, 2];
                        cell.Value = viewModel.createdAt;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3];
                        cell.Value = viewModel.serviceType.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 4];
                        cell.Value = viewModel.customer.name + " " + viewModel.customer.fatherLastname + " " + viewModel.customer.motherLastname;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 5];
                        cell.Value = viewModel.customer.Company.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 6];
                        cell.Value = viewModel.customer.WorkUnit.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 7];
                        cell.Value = "s/. " + viewModel.totalGranted;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 8];
                        cell.Value = "s/. " + viewModel.totalPrice;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        rowIndex = rowIndex + 1;
                    }
                    #endregion Table Body

                    rowIndex = rowIndex + 2;

                    cell = sheet.Cells[rowIndex, 2];
                    cell.Value = "LISTA DE EMPLEADOS";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 11;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableHeaderColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex = rowIndex + 1;

                    var index2 = 0;
                    foreach (var header in headers)
                    {
                        cell = sheet.Cells[rowIndex, 2 + index2];
                        cell.Value = header;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 11;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableHeaderColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                        index2++;
                    }
                    rowIndex = rowIndex + 1;

                    foreach (var viewModel in listToEmployes)
                    {

                        totalPriceEmployes += viewModel.totalPrice.Value;
                        accountPaymentEmployes += viewModel.totalGranted.Value;

                        cell = sheet.Cells[rowIndex, 2];
                        cell.Value = viewModel.createdAt;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 3];
                        cell.Value = viewModel.serviceType.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 4];
                        cell.Value = viewModel.customer.name + " " + viewModel.customer.fatherLastname + " " + viewModel.customer.motherLastname;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 5];
                        cell.Value = viewModel.customer.Company.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 6];
                        cell.Value = viewModel.customer.WorkUnit.name;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 7];
                        cell.Value = "s/. " + viewModel.totalGranted;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        cell = sheet.Cells[rowIndex, 8];
                        cell.Value = "s/. " + viewModel.totalPrice;
                        cell.Style.Font.Bold = true;
                        cell.Style.Font.Size = 10;
                        cell.Style.HorizontalAlignment = alignHorizontalCenter;
                        cell.Style.VerticalAlignment = alignVerticalCenter;
                        fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(tableBodyColor);
                        border = cell.Style.Border;
                        border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                        rowIndex = rowIndex + 1;
                    }

                    rowIndex = rowIndex + 2;

                    #region Final Description Table

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = "Numero de servicios";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "Precio promedio por servicio";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "Monto subvencionado total";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    cell = sheet.Cells[rowIndex, 4];
                    cell.Value = "Datos de obreros: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = listToWorkers.Count + " unidades";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "s/. " + Math.Round(accountPaymentWorkers / listToWorkers.Count, 2);
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "s/. " + accountPaymentWorkers;
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    cell = sheet.Cells[rowIndex, 4];
                    cell.Value = "Datos de empleados: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = listToEmployes.Count + " unidades";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "s/. " + Math.Round(accountPaymentEmployes / listToEmployes.Count, 2);
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "s/. " + accountPaymentEmployes;
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    rowIndex++;

                    cell = sheet.Cells[rowIndex, 4];
                    cell.Value = "Numero de servicios total: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 5];
                    cell.Value = listToEmployes.Count + listToWorkers.Count + " unidades";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    cell = sheet.Cells[rowIndex, 6];
                    cell.Value = "Pago total: ";
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalRight;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;

                    cell = sheet.Cells[rowIndex, 7];
                    cell.Value = "s/. " + Math.Round(totalPriceEmployes + totalPriceWorkers, 2);
                    cell.Style.Font.Bold = true;
                    cell.Style.Font.Size = 10;
                    cell.Style.HorizontalAlignment = alignHorizontalCenter;
                    cell.Style.VerticalAlignment = alignVerticalCenter;
                    fill = cell.Style.Fill;
                    fill.PatternType = ExcelFillStyle.Solid;
                    fill.BackgroundColor.SetColor(tableBodyColor);
                    border = cell.Style.Border;
                    border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                    #endregion Final Description Table

                }

                sheet.Protection.AllowInsertRows = false;
                sheet.Protection.AllowSort = true;
                sheet.Protection.AllowSelectUnlockedCells = true;
                sheet.Protection.AllowAutoFilter = true;
                sheet.Protection.IsProtected = true;

                FileInfo excelFile = new FileInfo(filepath + filename);
                excelPackage.SaveAs(excelFile);
            }
        }

    }
}
