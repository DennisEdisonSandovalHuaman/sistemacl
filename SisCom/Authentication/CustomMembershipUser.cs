﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using SisCom.Models;

namespace SisCom.Authentication
{
    public class CustomMembershipUser : MembershipUser
    {
        #region User Properties  

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string role { get; set; }

        #endregion

        public CustomMembershipUser(user user) : base("CustomMembership", user.dni, user.idUser, string.Empty, string.Empty, string.Empty, true, false, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now, DateTime.Now)
        {
            UserId = user.idUser;
            FirstName = user.name;
            LastName = user.fatherLastName + " " + user.motherLastName;
            role = user.role.name;
        }
    }
}