﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Pos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SisCom.Data.IRepositories
{
    public interface IPosRepository : IGenericRepository<pos>
    {
        List<pos> GetPos();
        List<pos> GetActivePos();
        pos FindById(int idPos);
        PaginatorGeneric<pos, PosViewModel> GetAllByPaged(PaginatorGeneric<pos, PosViewModel> paginator);
        List<AllOpenPos> GetAllOpenPos();
    }
}
