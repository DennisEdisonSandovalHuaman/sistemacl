﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SisCom.Data.IRepositories
{
    public interface IGenericRepository<T>
    {
        T Insert(T entidad);
        void Delete(int idEntity);
        void Update(T entidad);
        int Count(Expression<Func<T, bool>> where);
        T GetById(int id);
    }
}
