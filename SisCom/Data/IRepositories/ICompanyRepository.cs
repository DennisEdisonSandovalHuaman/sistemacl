﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SisCom.Data.IRepositories
{
    public interface ICompanyRepository : IGenericRepository<company>
    {
        List<company> GetCompany();
        company GetCompanyByName(string companyName);
        company FindById(int idCompany);
        PaginatorGeneric<company, CompanyViewModel> GetAllByPaged(PaginatorGeneric<company, CompanyViewModel> paginator);
    }
}
