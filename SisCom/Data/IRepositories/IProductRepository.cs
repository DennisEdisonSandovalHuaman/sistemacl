﻿using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Products;
using System.Collections.Generic;

namespace SisCom.Data.IRepositories
{
    public interface IProductRepository : IGenericRepository<product>
    {
        List<product> GetProducts();
        product FindById(int idProduct);
        void DecrementStockByIdProduct(long idProduct, int quantity);
        List<product> GetProductsToSaleByBranchID(int idBranch);
        PaginatorGeneric<product, ProductViewModel> GetAllByPagedFilters(ProductsFilters filters);
    }
}
