﻿using SisCom.Models;
using System.Collections.Generic;

namespace SisCom.Data.IRepositories
{
    public interface IOrganizationRepository : IGenericRepository<organization>
    {
        List<organization> GetOrganizations();
        organization FindById(int idOrganization);
    }
}
