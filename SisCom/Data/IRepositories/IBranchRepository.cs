﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SisCom.Data.IRepositories 
{
    public interface IBranchRepository : IGenericRepository<branch>
    {
        List<branch> GetBranch();
        branch FindById(int idBranch);
        PaginatorGeneric<branch, BranchViewModel> GetAllByPaged(PaginatorGeneric<branch, BranchViewModel> paginator);
    }
}
