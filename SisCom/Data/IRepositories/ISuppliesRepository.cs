﻿using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Supplies;
using System.Collections.Generic;

namespace SisCom.Data.IRepositories
{
    public interface ISuppliesRepository : IGenericRepository<supplies>
    {
        List<supplies> GetSupplies();
        void VirtualDelete(int id);
        PaginatorGeneric<supplies, SuppliesViewModel> GetAllByPagedFilters(SuppliesFilters filters);
    }
}