﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.ServiceType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SisCom.Data.Repositories.ServiceTypeRepository;

namespace SisCom.Data.IRepositories
{
    public interface IServiceTypeRepository : IGenericRepository<service_type>
    {
        List<service_type> GetServiceType();
        service_type FindById(int idServiceType);
        serviceTypeToSale GetCurrentServiceType();
        PaginatorGeneric<service_type, ServiceTypeViewModel> GetAllByPaged(PaginatorGeneric<service_type, ServiceTypeViewModel> paginator);
    }
}
