﻿using SisCom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SisCom.Structure.ViewModels.Attendance;
using SisCom.Helpers;

namespace SisCom.Data.IRepositories
{
    public interface IAttendanceRepository : IGenericRepository<attendance>
    {
        List<attendance> GetAttendance();
        attendance GetCurrentAttendaceByUserID(int idUser);
        void IncrementCurrentCash(int idAttendance, double? accountPayment);
        void DecrementCurrentCash(int idAttendance, double? accountPayment);
        attendance FindById(int? idAttendance);
        PaginatorGeneric<attendance, AttendanceViewModel> GetAllByPaged(PaginatorGeneric<attendance, AttendanceViewModel> paginator);
    }
}
