﻿using SisCom.Models;
using System.Collections.Generic;

namespace SisCom.Data.IRepositories
{
    public interface IRoleRepository
    {
        List<role> GetRole();
        role FindById(int idRole);
    }
}
