﻿using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Customers;
using System.Collections.Generic;

namespace SisCom.Data.IRepositories
{
    public interface ICustomersRepository : IGenericRepository<customer>
    {
        List<customer> GetCustomers();
        customer FindById(int idCustomer);
        customer GetByDNI(string dni);
        bool VerifyDeliveryOfFoolToCustomerById(int idCustomer);
        PaginatorGeneric<customer, CustomersViewModel> GetAllByPagedFilters(CustomersFilters filters);
    }
}
