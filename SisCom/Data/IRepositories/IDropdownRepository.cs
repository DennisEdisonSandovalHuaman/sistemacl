﻿using SisCom.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.IRepositories
{
    public interface IDropdownRepository
    {
        List<DropdownOption> WorkUnit();
        List<DropdownOption> ServiceType();
        List<DropdownOption> Branch();
        List<DropdownOption> User();
        List<DropdownOption> PosByUser(int idUser);
        List<DropdownOption> Role();
        List<DropdownOption> Company();
    }
}