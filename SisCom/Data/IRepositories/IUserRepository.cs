﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.IRepositories
{
    public interface IUserRepository : IGenericRepository<user>
    {
        List<user> GetUsers();
        user FindById(int idUser);
        PaginatorGeneric<user, UsersViewModel> GetAllByPaged(PaginatorGeneric<user, UsersViewModel> paginator);
    }
}