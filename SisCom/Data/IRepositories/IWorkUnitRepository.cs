﻿using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.WorkUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SisCom.Data.IRepositories
{
    public interface IWorkUnitRepository : IGenericRepository<work_unit>
    {
        List<work_unit> GetWorkUnit();
        work_unit FindById(int idWorkUnit);
        PaginatorGeneric<work_unit, WorkUnitViewModel> GetAllByPaged(PaginatorGeneric<work_unit, WorkUnitViewModel> paginator);
    }
}
