﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Reports;
using SisCom.Structure.ViewModels.Sales;

namespace SisCom.Data.IRepositories
{
    public interface ISaleRepository : IGenericRepository<sale>
    {
        List<sale> GetSales();
        List<sale> GetSalesByUserID(int idUser);
        List<sale> GetAllSales();
        List<sale> GetSalesClassicToReport(DateTime initDate, DateTime endDate, int idPos);
        List<sale> GetSalesFastToReport(FastSaleFilterViewModel filters);
        List<sale_detail> GetDetailsByIdSale(int idSale);
        bool VirtualDelete(int idSale);
        PaginatorGeneric<sale, SaleIndexViewModel> GetAllByPaged(PaginatorGeneric<sale, SaleIndexViewModel> paginator);
        PaginatorGeneric<sale, SaleIndexViewModel> GetAllByPagedAndUserID(PaginatorGeneric<sale, SaleIndexViewModel> paginator, int idUser);

        //Paginadores
        PaginatorGeneric<sale, SaleToReportViewModel> GetEmployeSalesFastToReportByPagedFilters(FastSaleFilterViewModel filters);
        FastSaleFilterViewModel GetWorkerSalesFastToReportByPagedFilters(FastSaleFilterViewModel filters);
    }
}
