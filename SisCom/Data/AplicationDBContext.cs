﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using SisCom.Models;

namespace SisCom.Data
{
    public class AplicationDBContext : DbContext
    {
        public DbSet<product> Product { get; set; }
        public DbSet<sale> Sale { get; set; }
        public DbSet<customer> Customer { get; set; }
        public DbSet<organization> Organization { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //    modelBuilder.Entity<product>().ToTable("Product");
            
        //    modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        //    modelBuilder.Entity<sale>().ToTable("Sale");
        //}
    }
}