﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class CompanyRepository : GenericRepository<company>, ICompanyRepository
    {
        private ComEdbEntities _db;

        public CompanyRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<company> GetCompany()
        {
            using (var db = new ComEdbEntities())
            {
                return db.company.ToList();
            }
        }
        public company FindById(int idCompany)
        {
            return _db.company.Find(idCompany);
        }
        public company GetCompanyByName(string companyName)
        {
            return _db.company.FirstOrDefault(x => x.name.Equals(companyName, StringComparison.InvariantCultureIgnoreCase));
        }
        public PaginatorGeneric<company, CompanyViewModel> GetAllByPaged(PaginatorGeneric<company, CompanyViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.company.ToList()
                        .OrderByDescending(x => x.idCompany);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }
    }
}