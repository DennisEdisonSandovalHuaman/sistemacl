﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.ServiceType;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class ServiceTypeRepository : GenericRepository<service_type>, IServiceTypeRepository
    {
        private ComEdbEntities _db;

        public ServiceTypeRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<service_type> GetServiceType()
        {
            using (var db = new ComEdbEntities())
            {
                return db.service_type.ToList();
            }
        }
        public service_type FindById(int idServiceType)
        {
            return _db.service_type.Find(idServiceType);
        }
        public serviceTypeToSale GetCurrentServiceType()
        {
            var dateTime = DateTime.Now;
            var time = dateTime.TimeOfDay;
            var serviceType = (from service_type in _db.service_type
                               where DbFunctions.CreateTime(service_type.endTime.Hour, service_type.endTime.Minute, service_type.endTime.Second) > time
                               where DbFunctions.CreateTime(service_type.startTime.Hour, service_type.startTime.Minute, service_type.startTime.Second) < time
                               select new serviceTypeToSale()
                               {
                                   idServiceType = service_type.idServiceType,
                                   name = service_type.name,
                                   price = service_type.price,
                                   startTime = service_type.startTime,
                                   endTime = service_type.endTime,
                               }).FirstOrDefault();
            return serviceType;
        }
        public class serviceTypeToSale
        {
            public int idServiceType { get; set; }
            public string name { get; set; }
            public double price { get; set; }
            public DateTime startTime { get; set; }
            public DateTime endTime { get; set; }
        }
        public PaginatorGeneric<service_type, ServiceTypeViewModel> GetAllByPaged(PaginatorGeneric<service_type, ServiceTypeViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.service_type.ToList()
                        .OrderByDescending(x => x.idServiceType);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }

    }
}