﻿using SisCom.Data.IRepositories;
using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Customers;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SisCom.Data.Repositories
{
    public class CustomersRepository : GenericRepository<customer>, ICustomersRepository
    {
        private ComEdbEntities _db;
        IServiceTypeRepository _serviceTypeRepository;

        public CustomersRepository(ComEdbEntities db, IServiceTypeRepository serviceTypeRepository)
        {
            _db = db;
            _serviceTypeRepository = serviceTypeRepository;
        }
        public List<customer> GetCustomers()
        {
            using (var db = new ComEdbEntities())
            {
                return db.customer.ToList();
            }
        }
        public customer FindById(int idCustomer)
        {
            return _db.customer.Find(idCustomer);
        }

        public bool VerifyDeliveryOfFoolToCustomerById(int idCustomer)
        {

            var currentService = _serviceTypeRepository.GetCurrentServiceType();
            var date = DateTime.Now;
            var saleModel = (from sale in _db.sale
                             join serviceType in _db.service_type on sale.idServiceType equals serviceType.idServiceType
                             join custom in _db.customer on sale.idCustomer equals custom.idCustomer

                             orderby sale.createdAt descending
                             where sale.idCustomer == idCustomer
                             //where serviceType.name == currentService.name
                             where custom.dni != "0"
                             select new
                             {
                                 idAttendace = sale.idAttendance,
                                 serviceName = serviceType.name,
                                 date = sale.createdAt,
                             }).FirstOrDefault();

            if (saleModel == null)
            {
                return false;
            }
            else
            {
                if (saleModel.date.Date.CompareTo(DateTime.Now.Date) < 0)
                    return false;
                else
                    return true;
            }
        }

        public customer GetByDNI(string dni)
        {
            return _db.customer.Where(customer => customer.dni == dni).FirstOrDefault();
        }
        public PaginatorGeneric<customer, CustomersViewModel> GetAllByPagedFilters(CustomersFilters filters)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (filters.Page - 1) * filters.PageSize;
                var queryComplete = db.customer.ToList()
                        .OrderBy(x => x.name)
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByName), x => x.name.ToUpper().Contains(filters.FilterByName.ToUpper()) || x.fatherLastname.ToUpper().Contains(filters.FilterByName.ToUpper()) || x.motherLastname.ToUpper().Contains(filters.FilterByName.ToUpper()))
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByDNI), x => x.dni.Contains(filters.FilterByDNI))
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByCompany), x => x.company.name.ToUpper().Contains(filters.FilterByCompany.ToUpper()));

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(filters.PageSize);
                filters.ResultsModel = queryPaged.ToList();
                filters.TotalRow = count;

                return filters;
            }
        }
    }
}