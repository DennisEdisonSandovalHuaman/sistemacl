﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Attendance;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class AttendanceRepository : GenericRepository<attendance>, IAttendanceRepository
    {
        private ComEdbEntities _db;

        public AttendanceRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<attendance> GetAttendance()
        {
            return _db.attendance.ToList();
        }
        public attendance GetCurrentAttendaceByUserID(int idUser)
        {
            return _db.attendance.Where(attend => attend.pos.idUser == idUser && attend.endAt == null).FirstOrDefault();
        }
        public void IncrementCurrentCash(int idAttendace, double? accountPayment)
        {
            var attendaceMD = _db.attendance.Find(idAttendace);
            attendaceMD.currentCash = attendaceMD.currentCash + accountPayment.Value;
            _db.SaveChanges();
        }
        public void DecrementCurrentCash(int idAttendace, double? accountPayment)
        {
            var attendaceMD = _db.attendance.Find(idAttendace);
            attendaceMD.currentCash = attendaceMD.currentCash - accountPayment.Value;
            _db.SaveChanges();
        }
        public attendance FindById(int? idAttendance)
        {
            return _db.attendance.Find(idAttendance);
        }
        public PaginatorGeneric<attendance, AttendanceViewModel> GetAllByPaged(PaginatorGeneric<attendance, AttendanceViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.attendance.ToList()
                        .OrderByDescending(x => x.idAttendance);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }
    }
}