﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class UserRepository : GenericRepository<user>, IUserRepository
    {
        private ComEdbEntities _db;

        public UserRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<user> GetUsers()
        {
            using (var db = new ComEdbEntities())
            {
                return db.user.ToList();
            }
        }
        public user FindById(int idUser)
        {
            return _db.user.Find(idUser);
        }
        public PaginatorGeneric<user, UsersViewModel> GetAllByPaged(PaginatorGeneric<user, UsersViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.user.ToList()
                        .OrderByDescending(x => x.idUser);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }
    }
}