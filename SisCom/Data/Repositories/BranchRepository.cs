﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Data.Repositories
{
    public class BranchRepository : GenericRepository<branch>, IBranchRepository
    {
        private ComEdbEntities _db;

        public BranchRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<branch> GetBranch()
        {
            using (var db = new ComEdbEntities())
            {
                return db.branch.ToList();
            }
        }
        public branch FindById(int idBranch)
        {
            return _db.branch.Find(idBranch);
        }
        public PaginatorGeneric<branch, BranchViewModel> GetAllByPaged(PaginatorGeneric<branch, BranchViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.branch.ToList()
                        .OrderByDescending(x => x.idBranch);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }
    }
}