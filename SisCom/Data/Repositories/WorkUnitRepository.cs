﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.WorkUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class WorkUnitRepository : GenericRepository<work_unit>, IWorkUnitRepository
    {
        private ComEdbEntities _db;

        public WorkUnitRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<work_unit> GetWorkUnit()
        {
            using (var db = new ComEdbEntities())
            {
                return db.work_unit.ToList();
            }
        }
        public work_unit FindById(int idWorkUnit)
        {
            return _db.work_unit.Find(idWorkUnit);
        }
        public PaginatorGeneric<work_unit, WorkUnitViewModel> GetAllByPaged(PaginatorGeneric<work_unit, WorkUnitViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.work_unit.ToList()
                        .OrderByDescending(x => x.idWorkUnit);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }
    }
}