﻿using SisCom.Data.IRepositories;
using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Supplies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class SuppliesRepository : GenericRepository<supplies>, ISuppliesRepository
    {
        private readonly ComEdbEntities _db;

        public SuppliesRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<supplies> GetSupplies()
        {
            using (var db = new ComEdbEntities())
            {
                return db.supplies.ToList();
            }
        }
        public void VirtualDelete(int id)
        {
            _db.supplies.Find(id).isActive = false;
            _db.SaveChanges();
        }
        public PaginatorGeneric<supplies, SuppliesViewModel> GetAllByPagedFilters(SuppliesFilters filters)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (filters.Page - 1) * filters.PageSize;
                var queryComplete = db.supplies.ToList()
                        .OrderByDescending(x => x.idSupplies)
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByName), x => x.name.ToUpper().Contains(filters.FilterByName.ToUpper()))
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByBranch), x => x.branch.name.ToUpper().Contains(filters.FilterByBranch.ToUpper()));

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(filters.PageSize);
                filters.ResultsModel = queryPaged.ToList();
                filters.TotalRow = count;

                return filters;
            }
        }
    }
}