﻿using SisCom.Data.IRepositories;
using SisCom.Extensions;
using SisCom.Models;
using SisCom.Structure.ViewModels.WorkUnit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class DropdownRepository: IDropdownRepository
    {
        private ComEdbEntities _db;
        private IPosRepository _posRepository;
        private IUserRepository _userRepository;

        public DropdownRepository(ComEdbEntities db, IPosRepository posRepository, IUserRepository userRepository)
        {
            _db = db;
            _posRepository = posRepository;
            _userRepository = userRepository;
        }
        public List<DropdownOption> WorkUnit()
        {
            return _db.work_unit.Select(x => new DropdownOption
                            {
                                Value = x.idWorkUnit,
                                Text = x.name
                            }).ToList();
        }
        public List<DropdownOption> ServiceType()
        {
            return _db.service_type.Select(x => new DropdownOption
            {
                Value = x.idServiceType,
                Text = x.name
            }).ToList();
        }
        public List<DropdownOption> Branch()
        {
            return _db.branch.Select(x => new DropdownOption
            {
                Value = x.idBranch,
                Text = x.name
            }).ToList();
        }
        public List<DropdownOption> User()
        {
            var pos = _posRepository.GetActivePos().ToList();
            var users = _userRepository.GetUsers();
    
            if (pos.Count() != 0)
            {
                foreach (var auxPos in pos)
                {
                    users.RemoveAll(x => x.idUser == auxPos.idUser);
                }

                return users.Select(x => new DropdownOption
                {
                    Value = x.idUser,
                    Text = x.name + " " + x.fatherLastName + " " + x.motherLastName
                }).ToList();
            }
            else
            {
                return _db.user.Select(x => new DropdownOption
                {
                    Value = x.idUser,
                    Text = x.name + " " + x.fatherLastName + " " + x.motherLastName
                }).ToList();
            }        
        }
        public List<DropdownOption> PosByUser(int idUser)
        {
            return _db.pos.Where(y => y.idUser == idUser).Select(x => new DropdownOption
            {
                Value = x.idPos,
                Text = x.name
            }).ToList();
        }
        public List<DropdownOption> Role()
        {
            return _db.role.Select(x => new DropdownOption
            {
                Value = x.idRole,
                Text = x.name
            }).ToList();
        }
        public List<DropdownOption> Company()
        {
            return _db.company.Where(y => y.isActive == true).Select(x => new DropdownOption
            {
                Value = x.idCompany,
                Text = x.name
            }).ToList();
        }
    }
    public class listUserPos
    {
        public int idUser;
        public string nombres;
    }
}