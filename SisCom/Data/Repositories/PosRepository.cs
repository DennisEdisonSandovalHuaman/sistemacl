﻿using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.Mappers.Pos;
using SisCom.Structure.ViewModels.Pos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class PosRepository : GenericRepository<pos>, IPosRepository
    {
        private ComEdbEntities _db;
        private PosMapper _posMapper;

        public PosRepository(ComEdbEntities db, PosMapper posMapper)
        {
            _db = db;
            _posMapper = posMapper;
        }
        public List<pos> GetPos()
        {
            using (var db = new ComEdbEntities())
            {
                return db.pos.ToList();
            }
        }
        public List<pos> GetActivePos()
        {
            using (var db = new ComEdbEntities())
            {
                return db.pos.Where(x => x.isActive == true).ToList();
            }
        }
        public pos FindById(int idPos)
        {
            return _db.pos.Find(idPos);
        }
        public PaginatorGeneric<pos, PosViewModel> GetAllByPaged(PaginatorGeneric<pos, PosViewModel> paginator)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (paginator.Page - 1) * paginator.PageSize;
                var queryComplete = db.pos.ToList()
                        .OrderByDescending(x => x.idPos);

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
                paginator.ResultsModel = queryPaged.ToList();
                paginator.TotalRow = count;

                return paginator;
            }
        }
        public List<AllOpenPos> GetAllOpenPos()
        {
            try
            {
                using (var db = new ComEdbEntities())
                {
                    var pos = db.pos.ToList();
                    List<AllOpenPos> listOpenPos = new List<AllOpenPos>();

                    foreach (var listPos in pos)
                    {
                        //var listAttendancesForDay = db.attendance.Where(y => y.startAt.Day == DateTime.Now.Day && y.startAt.Hour == DateTime.Now.Hour && y.idPos == listPos.idPos).FirstOrDefault();
                        var listAttendancesForDay = db.attendance.Where(y => y.startAt.Day == DateTime.Now.Day && y.idPos == listPos.idPos && y.endAt == null).FirstOrDefault();
                        
                        if (listAttendancesForDay != null)
                        {
                            listOpenPos.Add(new AllOpenPos
                            {
                                idAttendance = listAttendancesForDay.idAttendance,
                                dni = listAttendancesForDay.pos.user.dni,
                                user = listAttendancesForDay.pos.user.fatherLastName + " " + listAttendancesForDay.pos.user.motherLastName + ", " + listAttendancesForDay.pos.user.name,
                                pos = listAttendancesForDay.pos.name,
                                openHour = listAttendancesForDay.startAt.ToShortTimeString().ToString(),
                                state = true
                            });
                        }
                        else
                        {
                            listOpenPos.Add(new AllOpenPos
                            {
                                idAttendance = 0,
                                dni = listPos.user.dni,
                                user = listPos.user.fatherLastName + " " + listPos.user.motherLastName + ", " + listPos.user.name,
                                pos = listPos.name,
                                openHour = "-",
                                state = false
                            });
                        }
                    }

                    return listOpenPos;
                }
            }
            catch (Exception)
            {
                return new List<AllOpenPos>();

            }        
        }
    }
}