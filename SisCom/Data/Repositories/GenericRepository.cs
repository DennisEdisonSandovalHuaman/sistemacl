﻿using System;
using System.Collections.Generic;
using System.Linq;
using SisCom.Models;
using SisCom.Data.IRepositories;
using System.Linq.Expressions;

namespace SisCom.Data.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class, new()
    {
        public int Count(Expression<Func<T, bool>> where)
        {
            using (var db = new ComEdbEntities())
            {
                return db.Set<T>().Where(where).Count();
            }
        }

        public void Delete(int idEntity)
        {
            using (var db = new ComEdbEntities())
            {
                db.Set<T>().Remove(GetById(idEntity));
                db.SaveChanges();
            }
        }

        public T GetById(int id)
        {
            using (var db = new ComEdbEntities())
            {
                return db.Set<T>().Find(id);
            }
        }

        public T Insert(T entidad)
        {
            using (var db = new ComEdbEntities())
            {
                db.Set<T>().Add(entidad);
                db.SaveChanges();
                return entidad;
            }
        }

        public void Update(T entidad)
        {
            using (var db = new ComEdbEntities())
            {
                db.Entry(entidad).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
        }
    }
}