﻿using SisCom.Data.IRepositories;
using SisCom.Models;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Data.Repositories
{
    public class RoleRepository : GenericRepository<role>, IRoleRepository
    {
        private ComEdbEntities _db;

        public RoleRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<role> GetRole()
        {
            using (var db = new ComEdbEntities())
            {
                return db.role.ToList();
            }
        }
        public role FindById(int idRole)
        {
            return _db.role.Find(idRole);
        }
    }
}