﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using SisCom.Data.IRepositories;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Reports;
using SisCom.Structure.ViewModels.Sales;

namespace SisCom.Data.Repositories
{
    public class SaleRepository : GenericRepository<sale>, ISaleRepository
    {
        private IPosRepository _posRepository;
        private ICompanyRepository _companyRepository;
        private readonly ComEdbEntities _db;

        public SaleRepository(ICompanyRepository companyRepository, IPosRepository posRepository, ComEdbEntities db)
        {
            _posRepository = posRepository;
            _companyRepository = companyRepository;
            _db = db;
        }
        public List<sale_detail> GetDetailsByIdSale(int idSale)
        {
            return _db.sale_detail.Where(detail => detail.idSale == idSale).ToList();
        }

        public List<sale> GetSales()
        {
            using (var db = new ComEdbEntities())
            {
                return db.sale.OrderByDescending(x => x.createdAt).ToList();
            }
        }
        public List<sale> GetSalesByUserID(int idUser)
        {
            return _db.sale.OrderByDescending(x => x.createdAt).Where(x => x.attendance.pos.idUser == idUser).ToList();
        }
        public List<sale> GetAllSales()
        {
            return _db.sale.OrderByDescending(x => x.createdAt).ToList();
        }

        public List<sale> GetSalesClassicToReport(DateTime initDate, DateTime endDate, int idPos)
        {
            var endDate2 = endDate.AddDays(1);
            if (idPos == 0)
                return _db.sale.Where(sale => sale.deletedAt == null && sale.createdAt >= initDate && sale.createdAt <= endDate2 && sale.idServiceType == null).ToList();

            if (idPos != 0)
                return _db.sale.Where(sale => sale.deletedAt == null && sale.createdAt >= initDate && sale.createdAt <= endDate2 && sale.attendance.idPos == idPos && sale.idServiceType == null).ToList();

            return _db.sale.Where(sale => sale.deletedAt == null && sale.createdAt >= initDate && sale.createdAt <= endDate2 && sale.idServiceType == null).ToList();
        }
        public bool VirtualDelete(int idSale)
        {
            try
            {
                var sale = _db.sale.Find(idSale);
                sale.deletedAt = DateTime.Now;
                _db.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public PaginatorGeneric<sale, SaleIndexViewModel> GetAllByPaged(PaginatorGeneric<sale, SaleIndexViewModel> paginator)
        {
            var queryComplete = _db.sale.OrderByDescending(x => x.createdAt);

            var count = queryComplete.Count();
            var skip = (paginator.Page - 1) * paginator.PageSize;

            var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
            paginator.ResultsModel = queryPaged.ToList();
            paginator.TotalRow = count;

            return paginator;
        }

        public PaginatorGeneric<sale, SaleIndexViewModel> GetAllByPagedAndUserID(PaginatorGeneric<sale, SaleIndexViewModel> paginator, int idUser)
        {
            var queryComplete = _db.sale.OrderByDescending(x => x.createdAt).Where(x => x.attendance.pos.idUser == idUser);

            var count = queryComplete.Count();
            var skip = (paginator.Page - 1) * paginator.PageSize;

            var queryPaged = queryComplete.Skip(skip).Take(paginator.PageSize);
            paginator.ResultsModel = queryPaged.ToList();
            paginator.TotalRow = count;

            return paginator;
        }

        public List<sale> GetSalesFastToReport(FastSaleFilterViewModel filters)
        {
            return GetFinalQuery(filters).ToList();
        }

        //Paginadores
        public PaginatorGeneric<sale, SaleToReportViewModel> GetEmployeSalesFastToReportByPagedFilters(FastSaleFilterViewModel filters)
        {
            try
            {

                IEnumerable<sale> queryPaged = null;
                IEnumerable<sale> query = GetFinalQuery(filters);
                query = query.Where(sale => sale.customer.work_unit.name.Equals("empleado", StringComparison.InvariantCultureIgnoreCase) || sale.customer.work_unit.name.Equals("practicante", StringComparison.InvariantCultureIgnoreCase)).ToList();
                return GenerateEmployPaginator(query, queryPaged, filters);

            }
            catch (Exception)
            {
                return new PaginatorGeneric<sale, SaleToReportViewModel>();
            }
        }
        public FastSaleFilterViewModel GetWorkerSalesFastToReportByPagedFilters(FastSaleFilterViewModel filters)
        {
            try
            {

                IEnumerable<sale> queryPaged = null;
                IEnumerable<sale> query = GetFinalQuery(filters);
                query = query.Where(sale => sale.customer.work_unit.name.Equals("operario", StringComparison.InvariantCultureIgnoreCase));
                return GenerateWorkerPaginator(query, queryPaged, filters);

            }
            catch (Exception)
            {
                return new FastSaleFilterViewModel();
            }
        }

        public IEnumerable<sale> GetFinalQuery(FastSaleFilterViewModel filters)
        {
            var endDate2 = filters.endDate.AddDays(1);

            IEnumerable<sale> newQuery = _db.sale.Where(sale => sale.createdAt >= filters.initDate && sale.createdAt <= endDate2).ToList();
            newQuery = newQuery.Where(sale => sale.deletedAt == null && sale.idServiceType != null && sale.customer.dni != "0");

            if (filters.idCompanies != null && filters.idCompanies.Count != 0)
                newQuery = newQuery
                    .Where(sale => filters.idCompanies.Exists(idCompany => sale.customer.company.idCompany == idCompany))
                    .WhereIf(filters.idWorkUnit != 0, sale => sale.customer.work_unit.idWorkUnit == filters.idWorkUnit)
                    .WhereIf(filters.idServiceType != 0, sale => sale.idServiceType == filters.idServiceType)
                    .WhereIf(filters.idPos != 0, sale => sale.attendance.idPos == filters.idPos);
            else
                newQuery = newQuery.Where(sale => sale.customer.company.idCompany == 0);

            return newQuery;
        }

        public FastSaleFilterViewModel GenerateEmployPaginator(IEnumerable<sale> query, IEnumerable<sale> queryPaged, FastSaleFilterViewModel filters)
        {

            filters.totalPriceEmploy = Convert.ToDouble(query.Sum(sale => sale.totalPrice));
            filters.totalAccountPaymentEmploy = Convert.ToDouble(query.Sum(sale => sale.accountPayment));

            var skip = (filters.Page - 1) * filters.PageSize;
            var count = query.Count();
            queryPaged = query.OrderBy(x => x.createdAt).Skip(skip).Take(filters.PageSize);
            filters.ResultsModel = queryPaged.ToList();
            filters.TotalRow = count;

            return filters;

        }
        public FastSaleFilterViewModel GenerateWorkerPaginator(IEnumerable<sale> query, IEnumerable<sale> queryPaged, FastSaleFilterViewModel filters)
        {

            filters.totalPriceWorker = Convert.ToDouble(query.Sum(sale => sale.totalPrice));
            filters.totalAccountPaymentWorker = Convert.ToDouble(query.Sum(sale => sale.accountPayment));

            var skip = (filters.PageW - 1) * filters.PageSizeW;
            var count = query.Count();
            queryPaged = query.OrderBy(x => x.createdAt).Skip(skip).Take(filters.PageSizeW);
            filters.ResultsModelW = queryPaged.ToList();
            filters.TotalRowW = count;

            return filters;

        }
    }
}