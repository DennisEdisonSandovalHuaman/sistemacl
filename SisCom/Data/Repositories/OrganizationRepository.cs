﻿using SisCom.Data.IRepositories;
using SisCom.Models;
using System.Collections.Generic;
using System.Linq;

namespace SisCom.Data.Repositories
{
    public class OrganizationRepository : GenericRepository<organization>, IOrganizationRepository
    {
        private ComEdbEntities _db;

        public OrganizationRepository(ComEdbEntities db)
        {
            _db = db;
        }
        public List<organization> GetOrganizations()
        {
            using (var db = new ComEdbEntities())
            {
                return db.organization.ToList();
            }
        }
        public organization FindById(int idOrganization)
        {
            return _db.organization.Find(idOrganization);
        }
    }
}