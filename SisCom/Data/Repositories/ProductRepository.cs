﻿using SisCom.Data.IRepositories;
using SisCom.Filters;
using SisCom.Helpers;
using SisCom.Models;
using SisCom.Structure.ViewModels.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SisCom.Data.Repositories
{
    public class ProductRepository : GenericRepository<product>, IProductRepository
    {
        private ComEdbEntities _db;
        IServiceTypeRepository _serviceTypeRepository;
        IUserRepository _userRepository;

        public ProductRepository(ComEdbEntities db, IServiceTypeRepository serviceTypeRepository, IUserRepository userRepository)
        {
            _db = db;
            _serviceTypeRepository = serviceTypeRepository;
            _userRepository = userRepository;
        }

        public List<product> GetProducts()
        {
            using (var db = new ComEdbEntities())
            {
                return db.product.ToList();
            }
        }
        public product FindById(int idProduct)
        {
            return _db.product.Find(idProduct);
        }
        public void DecrementStockByIdProduct(long idProduct, int quantity)
        {
            using (var db = new ComEdbEntities())
            {
                var productModel = (from product in db.product where product.idProduct == idProduct select product).FirstOrDefault();
                if (productModel.currentStock != null)
                {
                    productModel.currentStock = productModel.currentStock - quantity;
                    db.SaveChanges();
                }
            }
        }
        public List<product> GetProductsToSaleByBranchID(int idBranch)
        {
            var currentService = _serviceTypeRepository.GetCurrentServiceType();
            return _db.product.Where(product => product.currentStock > 0 && product.idBranch == idBranch && product.isActive == true && product.idServiceType == currentService.idServiceType || product.idServiceType == null).ToList();
        }
        public PaginatorGeneric<product, ProductViewModel> GetAllByPagedFilters(ProductsFilters filters)
        {
            using (var db = new ComEdbEntities())
            {
                var skip = (filters.Page - 1) * filters.PageSize;
                var queryComplete = db.product.ToList()
                        .OrderByDescending(x => x.idProduct)
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByName), x => x.name.ToUpper().Contains(filters.FilterByName.ToUpper()))
                        .WhereIf(!string.IsNullOrEmpty(filters.FilterByServiceType), x => x.service_type.name.ToUpper().Contains(filters.FilterByServiceType.ToUpper()));

                var count = queryComplete.Count();
                var queryPaged = queryComplete.Skip(skip).Take(filters.PageSize);
                filters.ResultsModel = queryPaged.ToList();
                filters.TotalRow = count;

                return filters;
            }
        }
    }
}