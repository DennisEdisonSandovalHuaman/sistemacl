﻿window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');
// window.VueBootstrapTypeahead = require('vue-bootstrap-typeahead').default;
window.toastr = require('toastr');
window.Swal = require('sweetalert2');
// window.Footable = require('footable/compiled/footable');

require("popper.js");
require("bootstrap");
require("@fortawesome/fontawesome-free/js/all");
require("moment");
// require("eonasdan-bootstrap-datetimepicker");
require("v-tooltip");
// require("select2");
// require("jquery-validation");
// require("./waitMe.min.js");
// require("./menu.js");
// require("./footable.js");
// require("./generalFunctions.js");

// Vue.component('signature-process-component', require('./../../js/components/SignatureProcessComponent.vue').default);
Vue.component('TableComponent', require('./components/TableComponent.vue').default);
// Vue.component('VueBootstrapTypeahead', VueBootstrapTypeahead);

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

window.pagesNumber = function (paginate, offset) {
    if (!paginate.to) {
        return [];
    }

    var from = paginate.current_page - offset;
    if (from < 1) {
        from = 1;
    }

    var to = from + (offset * 2);
    if (to >= paginate.last_page) {
        to = paginate.last_page;
    }

    var pagesArray = [];
    while (from <= to) {
        pagesArray.push(from);
        from++;
    }
    return pagesArray;
};

window.disableButtonByID = function (id) {
    document.getElementById(id).disabled = true;
};


window.$ = require('jquery');
window.JQuery = require('jquery');

window.pagesNumber = function (paginate, offset) {
    if (!paginate.to) {
        return [];
    }

    var from = paginate.current_page - offset;
    if (from < 1) {
        from = 1;
    }

    var to = from + (offset * 2);
    if (to >= paginate.last_page) {
        to = paginate.last_page;
    }

    var pagesArray = [];
    while (from <= to) {
        pagesArray.push(from);
        from++;
    }
    return pagesArray;
};

window.disableButtonByID = function (id) {
    document.getElementById(id).disabled = true;
};

window.preloader = function (data) {
    if (data === "show") {
        $(".se-pre-con").show();
    } else if (data === "hidden") {
        $(".se-pre-con").hide();
    }
};
window.showAlertSetTimeout = function (message, type, valueInAlert = '', urlToGo) {
    Swal.fire({
        timer: 1500,
        type: type,
        title: message,
        text: valueInAlert,
    });
    setTimeout(() => {
        window.location.href = urlToGo;
    }, 2000);
};
window.showAlert = function (title, type, message = '') {
    return Swal.fire({
        timer: 4000,
        type: type,
        title: title,
        text: message,
    });
};
window.showToast = function (title, type, message = '') {
    return Swal.fire({
        toast: true,
        timer: 4000,
        animation: true,
        timerProgressBar: true,
        customClass: {
            popup: 'animated tada',
        },
        position: 'top-end',
        type: type,
        title: title,
        text: message,
        showConfirmButton: false,
    });
};
window.showAlertWithOptions = function (title, type, message = '', confirmButtonText, cancelButtonText) {
    return Swal.fire({
        type: type,
        title: title,
        text: message,
        showCancelButton: true,
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText,
        cancelButtonColor: '#d33',
    }).then((result) => {
        return result.value;
    });
};

const app = new Vue({
    // el: '#app'
});
