///**
// * First we will load all of this project's JavaScript dependencies which
// * includes Vue and other libraries. It is a great starting point when
// * building robust, powerful web applications using Vue and Laravel.
// */

//require('./bootstrap');

//window.$ = require('jquery');
//window.JQuery = require('jquery');

//window.Vue = require('vue');

///**
// * The following block of code may be used to automatically register your
// * Vue components. It will recursively scan this directory for the Vue
// * components and automatically register them with their "basename".
// *
// * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
// */

//// const files = require.context('./', true, /\.vue$/i);
//// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
//Vue.component('TableComponent', require('./components/TableComponent.vue').default);
//Vue.component('BarchartComponent', require('./components/componentCharts/BarchartComponent.vue').default);
//// Vue.component('TableComponent', require('./components/TableComponent.vue').default);


///**
// * Next, we will create a fresh Vue application instance and attach it to
// * the page. Then, you may begin adding components to this application
// * or customize the JavaScript scaffolding to fit your unique needs.
// */
//window.pagesNumber =  function (paginate, offset) {
//    if (!paginate.to) {
//        return [];
//    }

//    var from = paginate.current_page - offset;
//    if (from < 1) {
//        from = 1;
//    }

//    var to = from + (offset * 2);
//    if (to >= paginate.last_page) {
//        to = paginate.last_page;
//    }

//    var pagesArray = [];
//    while (from <= to) {
//        pagesArray.push(from);
//        from++;
//    }
//    return pagesArray;
//};

//window.disableButtonByID = function(id) {
//    document.getElementById(id).disabled = true;
//};

//window.preloader = function(data) {
//    if (data === "show") {
//        $(".se-pre-con").show();
//    } else if (data === "hidden") {
//        $(".se-pre-con").hide();
//    }
//};
//window.showAlertSetTimeout = function(message, type, valueInAlert = '', urlToGo) {
//    Swal.fire({
//        timer: 1500,
//        type: type,
//        title: message,
//        text: valueInAlert,
//    });
//    setTimeout(() => {
//        window.location.href = urlToGo;
//    }, 2000);
//};
//window.showAlert = function(message, type, valueInAlert = '') {
//    return Swal.fire({
//        timer: 3000,
//        type: type,
//        title: message,
//        text: valueInAlert,
//    });
//};
//window.showToast = function(message, type, valueInToast = '') {
//    return Swal.fire({
//        timer: 3000,
//        animation: false,
//        customClass: {
//            popup: 'animated tada'
//        },
//        position: 'top-end',
//        type: type,
//        title: message,
//        text: valueInToast,
//        showConfirmButton: false
//    });
//};
//window.showAlertWithOptions = function(message, type, valueInAlert = '', confirmButtonText, cancelButtonText) {
//    return Swal.fire({
//        type: type,
//        title: message,
//        text: valueInAlert,
//        showCancelButton: true,
//        confirmButtonText: confirmButtonText,
//        cancelButtonText: cancelButtonText,
//        cancelButtonColor: '#d33',
//    }).then((result) => {
//        return result.value;
//    });
//};

//const app = new Vue({
//    // el: '#app'
//});
