﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#loginVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			dni: "",
			password: "",
			message: "",
			state: false
		},
		mounted() {
			preloader('hidden');
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'error');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'loginJson':
						this.state = response.state;
						if (this.state === true) {
							if (response.session === 0) {
								this.message = response.Message;
							} else {
								location.href = response.urlRedirect;
							}
						} else {
							showAlert('Completar todos los campos.', 'error');
						}
						break;
					default:
						break;
				}
			},
			login: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Home/loginJson',
					Action: 'loginJson',
					Data: {
						model: {
							UserName: instanceVue.dni,
							Password: instanceVue.password
						}
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
