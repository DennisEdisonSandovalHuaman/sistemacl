﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#perfilVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
		},
		mounted() {
			preloader('hidden');
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'error');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataUser':
						this.form = response.userViewModel;
						break;
					case 'LogOut':
						alert(response.state + " - " + response.urlRedirect);
						if (response.state === true) {
							location.href = response.urlRedirect;
						} else {
							showAlert(response.Message, 'error');
						}
						break;
					default:
						break;
				}
			},
			getDataUser: function () {
				let request = {
					Type: 'GET',
					URL: $('#perfilVue').data('url'),
					Action: "getDataUser",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			LogOut: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Home/LogOut',
					Action: 'LogOut',
					Data: {
						
					}
				};
				this.makeRequestServer(request);
			},
			prueba: function () {
				alert("");
			}
		},
		created() {
			this.getDataUser();
		}
	});
})();
