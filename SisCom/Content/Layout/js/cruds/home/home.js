var productIndex = new Vue({
    el: '#home',
    data: {
        url: $('#baseUrl').val(),
        homeVM: [],
        userList: [],
		openAttendances: [],
        activesProducts: 0,
        inactivesProducts: 0,

        allCustomers: 0,
        activesCustomers: 0,
        inactivesCustomers: 0,

        canceledSales: 0,
        fastSalesMade: 0,
        normalSalesMade: 0,
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: async function () {
            var a = await this.getInfoSales();
            var b = await this.getInfoProducts();
            var c = await this.getInfoCustomers();
            var d = await this.getUsers();
			var e = await this.getOpenAttendance();
            
        },
        getInfoProducts: async function () {
            preloader('show')
            let url = this.url + 'Products/GetActivesAndInactivesProducts';
            axios.get(url).then((response) => {
                this.activesProducts = response.data.activesProducts;
                this.inactivesProducts = response.data.inactivesProducts;
            }).catch((errors) => { }).finally((response) => {
                preloader('hidden');
            });
        },
        getInfoCustomers: async function () {
            preloader('show')
            let url = this.url + 'Customers/GetActivesAndInactivesCustomers';
            axios.get(url).then((response) => {
                this.allCustomers = response.data.allCustomers;
                this.activesCustomers = response.data.activesCustomers;
                this.inactivesCustomers = response.data.inactivesCustomers;
            }).catch((errors) => { }).finally((response) => {
                preloader('hidden');
            });
        },
        getInfoSales: async function () {
            preloader('show')
            let url = this.url + 'Sales/GetActivesAndInactivesSales';
            axios.get(url).then((response) => {
                this.canceledSales = response.data.canceledSales;
                this.fastSalesMade = response.data.fastSalesMade;
                this.normalSalesMade = response.data.normalSalesMade;
            }).catch((errors) => { }).finally((response) => {
                preloader('hidden');
            });
        },
        getUsers: async function () {
            preloader('show')
            let url = this.url + 'User/GetUsersToHomeIndex';
            axios.get(url).then((response) => {
                this.userList = response.data.users;
            }).catch((errors) => { }).finally((response) => {
                preloader('hidden');
            });
        },
		getOpenAttendance: async function () {
			preloader('show')
			let url = this.url + 'Home/GetOpenAttendance';
			axios.get(url).then((response) => {
				this.openAttendances = response.data.openAttendances;
			}).catch((errors) => { }).finally((response) => {
				preloader('hidden');
			});
		},
    }
});