﻿import { Datetime } from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';

(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#serviceTypeVue',
		components: {
			datetime: Datetime
		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			msjError: ""
		},
		mounted() {
			this.getDataServiceType();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataServiceType':
						this.form = response.serviceTypeViewModel;
						break;
					case 'createServiceType':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Tipo de servicio registrado', "success", 'El Tipo de servicio se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al guardar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}	
						break;
					default:
						break;
				}
			},
			getDataServiceType: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'ServiceType/CreateJson',
					Action: "getDataServiceType"
				};
				this.makeRequestServer(request);
			},
			createServiceType: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'ServiceType/CreateServiceTypeJson',
					Action: 'createServiceType',
					Data: {
						serviceTypeViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
