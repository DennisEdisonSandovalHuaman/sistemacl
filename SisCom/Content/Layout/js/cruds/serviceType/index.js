﻿import vuePagination from 'vue-bs-pagination';

(function () {
	'use strict';
	var vueIndex = new Vue({
		el: '#serviceTypeVue',
		components: {
			vuePagination
		},
		data: {
			baseURL: $('#baseUrl').val(),
			list: {
				PageCount: 1
			},
			paginatorList: 1,
			PageSizeList: 10
		},
		watch: {
			paginatorList: function (newVal, oldVal) {
				this.list.Page = this.initTables(newVal, 1);
			}
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: "application/json; charset=utf-8",
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case "GetListServiceType":
						this.list = response.list;
						break;
				}
			},
			initTables: function (page = 1) {
				let request = {
					Type: 'POST',
					URL: this.baseURL + "ServiceType/IndexJson",
					Action: "GetListServiceType",
					Data: {
						paginator: {
							Page: page,
							PageSize: this.PageSizeList
						}
					}
				};
				this.makeRequestServer(request);
			}

		},
		created() {
			this.initTables();
		}
	});
})();
