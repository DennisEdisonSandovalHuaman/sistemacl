﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#branchVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			msjError: ""
		},
		mounted() {
			this.getDataBranch();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataBranch':
						this.form = response.branchViewModel;
						break;
					case 'createBranch':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Sucursal registrada', "success", 'La sucursal se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al guardar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}								
						break;
					default:
						break;
				}
			},
			getDataBranch: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'Branch/CreateJson',
					Action: "getDataBranch"
				};
				this.makeRequestServer(request);
			},
			createBranch: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Branch/CreateBranchJson',
					Action: 'createBranch',
					Data: {
						branchViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
