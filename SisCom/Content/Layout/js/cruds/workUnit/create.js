﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#workUnitVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			msjError: ""
		},
		mounted() {
			this.getDataWorkUnit();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataWorkUnit':
						this.form = response.workUnitViewModel;
						break;
					case 'createWorkUnit':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Unidad de trabajo registrado', "success", 'La unidad de trabajo se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al actualizar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					default:
						break;
				}
			},
			getDataWorkUnit: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'WorkUnit/CreateJson',
					Action: "getDataWorkUnit"
				};
				this.makeRequestServer(request);
			},
			createWorkUnit: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'WorkUnit/CreateWorkUnitJson',
					Action: 'createWorkUnit',
					Data: {
						workUnitViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
