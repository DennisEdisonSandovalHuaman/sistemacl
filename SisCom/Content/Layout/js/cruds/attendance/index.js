﻿﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#attendanceVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			dropdowns: [],
			form: {},
			userName: "",
			idUser: 0
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataAttendance':
						this.dropdowns = response.dropdowns;
						this.form = response.attendanceViewModel;
						this.userName = response.userName;
						break;
					case 'openAttendance':
						if (response.state) {
							showAlert(response.Message, 'Notificación de Sistema!');
							location.href = response.urlRedirect;
						} else {
							showAlert(response.Message, 'Notificación de Sistema!');
						}
						
						break;
					default:
						break;
				}
			},
			getDataAttendance: function () {
				let request = {
					Type: 'GET',
					URL: $('#attendanceVue').data('url'),
					Action: "getDataAttendance",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			openAttendance: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Attendance/OpenAttendanceJson',
					Action: 'openAttendance',
					Data: {
						attendanceViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.getDataAttendance();
		}
	});
})();
