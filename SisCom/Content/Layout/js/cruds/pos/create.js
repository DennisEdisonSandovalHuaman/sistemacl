﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#posVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			dropdowns: [],
			msjError: ""
		},
		mounted() {
			this.getDataPos();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataPos':
						this.form = response.posViewModel;
						this.dropdowns = response.dropdowns;
						break;
					case 'createPos':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Punto de servicio registrado', "success", 'El punto de servicio se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al actualizar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					default:
						break;
				}
			},
			getDataPos: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'Pos/CreateJson',
					Action: "getDataPos"
				};
				this.makeRequestServer(request);
			},
			createPos: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Pos/CreatePosJson',
					Action: 'createPos',
					Data: {
						posViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
