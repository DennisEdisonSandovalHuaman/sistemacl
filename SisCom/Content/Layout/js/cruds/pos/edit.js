﻿﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#posVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			dropdowns: [],
			msjError: ""
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataEdit':
						this.form = response.posViewModel;
						this.dropdowns = response.dropdowns;
						break;
					case 'editPos':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Punto de servicio actualizado', "success", 'El punto de servicio se ha actualizado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al actualizar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					case 'deletePos':
						showAlert(response.Message, 'Notificación de Sistema!');
						location.href = response.urlRedirect;
						break;
					default:
						break;
				}
			},
			getDataEdit: function () {
				let request = {
					Type: 'GET',
					URL: $('#posVue').data('url'),
					Action: "getDataEdit",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			editPos: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Pos/EditPosJson',
					Action: 'editPos',
					Data: {
						posViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			},
			deletePos: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Pos/DeletePosJson',
					Action: 'deletePos',
					Data: {
						posViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.getDataEdit();
		}
	});
})();
