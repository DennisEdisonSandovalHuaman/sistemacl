var employedCreate = new Vue({
    el: '#employedCreate',
    data: {
        url: $('#baseUrl').val() + "employed/",
        employedVM: {},
        errors: {},
        validate: false
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            let url = this.url + "createjson";
            window.axios.get(url).then((response) => {
                this.employedVM = response.data;
            }).catch((errors) => {
            }).finally((response) => {
            });
        },
        createEmployed: function ($opcion) {
            let url = this.url + "store";
            let employedVM = this.employedVM;
            preloader('show');
            window.axios.post(url, employedVM).then((response) => {
                if ($opcion === 1) {
                    window.location.href = this.url;
                } else {
                    this.cleanForm();
                    alert('Elemento creado')
                }
            }).catch((error) => {
                this.errors = error.response.data.errors;
            }).finally((response) => {
                preloader('hidden');
            });
        },
        cleanForm: function () {
            this.employedVM = {};
        },
    },
});
