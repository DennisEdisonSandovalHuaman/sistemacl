var employedIndex = new Vue({
    el: '#employedIndex',
    data:{
        url: $('#baseUrl').val() + 'employed/',
        employedVM: [],
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function(){
            let url = this.url + 'indexjson';
            preloader('show');
            window.axios.get(url).then((response)=>{
                this.employedVM = response.data;
            }).catch((errors)=>{
            }).finally((response)=>{
                preloader('hidden');
            })
        }
    },
});
