﻿(function () {
	'use strict';
	var vueIndex = new Vue({
		el: '#organizationsVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			list: {}
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: "application/json; charset=utf-8",
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case "GetListOrganizations":
						this.list = response;
						break;
				}
			},
			initTables: function () {
				let request = {
					Type: 'POST',
					URL: this.baseURL + "Organizations/IndexJson",
					Action: "GetListOrganizations"
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.initTables();
		}
	});
})();
