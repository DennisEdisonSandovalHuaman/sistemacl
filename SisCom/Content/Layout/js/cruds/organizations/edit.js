﻿﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#organizationsVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {}
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataEdit':
						this.form = response.organizationViewModel;
						break;
					case 'editOrganization':
						showAlert(response.Message, 'Notificación de Sistema!');
						location.href = response.urlRedirect;
						break;
					default:
						break;
				}
			},
			getDataEdit: function () {
				let request = {
					Type: 'GET',
					URL: $('#organizationsVue').data('url'),
					Action: "getDataEdit",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			editOrganization: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Organizations/EditOrganizationJson',
					Action: 'editOrganization',
					Data: {
						organizationViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.getDataEdit();
		}
	});
})();
