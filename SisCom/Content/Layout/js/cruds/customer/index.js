import vuePagination from 'vue-bs-pagination';

(function () {
	'use strict';
	var vueIndex = new Vue({
		el: '#customerVue',
		components: {
			vuePagination
		},
		data: {
			baseURL: $('#baseUrl').val(),
			list: {
				WorkUnit: [],
				Company: [],
				PageCount: 1
			},
			paginatorList: 1,
			PageSizeList: 10,
			FilterByName : '',
			FilterByFatherLastName : '',
			FilterByMotherLastName : '',
			FilterByDNI : '',
			FilterByCompany : ''
		},
		watch: {
			paginatorList: function (newVal, oldVal) {
				this.list.Page = this.initTables(newVal, 1);
			}
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: "application/json; charset=utf-8",
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurri� un error comun�quese con el administrador del sistema.', 'Error del sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case "GetListCustomers":
						this.list = response.list;
						FilterByName: '';
						FilterByDNI: '';
						FilterByCompany: '';
						break;
				}
			},
			initTables: function (page = 1) {
				let request = {
					Type: 'POST',
					URL: this.baseURL + "Customers/IndexJson",
					Action: "GetListCustomers",
					Data: {
						filters: {
							Page: page,
							PageSize: this.PageSizeList,
							FilterByName: this.FilterByName,
							FilterByDNI: this.FilterByDNI,
							FilterByCompany: this.FilterByCompany
						}
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.initTables();
		}
	});
})();
