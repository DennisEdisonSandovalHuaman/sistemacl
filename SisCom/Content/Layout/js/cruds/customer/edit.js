﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#customerVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: { WorkUnit: [] },
			dropdowns: [],
			msjError: ""
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataEdit':
						this.form = response.customersViewModel;
						this.dropdowns = response.dropdowns;
						break;
					case 'editCustomer':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Cliente actualizado', "success", 'El cliente se ha actualizado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al guardar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					case 'deleteCustomer':
						showAlert(response.Message, 'Notificación de Sistema!');
						location.href = response.urlRedirect;
						break;
					default:
						break;
				}
			},
			getDataEdit: function () {
				let request = {
					Type: 'GET',
					URL: $('#customerVue').data('url'),
					Action: "getDataEdit",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			editCustomer: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Customers/EditCustomerJson',
					Action: 'editCustomer',
					Data: {
						customersViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			},
			deleteCustomer: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Customers/DeleteCustomerJson',
					Action: 'deleteCustomer',
					Data: {
						customersViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.getDataEdit();
		}
	});
})();
