(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#customerVue',
		components: {
			
		},
		data: {
			baseURL: $('#baseUrl').val(),
			dropdowns: [],
			form: {},
			msjError: ""
		},
		mounted() {
			this.getDataCustomer();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');			
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurri� un error comun�quese con el admnistrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataCustomer':
						this.dropdowns = response.dropdowns;
						this.form = response.customersViewModel;
						break;
					case 'createCustomer':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Cliente registrado', "success", 'El cliente se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al guardar, recargar la p�gina o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}	
					default:
						break;
				}
			},
			getDataCustomer: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'Customers/CreateJson',
					Action: "getDataCustomer"
				};
				this.makeRequestServer(request);
			},
			createCustomer: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Customers/CreateCustomerJson',
					Action: 'createCustomer',
					Data: {
						customersViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
