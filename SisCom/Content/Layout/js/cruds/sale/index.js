import vuePagination from 'vue-bs-pagination';

var saleIndex = new Vue({
    el: '#saleIndex',
    components: {
        vuePagination
    },
    data: {
        url: $('#baseUrl').val() + 'sales/',
        typeDocumentPDF: '',
        saleVM: {},
        list: {
            PageCount: 1
        },
        filterText: "",
        pagesNumber: 0,
        paginate: {},
        descriptionToAnulate: "",
        paginatorList: 1,
        PageSizeList: 10
    },
    watch: {
        paginatorList: function (newVal, oldVal) {
            this.list.Page = this.initForm(newVal, 1);
        }
    },
    computed: {
        isActived: function () {
            return this.paginate.current_page;
        }
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function (page = 1) {
            let url = this.url + 'IndexJson';
            preloader('show');
            var data = {
                paginator: {
                    Page: page,
                    PageSize: this.PageSizeList
                }
            };
            window.axios.post(url, data).then((response) => {
                if (response.data.state) {
                    this.list = response.data.list;
                } else {
                    showAlert(response.data.titleError, 'error', response.data.message);
                    this.list = response.data.list;
                }
                //this.paginate = response.data.paginate;
            }).catch((errors) => {
            }).finally((response) => {
                //this.pagesNumber = pagesNumber(this.paginate, 3);
                preloader('hidden');
            });
        },
        edit: function (idSale) {
            window.location.href = this.url + "edit/" + idSale;
        },
        detail: function (idSale) {
            window.location.href = this.url + "detail/" + idSale;
        },
        getStateOfSale: function (idSale) {
            let url = this.url + "getStateOfSale/" + idSale;
            preloader('show');
            window.axios.get(url).then((response) => {
                this.saleVM = response.data;
                this.typeDocumentPDF = this.saleVM.typeDoc;
            }).catch((errors) => {
            }).finally((response) => {
                preloader('hidden');
            });
        },
        showAlertBeforeRefuse: function (idSale) {
            showAlertWithOptions("Anular venta !!!", "warning", "La venta se anulará, el dinero saldrá de caja ... ", "Anular", "Atras").then((response) => {
                if (response)
                    this.refuseSaleByID(idSale);
            });
        },
        refuseSaleByID: function (idSale) {
            preloader('show');
            let url = this.url + "delete?id=" + idSale;
            axios.get(url).then((response) => {
                if (response.data.state) {
                    showAlert("La venta ha sido rechazada !!!", "success", response.data.message);
                }
                else {
                    showAlert("Error en la solicitud !!!", "error", "Ocurrio un error mientras se rechazaba la venta ...");
                }
            }).catch((errors) => {
                showAlert("Error en la solicitud !!!", "error", "Ocurrio un error mientras se rechazaba la venta ...");
            }).finally((response) => {
                preloader('hidden');
                this.initForm(1);
            });
        },

        changePage: function (page) {
            this.paginate.current_page = page;
            this.initForm(page);
        }
    }
});
