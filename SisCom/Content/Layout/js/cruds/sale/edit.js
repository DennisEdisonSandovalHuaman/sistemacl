var saleEdit = new Vue({
    el: '#saleEdit',
    data: {
        url: $('#baseUrl').val() + "sale/",
        idSale: $('#idSale').val(),
        saleVM: {},
        customerVM: {},
        discount: 0,
        cashDiscount: 0.0,
        filteredCustomers: [],
        currentSale: [],
        columnNames: [],
        columnNamesSale: [],
        dataRow: [],
        customerFilterText: "",
        tableClass: "table",
        typeDoc: "boleta",
        validations: {},
        errors: false,
        customer: "",
        customerTypeDoc: 'dni',
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            this.columnNames = ['Código', 'Descripción', 'Cantidad', 'Peso', 'Precio', 'Desct. %', 'Subtotal', ''];
            this.columnNamesSale = ['Código', 'Descripción', 'Cantidad', 'Peso', 'Precio', 'Desct.%', 'Subtotal', ''];
            this.getAllData();
        },
        //Filtra los clientes de acuerdo al texto del Input y los carga al atributo select.
        filterCustomer: function (customerFilterText) {
            let url = this.url + "filterCustomers";
            let data = {
                'customerFilterText': customerFilterText,
            };
            window.axios.get(url, {params: data}).then((response) => {
                this.filteredCustomers = response.data;
            }).catch((errors) => {
            }).finally((response) => {
            });
        },
        updateRow: function (event) {
            let posicion = event.target.attributes.po.value;
            let positionOfProduct = event.target.id;
            let boton = this.dataRow[positionOfProduct][8];

            //Evento input descuento
            if (posicion === '5') {
                let descuentoInput = event.target.value;
                this.dataRow[event.target.id][5].data = descuentoInput;
            }
            //Evento input precio
            else if (posicion === '4') {
                let precioInput = event.target.value;
                this.dataRow[event.target.id][4].data = precioInput;
            }
            //Evento input peso
            else if (posicion === '3') {
                let pesoInput = event.target.value;
                this.dataRow[event.target.id][3].data = pesoInput;
                let cantidadInput = parseFloat(event.target.value);

                let typeStockProduct = this.dataRow[event.target.id][9].data;
                if (typeStockProduct === 'Kilogramo') {
                    let pesoProduct = parseFloat(this.dataRow[event.target.id][10].data);
                    if (cantidadInput > pesoProduct) {
                        showToast('Peso insuficiente, máximo permitido: ' + pesoProduct, 'error', 'El peso es insuficiente !!!');
                        boton.class = "d-none";
                    } else {
                        boton.class = "btn btn-small bt n-primary";
                    }
                }
            }
            //Evento input cantidad
            else if (posicion === '2') {
                let cantidadInput = parseFloat(event.target.value);
                this.dataRow[event.target.id][2].data = cantidadInput;

                let typeStockProduct = this.dataRow[event.target.id][9].data;
                if (typeStockProduct === 'Unidad') {
                    let stockProduct = parseFloat(this.dataRow[event.target.id][7].data);
                    if (cantidadInput > stockProduct) {
                        showToast('Stock insuficiente, máximo permitido: ' + stockProduct, 'error', 'El stock es insuficiente !!!');
                        boton.class = "d-none";
                    } else {
                        boton.class = "btn btn-small btn-primary";
                    }
                }
            }
            this.calculateSubTotalForProductWithTypeOfUnity(positionOfProduct);
        },
        calculateSubTotalForProductWithTypeOfUnity: function (positionOfProduct) {
            let precioProducto = this.dataRow[positionOfProduct][4].data;
            let cantidadInput = this.dataRow[positionOfProduct][2].data;
            let discountInput = this.dataRow[positionOfProduct][5].data;
            let pesoProduct = '1';
            let typeStockProduct = this.dataRow[positionOfProduct][9].data;
            if (typeStockProduct === 'Kilogramo') {
                pesoProduct = this.dataRow[positionOfProduct][3].data;
            }
            let subTotal = Math.round((parseFloat(precioProducto) * parseFloat(cantidadInput) * parseFloat(pesoProduct)) * 100) / 100;
            this.dataRow[positionOfProduct][6].data = Math.round((subTotal * (1 - parseFloat(discountInput) / 100)) * 100) / 100;
        },
        //Agrega el producto a la venta actual:
        addToSale: function (event) {
            if (event.target.localName === "button") {
                let id = event.target.id;
                let idProduct = event.target.attributes.po.value;
                if (this.dataRow[id][6].data <= 0) {
                    return;
                } else if(isNaN(this.dataRow[id][6].data)){
                    return;
                }
                let data = [
                    // Codigo 0
                    {
                        'type': 'text',
                        'data': this.dataRow[id][0].data,
                    },
                    // Descripcion 1
                    {
                        'type': 'text',
                        'data': this.dataRow[id][1].data,
                    },
                    // Cantidad 2
                    {
                        'type': 'text',
                        'data': this.dataRow[id][2].data,
                    },
                    // Peso 3
                    {
                        'type': 'text',
                        'data': this.dataRow[id][3].data,
                    },
                    // Precio 4
                    {
                        'type': 'text',
                        'data': this.dataRow[id][4].data,
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': event.target.attributes.po.value,
                    },
                    // Descuento 6
                    {
                        'type': 'text',
                        'data': this.dataRow[id][5].data,
                    },
                    // Subtotal 7
                    {
                        'type': 'text',
                        'data': this.dataRow[id][6].data,
                    },
                    // Boton 8
                    {
                        'type': 'button',
                        'data': "<i class='material-icons'>delete</i>",
                        'id': id,
                        'po': event.target.attributes.po.value,
                        'class': "btn btn-small red d-block"
                    },
                    // Stock 9
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][7].data,
                    },
                    // typeUnity 10
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][9].data,
                    }
                ];

                let found = this.currentSale.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.saleVM.subTotal -= this.currentSale[found][7].data;
                    this.currentSale.splice(found, 1, data);
                } else {
                    this.currentSale.push(data);
                }
                this.calculateTotalDiscount(this.currentSale);
                this.calculateTotalPrice(this.currentSale);
                this.calculateIGV(this.saleVM.totalPrice);
                this.calculateSubTotal(this.saleVM.totalPrice, this.saleVM.IGV);
            }
        },
        //Calcula el descuento monetario total de la venta actual
        calculateTotalDiscount: function (currentSale) {
            let discount = 0;
            currentSale.forEach(function (item) {
                let weightOfProduct = 1;
                if (item[10]['data'] === 'Kilogramo') {
                    weightOfProduct = item[3]['data'];
                }
                discount += item[4]['data'] * item[2]['data'] * weightOfProduct - item[7]['data'];
            });
            this.saleVM.totalDiscount = Math.round(discount * 100) / 100;
        },
        calculateTotalPrice: function (currentSale) {
            let totalPrice = 0;
            currentSale.forEach(function (item) {
                totalPrice += (parseFloat(item[7]['data']));
            });
            this.saleVM.totalPrice = Math.round(totalPrice * 100) / 100;
        },
        calculateIGV: function (totalPrice) {
            let base = ((totalPrice / (1.18)) * 100) / 100;
            this.saleVM.IGV = Math.round((totalPrice - base) * 100) / 100;
        },
        calculateSubTotal: function (totalPrice, IGV) {
            this.saleVM.subTotal = Math.round((totalPrice - IGV) * 100) / 100;
        },
        rmvFromSale: function (event) {
            let idProduct = 0;
            if (event.target.localName === "button") {
                idProduct = event.target.attributes.po.value;
            } else if (event.target.localName === "svg") {
                idProduct = event.target.parentElement.attributes.po.value;
            }
            if (idProduct !== 0) {
                let found = this.currentSale.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.currentSale.splice(found, 1);
                    this.calculateTotalDiscount(this.currentSale);
                    this.calculateTotalPrice(this.currentSale);
                    this.calculateIGV(this.saleVM.totalPrice);
                    this.calculateSubTotal(this.saleVM.totalPrice, this.saleVM.IGV);
                }
            }
        },
        //Obtiene toda la data necesaria para iniciar una venta
        getAllData: function () {
            let url = this.url + "editjson";
            var data = {
                'idSale': this.idSale,
            };
            preloader('show');
            window.axios.get(url, {params: data}).then((response) => {
                this.products = response.data.products;
                this.saleVM = response.data.saleVM;
                this.customerVM = response.data.customerVM;
                if (this.customerVM.dni === null) {
                    this.customerTypeDoc = 'ruc';
                } else {
                    this.customerTypeDoc = 'dni';
                }
                this.fillDataRow(this.products);
                this.fillCurrentSale(response.data.detailsAndProducts);
                this.getClientsOfInputAutocomplete();
            }).catch((error) => {
            }).finally((response) => {
                preloader('hidden');
            })
        },
        getClientsOfInputAutocomplete: function () {
            let url = this.url + 'GetCustomersForInputAutocomplete';
            window.axios.get((url)).then((response) => {
                this.fillInputAutocompleteCustomers(response.data);
            }).catch((error) => {
            }).finally((response) => {
            });
        },
        //Llena el input autoComplete con la data de clientes:
        fillInputAutocompleteCustomers: function (data) {
            var vue = this;
            $(function () {
                $('input.autocompleteCustomers').autocomplete({
                    data: data,
                    onAutocomplete: function (customer) {
                        let numberOfDocument = customer.split(' ').reverse()[0];
                        vue.invocateCustomer(numberOfDocument);
                    },
                    limit: 5, // The max amount of results that can be shown at once. Default: Infinity.
                });
            });
        },
        //Agrega el cliente seleccionado del atributo html select al formulario customerVM
        invocateCustomer: function (nroDocument) {
            let url = this.url + "findCustomer/" + nroDocument;
            window.axios.get(url).then((response) => {
                this.customerFilterText = "";
                this.customerVM = response.data;
                // Si el cliente seleccionado no tiene dni, se maneja por RUC y viceversa.
                if (this.customerVM.dni === null) {
                    this.customerTypeDoc = 'ruc';
                } else {
                    this.customerTypeDoc = 'dni';
                    this.saleVM.typeDoc = 'boleta';
                }
                this.customer = '';
            });
        },
        //Carga los productos de la venta a editar en la tabla de CurrentSale:
        fillCurrentSale: function (detailsVM) {
            let currentSale = [];
            detailsVM.forEach(function (detail, index) {
                let data = [
                    // Codigo 0
                    {
                        'type': 'text',
                        'data': detail.idProduct.toString(),
                    },
                    // Descripcion 1
                    {
                        'type': 'text',
                        'data': detail.productDescription,
                    },
                    // Cantidad 2
                    {
                        'type': 'text',
                        'data': detail.quantity,
                    },
                    // Peso 3
                    {
                        'type': 'text',
                        'data': detail.detailWeightProduct,
                    },
                    // Precio 4
                    {
                        'type': 'text',
                        'data': detail.productPrice,
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': detail.idProduct.toString(),
                    },
                    // Descuento 6
                    {
                        'type': 'text',
                        'data': detail.unitDiscount,
                    },
                    // Subtotal 7
                    {
                        'type': 'text',
                        'data': detail.detailSubtotalPrice,
                    },
                    // Boton 8
                    {
                        'type': 'button',
                        'data': "<i class='material-icons'>delete</i>",
                        'id': index,
                        'po': detail.idProduct,
                        'class': "btn btn-small red d-block"
                    },
                    // Stock 9
                    {
                        'type': 'hidden',
                        'data': detail.productStock,
                    },
                    // typeUnity 10
                    {
                        'type': 'hidden',
                        'data': detail.typeUnity,
                    },
                ];
                currentSale.push(data);
            });
            this.currentSale = currentSale;
        },
        //Carga toda la data de productos al componente tabla:
        fillDataRow: function (response) {
            let dr = [];
            for (let i = 0; i < response.length; i++) {
                let data = [
                    // Field 0
                    {
                        'type': 'text',
                        'data': response[i].idProduct.toString(),
                    },
                    // Field 1
                    {
                        'type': 'text',
                        'data': response[i].description
                    },
                    // Field 2
                    {
                        'type': 'input',
                        'data': '1',
                        'id': i,
                    },
                    // Field 3
                    {
                        'type': 'input',
                        'data': 1,
                        'id': i
                    },
                    // Field 4
                    {
                        'type': 'input',
                        'data': response[i].price,
                        'id': i
                    },
                    // Field 5
                    {
                        'type': 'input',
                        'data': '0',
                        'id': i
                    },
                    // Field 6
                    {
                        'type': 'text',
                        'data': response[i].price
                    },
                    // Field 7
                    {
                        'type': 'hidden',
                        'data': response[i].stock,
                    },
                    // Field 8
                    {
                        'type': 'button',
                        'data': "+",
                        'id': i,
                        'po': response[i].idProduct,
                        'class': "btn btn-small btn-primary d-block"
                    },
                    // Field 9
                    {
                        'type': 'hidden',
                        'data': response[i].typeUnity,
                    },
                    // Field 10
                    {
                        'type': 'hidden',
                        'data': response[i].weight,
                    }
                ];
                let typeStock = data[9].data;
                if (typeStock === 'Unidad') {
                    data[3].type = 'text';
                    data[3].data = response[i].weight;
                }
                dr.push(data);
            }
            this.dataRow = dr;
        },
        checkDoc: function () {
            let url = this.url + "getcustomer";
            //RUC
            if (this.customerTypeDoc === 'ruc') {
                preloader('show');
                if (this.customerVM.ruc.length === 11) {
                    window.axios.post(url, {ruc: this.customerVM.ruc}).then((response) => {
                        if (response.data) {
                            this.customerVM = response.data;
                        } else {
                            showAlert('Cliente no encontrado, intente de nuevo !', 'error');
                            this.customerVM = {};
                        }
                    }).finally((response) => {
                        preloader('hidden');
                    });
                }
            }
            //DNI
            else {
                if (this.customerVM.dni.length === 8) {
                    preloader('show');
                    window.axios.post(url, {dni: this.customerVM.dni}).then((response) => {
                        if (response.data) {
                            this.customerVM = response.data;
                        } else {
                            showAlert('Cliente no encontrado, intente de nuevo !', 'error');
                            this.customerVM = {};
                        }
                    }).finally((response) => {
                        preloader('hidden');
                    });
                }
            }
        },
        cleanForm: function () {
            this.customerVM.dni = null;
            this.customerVM.ruc = null;
            this.customerVM.name = null;
            this.customerVM.nameCorp = null;
            this.customerVM.address = null;
            this.customerVM.phone = null;
            this.customerVM.email = null;
            this.customerVM.route = null;
            this.saleVM.typeDoc = this.typeDoc;
            this.validations = {};
            this.customer = "";
            if(this.customerTypeDoc === 'dni'){
                this.saleVM.typeDoc = 'boleta'
            }
        },
        updateSale: function () {

            document.getElementById('inputhidden').focus();

            let url = this.url + "update";
            if ((this.customerVM.dni !== null && this.customerVM.dni.trim() !== '') || (this.customerVM.ruc !== null && this.customerVM.ruc.trim() !== '')) {
                let data = {
                    'saleVM': this.saleVM,
                    'customerVM': this.customerVM,
                    'items': this.currentSale
                };
                preloader('show');
                window.axios.put(url, data).then((response) => {
                    if (response.data) {
                        showAlert(this.saleVM.type + ' actualizada !!!', 'success');
                        this.resetForm();
                        this.getAllData();
                    } else {
                        showToast('Ocurrió un error, intente de nuevo !!!', 'error');
                    }
                }).catch((error) => {
                    this.validations = error.response.data.errors;
                    this.validations['dni'] = error.response.data.errors['customerVM.dni'];
                    this.validations['ruc'] = error.response.data.errors['customerVM.ruc'];
                    this.validations['name'] = error.response.data.errors['customerVM.name'];
                    this.validations['nameCorp'] = error.response.data.errors['customerVM.nameCorp'];
                    this.errors = true;
                }).finally((response) => {
                    preloader('hidden');
                });
            } else {
                showAlert('Necesita ingresar el DNI/RUC !!! ', 'error');
            }
        },
        resetForm: function () {
            this.saleVM = {};
            this.customerVM = {};
            this.currentSale = [];
        },
    }

});
