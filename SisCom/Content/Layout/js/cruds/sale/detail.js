var saleDetail = new Vue({
    el: '#saleDetail',
    data: {
        url: $('#baseUrl').val() + 'sales/',
        idSale: $('#idSale').val(),
        saleVM: {},
        details: [],
    },
    mounted: function () {
        this.initForm();
    },
    methods: {
        initForm: function () {
            preloader('show');
            let url = this.url + "detailJson";
            var data = {
                'idSale': this.idSale
            };
            axios.get(url, { params: data }).then((response) => {
            }).catch((errors) => {
            }).finally((response) => {
                preloader('hidden');
            });
        },
        showAlertBeforeRefuse: function () {
            showAlertWithOptions("Eliminar venta !!!", "warning", "La venta se rechaza, el stock regresa y el dinero sale de caja ... ", "Eliminar", "Atras").then((response) => {
                if (response)
                    this.refuseSaleByID(this.idSale);
            });
        },
        refuseSaleByID: function (idSale) {
            preloader('show');
            let url = this.url + "delete?id=" + idSale;
            axios.get(url).then((response) => {
                if (response.data.state) {
                    showAlert("La venta ha sido rechazada !!!", "success", response.data.message);
                    $('#refuse').prop('disabled', true);
                }
                else {
                    showAlert("Error en la solicitud !!!", "error", "Ocurrio un error mientras se rechazaba la venta ...");
                }
            }).catch((errors) => {
                showAlert("Error en la solicitud !!!", "error", "Ocurrio un error mientras se rechazaba la venta ...");
            }).finally((response) => {
                preloader('hidden');
            });
        },
    }
});
