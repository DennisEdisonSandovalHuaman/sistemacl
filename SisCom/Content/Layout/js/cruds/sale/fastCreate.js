﻿import VueBootstrapTypeahead from 'vue-bootstrap-typeahead';
import Datepicker from 'vuejs-datepicker';
import { en, es } from 'vuejs-datepicker/dist/locale';
var saleCreate = new Vue({
    el: '#saleFastCreate',
    components: {
        VueBootstrapTypeahead,
        Datepicker
    },
    data: {
        url: $('#baseUrl').val() + "sales/",
        saleVM: {},
        customerVM: {},
        serviceTypeVM: {},
        discount: 0,
        cashDiscount: 0.0,
        currentSale: [],
        columnNames: [],
        columnNamesSale: [],
        dataRow: [],
        tableClass: "table",
        typeDoc: "boleta",
        validations: {},
        serviceTypes: [],
        serviceTypeID: 0,
        errors: false,
        customer: "",
        totalToPay: 0,
        customerName: "",
        customerNameFast: "",
        customerNameClassic: "",
        customerTypeDoc: 'dni',
        showClassic: false,
        showName: "clásica",
        WorkUnitName: "",
        customersToAutoComplete: [],

        startDateInput: "",
        endDateInput: "",
        startDate: null,
        endDateObject: null,
        startDateObject: null,
        endDate: null,
        en: en,
        es: es,

    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            this.currentSale = [];
            this.columnNames = ['Código', 'Descripción', 'Cantidad', 'Precio Unitario', 'Subtotal', ''];
            this.columnNamesSale = ['Código', 'Descripción', 'Cantidad', 'Precio Unitario', 'Subtotal', ''];
            this.getAllData();
        },
        changeStartDate: function (e) {
            this.startDateObject = new Date(e.getYear() + 1900, e.getMonth() + 1, e.getDate());
            this.startDate = (e.getYear() + 1900) + "-" + (e.getMonth() + 1) + "-" + e.getDate();
            this.startDateInput = e.getDate() + "/" + (e.getMonth() + 1) + "/" + (e.getYear() + 1900);
            this.day = e.getDate();
            this.month = (e.getMonth() + 1);
            this.year = (e.getYear() + 1900);
        },
        updateRow: function (event) {
            let posicion = event.target.attributes.po.value;
            let positionOfProduct = event.target.id;
            let boton = this.dataRow[positionOfProduct][8];

            //Evento input descuento
            if (posicion === '5') {
                let descuentoInput = event.target.value;
                this.dataRow[event.target.id][5].data = descuentoInput;
            }
            //Evento input precio
            else if (posicion === '4') {
                let precioInput = event.target.value;
                this.dataRow[event.target.id][4].data = precioInput;
            }
            //Evento input peso
            else if (posicion === '3') {
                let pesoInput = event.target.value;
                this.dataRow[event.target.id][3].data = pesoInput;
                let cantidadInput = parseFloat(event.target.value);

                let typeStockProduct = this.dataRow[event.target.id][9].data;
                if (typeStockProduct === 'Kilogramo') {
                    let pesoProduct = parseFloat(this.dataRow[event.target.id][10].data);
                    if (cantidadInput > pesoProduct) {
                        showToast('Peso insuficiente, máximo permitido: ' + pesoProduct, 'error', 'El peso es insuficiente !!!');
                        boton.class = "d-none";
                    } else {
                        boton.class = "btn btn-small bt n-primary";
                    }
                }
            }
            //Evento input cantidad
            else if (posicion === '2') {
                let cantidadInput = parseFloat(event.target.value);
                this.dataRow[event.target.id][2].data = cantidadInput;

                let stockProduct = parseFloat(this.dataRow[event.target.id][9].data);
                if (cantidadInput > stockProduct) {
                    showToast('Stock insuficiente, máximo permitido: ' + stockProduct, 'error', 'El stock es insuficiente !!!');
                    boton.class = "d-none";
                } else {
                    boton.class = "btn btn-small btn-primary";
                }
            }
            this.calculateSubTotalForProductWithTypeOfUnity(positionOfProduct);
        },
        changeAmount: function () {
            this.saleVM.change = Math.round((parseFloat(this.saleVM.cash) - parseFloat(this.totalToPay)) * 100) / 100;
            this.saleVM.accountPayment = parseFloat(this.saleVM.cash) - parseFloat(this.saleVM.change);
        },
        calculateSubTotalForProductWithTypeOfUnity: function (positionOfProduct) {
            let precioProducto = this.dataRow[positionOfProduct][4].data;
            let cantidadInput = this.dataRow[positionOfProduct][2].data;
            let discountInput = this.dataRow[positionOfProduct][5].data;
            let pesoProduct = '1';
            let typeStockProduct = this.dataRow[positionOfProduct][9].data;
            if (typeStockProduct === 'Kilogramo') {
                pesoProduct = this.dataRow[positionOfProduct][3].data;
            }
            let subTotal = Math.round((parseFloat(precioProducto) * parseFloat(cantidadInput) * parseFloat(pesoProduct)) * 100) / 100;
            this.dataRow[positionOfProduct][6].data = Math.round((subTotal * (1 - parseFloat(discountInput) / 100)) * 100) / 100;
        },
        addToSale: function (event) {
            if (event.target.localName === "button") {
                let id = event.target.id;
                let idProduct = event.target.attributes.po.value;
                if (this.dataRow[id][6].data <= 0) {
                    return;
                } else if (isNaN(this.dataRow[id][6].data)) {
                    return;
                }
                let data = [
                    // Codigo 0
                    {
                        'type': 'text',
                        'data': this.dataRow[id][0].data,
                    },
                    // Descripcion 1
                    {
                        'type': 'text',
                        'data': this.dataRow[id][1].data,
                    },
                    // Cantidad 2
                    {
                        'type': 'text',
                        'data': this.dataRow[id][2].data,
                    },
                    // Peso 3
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][3].data,
                    },
                    // Precio 4
                    {
                        'type': 'text',
                        'data': this.dataRow[id][4].data,
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': event.target.attributes.po.value,
                    },
                    // precio concedido 6
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][7].data,
                    },
                    // Subtotal 7
                    {
                        'type': 'text',
                        'data': this.dataRow[id][6].data,
                    },
                    // Boton 8
                    {
                        'type': 'button',
                        'data': "-",
                        'id': id,
                        'po': event.target.attributes.po.value,
                        'class': "btn btn-small btn-danger red d-block"
                    },
                    // Stock 9
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][9].data,
                    },
                    // typeUnity 10
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][10].data,
                    }
                ];
                let found = this.currentSale.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.saleVM.subTotal -= this.currentSale[found][7].data;
                    this.currentSale.splice(found, 1, data);
                } else {
                    this.currentSale.push(data);
                }
                //this.calculateTotalDiscount(this.currentSale);
                this.calculateTotalPrice(this.currentSale);
                //this.calculateIGV(this.saleVM.totalPrice);
                //this.calculateSubTotal(this.saleVM.totalPrice, this.saleVM.IGV);
                this.changeAmount();
            }
        },
        calculateTotalDiscount: function (currentSale) {
            let discount = 0;
            currentSale.forEach(function (item) {
                let weightOfProduct = 1;
                if (item[10]['data'] === 'Kilogramo') {
                    weightOfProduct = item[3]['data'];
                }
                discount += item[4]['data'] * item[2]['data'] * weightOfProduct - item[7]['data'];
            });
            this.saleVM.totalDiscount = Math.round(discount * 100) / 100;
        },
        calculateTotalPrice: function (currentSale) {
            let totalPrice = 0;
            let totalGranted = 0;
            currentSale.forEach(function (item) {
                totalPrice += (parseFloat(item[7]['data']));
                totalGranted += item[2]['data'] * item[6]['data'];
            });
            this.saleVM.totalGranted = totalGranted;
            this.saleVM.totalPrice = Math.round(totalPrice * 100) / 100;
            this.saleVM.cash = this.saleVM.totalPrice - this.saleVM.totalGranted;
            this.totalToPay = this.saleVM.totalPrice - this.saleVM.totalGranted;
        },
        calculateIGV: function (totalPrice) {
            let base = ((totalPrice / (1.18)) * 100) / 100;
            this.saleVM.IGV = Math.round((totalPrice - base) * 100) / 100;
        },
        calculateSubTotal: function (totalPrice, IGV) {
            this.saleVM.subTotal = Math.round((totalPrice - IGV) * 100) / 100;
        },
        rmvFromSale: function (event) {
            let idProduct = 0;
            if (event.target.localName === "button") {
                idProduct = event.target.attributes.po.value;
            } else if (event.target.localName === "svg") {
                idProduct = event.target.parentElement.attributes.po.value;
            }
            if (idProduct !== 0) {
                let found = this.currentSale.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.currentSale.splice(found, 1);
                    this.calculateTotalDiscount(this.currentSale);
                    this.calculateTotalPrice(this.currentSale);
                    this.calculateIGV(this.saleVM.totalPrice);
                    this.calculateSubTotal(this.saleVM.totalPrice, this.saleVM.IGV);
                }
            }
        },
        getAllData: function () {
            let url = this.url + "FastCreateJson";
            preloader('show');
            window.axios.post(url).then((response) => {
                if (response.data.resultViewModel.State) {
                    this.customerVM = response.data.customerVM;
                    this.customerVM.isActive = "initialState";
                    this.saleVM = response.data.saleVM;
                    this.products = response.data.products;
                    this.serviceTypes = response.data.serviceTypes;
                    this.serviceTypeID = this.serviceTypes[1].idServiceType;
                    this.customersToAutoComplete = response.data.customersToTypeHead;
                    this.fillDataRow(this.products);
                } else {
                    showAlert(response.data.resultViewModel.MessageTitle, response.data.resultViewModel.MessageType, response.data.resultViewModel.Message);
                    this.customerVM = response.data.customerVM;
                    this.saleVM = response.data.saleVM;
                }
            }).catch((error) => {
            }).finally((response) => {
                preloader('hidden');
            })
        },
        SaveUntimelySale: function () {

            if (this.customerName === "")
                return showAlert("¡¡ Necesita ingresar un cliente !!", "error", "Es necesario ingresar un cliente para realizar la venta extemporánea ... ");
            else if (this.serviceTypeID === 0)
                return showAlert("¡¡ Necesita ingresar un servicio !!", "error", "Es necesario ingresar un servicio para realizar la venta extemporánea ... ");
            else if (this.startDateInput === "")
                return showAlert("¡¡ Necesita ingresar una fecha !!", "error", "Es necesario ingresar una fecha para realizar la venta extemporánea ... ");
            else {

                preloader('show');
                var date = this.startDateInput.split("/");
                date = date[0] + "/" + date[1] + "/" + date[2];
                let url = this.url + 'SaveUntimelySale';
                let data = {
                    customerVM: this.customerVM,
                    date: date,
                    idServiceType: this.serviceTypeID,
                };
                window.axios.post(url, data).then((response) => {
                    if (response.data.state)
                        showAlert("¡¡ Venta guardada exitosamente !!", "success", "La venta extemporánea se ha guardado exitosamente ... ");

                    else
                        showAlert(response.data.titleError, "error", response.data.message);

                }).catch((error) => {
                    showAlert("Ocurríó un error !!! ", "error", "Intente de nuevo más tarde ... ");
                }).finally((response) => {
                    preloader('hidden');
                });
            }
        },
        getClientsOfInputAutocomplete: function () {
            let url = this.url + 'GetCustomersForInputAutocomplete';
            window.axios.get((url)).then((response) => {
                this.fillInputAutocompleteCustomers(response.data);
            }).catch((error) => {
            }).finally((response) => {
            });
        },
        fillInputAutocompleteCustomers: function (data) {
            var vue = this;
            $(function () {
                $('input.autocompleteCustomers').autocomplete({
                    data: data,
                    onAutocomplete: function (customer) {
                        let numberOfDocument = customer.split(' ').reverse()[0];
                        vue.invocateCustomer(numberOfDocument);
                    },
                    limit: 5, // The max amount of results that can be shown at once. Default: Infinity.
                });
            });
        }
        ,
        //Carga el cliente seleccionado del "select" en el formulario y limpia el input "filterText":
        invocateCustomer: function () {
            this.customerVM = {};
            this.customerName = "";
            this.WorkUnitName = "";

            let url = this.url + "findCustomer";
            preloader('show');
            var data = {
                'dni': this.saleVM.dni,
            };
            window.axios.post(url, data).then((response) => {
                if (response.data.state) {
                    showToast('Usuario encontrado !!!', 'success', 'El usuario ha sido encontrado en nuestra base de datos ...');
                    this.customerVM = response.data.customerVM;
                    this.customerName = this.customerVM.name + ' ' + this.customerVM.fatherLastname + ' ' + this.customerVM.motherLastname;
                    this.WorkUnitName = this.customerVM.WorkUnit.name;
                } else {
                    showAlert(response.data.titleError, 'error', response.data.message);
                }
            }).finally((response) => {
                preloader('hidden');
            });
        }
        ,
        //Carga toda la data de productos al componente tabla:
        fillDataRow: function (response) {
            let dr = [];
            for (let i = 0; i < response.length; i++) {
                let data = [
                    // Field 0
                    {
                        'type': 'text',
                        'data': response[i].idProduct.toString(),
                    },
                    // Field 1
                    {
                        'type': 'text',
                        'data': response[i].name
                    },
                    // Field 2
                    {
                        'type': 'input',
                        'data': '1',
                        'id': i,
                    },
                    // Field 3
                    {
                        'type': 'hidden',
                        'data': 1,
                        'id': i
                    },
                    // Field 4
                    {
                        'type': 'text',
                        'data': response[i].price,
                        'id': i
                    },
                    // Field 5
                    {
                        'type': 'hidden',
                        'data': '0',
                        'id': i
                    },
                    // Field 6
                    {
                        'type': 'text',
                        'data': response[i].price
                    },
                    // Field 7
                    {
                        'type': 'hidden',
                        'data': response[i].grantedPrice,
                    },
                    // Field 8
                    {
                        'type': 'button',
                        'data': "+",
                        'id': i,
                        'po': response[i].idProduct,
                        'class': "btn btn-small btn-primary d-block"
                    },
                    // Field 9
                    {
                        'type': 'hidden',
                        'data': response[i].currentStock,
                    },
                    // Field 10
                    {
                        'type': 'hidden',
                        'data': response[i].grantedPrice,
                    }
                ];
                let typeStock = data[9].data;
                if (typeStock === 'Unidad') {
                    data[3].type = 'text';
                    data[3].data = response[i].weight;
                }
                dr.push(data);
            }
            this.dataRow = dr;
        },
        //Verifica la existencia de un cliente basado en su RUC o DNI y lo carga en el formulario:
        checkDoc: function (event) {
            let url = this.url + "getcustomer";
            //RUC
            if (this.customerTypeDoc === 'ruc') {
                if (this.customerVM.ruc.length === 11) {
                    preloader('show');
                    window.axios.post(url, { ruc: this.customerVM.ruc }).then((response) => {
                        if (response.data) {
                            this.customerVM = response.data;
                        } else {
                            showAlert('Cliente no encontrado, intente de nuevo !', 'error');
                            this.customerVM = {};
                        }
                    }).catch((error) => {
                    }).finally((response) => {
                        preloader('hidden');
                    });
                }
            }
            //DNI
            else {
                if (this.customerVM.dni.length === 8) {
                    preloader('show');
                    window.axios.post(url, { dni: this.customerVM.dni }).then((response) => {
                        if (response.data) {
                            this.customerVM = response.data;
                        } else {
                            showAlert('Cliente no encontrado, intente de nuevo !', 'error');
                            this.customerVM = {};
                        }
                    }).catch((error) => {
                    }).finally((response) => {
                        preloader('hidden');
                    });
                }
            }
        }
        ,
        saveFastSale: function () {
            let url = this.url + "storeFast";

            let data = {
                'saleCreateViewModel': this.saleVM,
                'dni': this.saleVM.dni,
            };
            preloader('show');
            window.axios.post(url, data).then((response) => {
                if (response.data.state) {
                    showAlert('Venta registrada !!! ', 'success', 'Se registró la venta exitosamente ... ');
                } else {
                    showAlert(response.data.titleError, response.data.typeResponse, response.data.message);
                }
            }).catch((error) => {
            }).finally((response) => {
                this.initForm();
                preloader('hidden');
            });
        },
        selectCustomerFast: function () {
            let url = this.url + "GetDNICustomer";

            let document = this.customerNameFast.split(":", 2);

            if (document[1] === undefined)
                return;

            let data = {
                document: document[1],
            };

            window.axios.post(url, data).then((response) => {
                this.saleVM.dni = response.data.dni;
            }).then((response) => {
                this.customerNameFast = "";
                this.$refs.nameTypeaheadA.inputValue = "";
                this.saveFastSale();
            });

        },
        selectCustomerClassic: function () {
            let url = this.url + "GetDNICustomer";

            let document = this.customerNameClassic.split(":", 2);

            if (document[1] === undefined)
                return;

            let data = {
                document: document[1],
            };

            window.axios.post(url, data).then((response) => {
                this.saleVM.dni = response.data.dni;
            }).then((response) => {
                this.customerNameClassic = "";
                this.$refs.nameTypeaheadB.inputValue = "";

                this.invocateCustomer();
            });

        },
        saveSale: function () {
            let url = this.url + "store";
            if (this.saleVM.change < 0 || isNaN(this.saleVM.change) || isNaN(this.saleVM.saleOnCredit)) {
                showAlert('Porfavor, revise los montos !!!', 'error', 'Monto no permitido para esta venta.');
                return;
            }
            if (this.customerVM.idCustomer === 0) {
                showAlert('El cliente requerido !!!', 'error', 'El cliente es necesario para realizar la venta ... ');
                return;
            }

            this.saleVM.idCustomer = this.customerVM.idCustomer;

            let data = {
                'saleCreateViewModel': this.saleVM,
                'items': this.selectData(this.currentSale)
            };
            preloader('show');
            window.axios.post(url, data).then((response) => {
                if (response.data.state) {
                    showAlert('Venta registrada !!! ', 'success', 'Se registró la venta exitosamente ... ');
                } else {
                    showAlert(response.data.titleError, 'error', response.data.message);
                }
            }).catch((error) => {

            }).finally((response) => {
                preloader('hidden');
            });
        },
        selectData: function (currentSale) {
            var detailToSale = [];
            currentSale.forEach(function (detail) {
                var detailVM = {};
                detailVM.idProduct = detail[0].data;
                detailVM.nameProduct = detail[1].data;
                detailVM.quantity = detail[2].data;
                detailVM.weight = detail[3].data;
                detailVM.unitPrice = detail[4].data;
                detailVM.discount = detail[6].data;
                detailVM.unitPrice = detail[7].data;
                detailVM.currentStock = detail[9].data;
                detailVM.grantedPrice = detail[10].data;
                detailToSale.push(detailVM);
            });
            return detailToSale;
        },
        generateVocher: function () {
            window.open(this.url + 'voucher', '_blank');
        },
        cleanData() {
            this.currentSale = [];
            this.getAllData();
        },
    },
    created() {
        // Habilita/Deshabilita los botones de GuardarVenta/GenerarVoucher
        //document.getElementById("voucher").disabled = true;
        //document.getElementById("saveSale").disabled = false;
    },
});
