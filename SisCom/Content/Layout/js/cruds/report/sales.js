import Datepicker from 'vuejs-datepicker';
import { en, es } from 'vuejs-datepicker/dist/locale';
import vuePagination from 'vue-bs-pagination';

var saleReport = new Vue({
    el: '#saleReport',
    components: {
        Datepicker,
        vuePagination
    },
    data: {
        salesToReport: [],
        listPos: [],
        listWorkUnits: [],
        listServiceType: [],
        listCompanies: [],

        emailsSent: [],
        startDateInput: "",
        endDateInput: "",
        startDate: null,
        endDateObject: null,
        startDateObject: null,
        endDate: null,

        employSales: [],
        workerSales: [],
        totalPriceEmploy: 0,
        totalAccountPaymentEmploy: 0,
        totalPriceWorker: 0,
        totalAccountPaymentWorker: 0,

        idPos: 0,
        idServiceType: 0,
        idWorkUnit: 0,
        customerName: "",
        customerCompany: "0",
        typeClient: 0,
        day: "",
        month: "",
        year: "",

        email: "",
        asunto: "",
        name: null,
        limitRegister: "",
        url: $("#baseUrl").val() + 'reports/',
        en: en,
        es: es,

        emailToDestiny: "dsandovalh15@unc.edu.pe",
        subject: "Reporte de entregas",
        Mr: "Nombre de destino",
        message: "Remito el reporte de entregas de las fechas siguientes ",
        list: {
            PageCount: 1
        },
        paginatorList: 1,
        PageSizeList: 10,
        listW: {
            PageCountW: 1
        },
        paginatorListW: 1,
        PageSizeListW: 10,
        newVal: 1,
        oldVal: 1,
        newValW: 1,
        oldValW: 1,
        pageW: 1
    },
    watch: {
        paginatorList: function (newVal, oldVal) {
            this.list.Page = this.filterSales(newVal, this.pageW);
            this.newVal = newVal;
            this.oldVal = oldVal;
        },
        paginatorListW: function (newVal, oldVal) {
            this.listW.PageW = this.pageWorker(newVal, 1);
        }
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            preloader('hidden');
            let url = this.url + "SalesJson";
            preloader("show");
            axios.get(url).then((response) => {
                this.listPos = response.data.listPos;
                this.listServiceType = response.data.listServiceTypes;
                this.listWorkUnits = response.data.listWorkUnits;
                this.listCompanies = response.data.listCompanies;

            }).finally((response) => {
                preloader("hidden");
            });
        },

        sendReportToEmail: function () {
            if (this.startDate == null || this.endDate == null) {
                showAlert("Es necesario una fecha !!!", 'warning', "Por favor revise la fecha de inicio y/o fecha fin... ");
                return;
            } else if (this.startDateObject > this.endDateObject) {
                showAlert("Fechas inválidas !!!", 'warning', "La fecha de inicio es mayor a la fecha fin... ");
                return;
            }

            this.salesToReport = this.salesToReport.concat(this.employSales, this.workerSales);
            let url = this.url + "SendEmail";
            let data = {
                saleToReportViewModels: this.salesToReport,
                emailToDestiny: this.emailToDestiny,
                subject: this.subject,
                Mr: this.Mr,
                body: this.message,
                startDate: this.startDate,
                endDate: this.endDate
            };
            preloader('show');
            axios.post(url, data).then((response) => {
                if (response.data.state) {
                    showAlert("Correo enviado !!!", 'success', 'El correo ha sido enviado satisfactoriamente... ');
                    this.emailsSent.push({ Mr: this.Mr, emailToDestiny: this.emailToDestiny, subject: this.subject, message: this.message });
                } else {
                    showAlert("Correo rechazado!!!", 'error', 'El correo ha sido rechazado, porfavor revise le informacio ingresada... ');
                }
            }).finally((response) => {
                preloader('hidden');
            });
        },
        pageWorker: function (pageW = 1) {
            this.pageW = pageW;
            this.filterSales(this.newVal, this.pageW)
        },

        filterSales: function (page = 1, pageW = this.pageW) {

            if (this.startDate == null || this.endDate == null) {
                showAlert("Es necesario una fecha !!!", 'warning', "Por favor revise la fecha de inicio y/o fecha fin... ");
                return;
            } else if (this.startDateObject > this.endDateObject) {
                showAlert("Fechas inválidas !!!", 'warning', "La fecha de inicio es mayor a la fecha fin... ");
                return;
            }

            let url = this.url + "FastSalesReportJson";

            var idCompanies = [];
            this.listCompanies.forEach(function (item, index) {
                if (item.stateToFilter)
                    idCompanies.push(item.idCompany);
            });

            let data = {
                filters: {
                    initDate: this.startDate,
                    endDate: this.endDate,
                    typeClient: this.typeClient,
                    idWorkUnit: this.idWorkUnit,
                    customerName: this.customerName,
                    idPos: this.idPos,
                    idServiceType: this.idServiceType,
                    customerCompany: this.customerCompany,
                    idCompanies: idCompanies,
                    Page: page,
                    PageSize: this.PageSizeList,
                    PageW: pageW,
                    PageSizeW: this.PageSizeListW
                }
            };

            preloader("show");
            axios.post(url, data).then((response) => {

                this.employSales = response.data.employSales;
                this.workerSales = response.data.workerSales;

                this.totalPriceEmploy = response.data.totalPriceEmploy;
                this.totalAccountPaymentEmploy = response.data.totalAccountPaymentEmploy;

                this.totalPriceWorker = response.data.totalPriceWorker;
                this.totalAccountPaymentWorker = response.data.totalAccountPaymentWorker;

                this.list = response.data.list;
                this.listW = response.data.listW;

            }).finally((response) => {
                preloader("hidden");
            });
        },

        changeStartDate: function (e) {
            this.startDateObject = new Date(e.getYear() + 1900, e.getMonth() + 1, e.getDate());
            this.startDate = (e.getYear() + 1900) + "-" + (e.getMonth() + 1) + "-" + e.getDate();
            this.startDateInput = e.getDate() + "/" + (e.getMonth() + 1) + "/" + (e.getYear() + 1900);
            this.day = e.getDate();
            this.month = (e.getMonth() + 1);
            this.year = (e.getYear() + 1900);

            if (this.endDateObject != null) {
                if (this.endDateObject < this.startDateObject) showAlert("Fechas inválidas !!!", 'error', 'Por favor revise las ambas fechas antes de continuar ...');
            }

        },
        changeEndDate: function (e) {
            this.endDateObject = new Date(e.getYear() + 1900, e.getMonth() + 1, e.getDate());

            if (this.endDateObject >= this.startDateObject) {
                this.endDate = (e.getYear() + 1900) + "-" + (e.getMonth() + 1) + "-" + (e.getDate());
                this.endDateInput = e.getDate() + "/" + (e.getMonth() + 1) + "/" + (e.getYear() + 1900);
                this.newVal = 1;
                this.pageW = 1;
                this.paginatorList = 1
                this.paginatorListW = 1
                this.filterSales(this.newVal, this.pageW);
            } else {
                showAlert("Fechas inválidas !!!", 'error', 'Por favor revise ambas fechas antes de continuar ...');
            }
        },

        cleanInput: function () {
            this.startDateInput = '';
            this.endDateInput = '';
            this.startDate = null;
            this.endDate = null;
            this.name = null;
        },

        isNumber: function (events) {
            var key = window.event ? events.which : events.keyCode;
            if (key < 48 || key > 57) {
                events.preventDefault();
            }
        },
        downloadFile: function () {
            preloader("show");

            var idCompanies = [];
            this.listCompanies.forEach(function (item, index) {
                if (item.stateToFilter)
                    idCompanies.push(item.idCompany);
            });

            let data = {
                filters: {
                    initDate: this.startDate,
                    endDate: this.endDate,
                    typeClient: this.typeClient,
                    idWorkUnit: this.idWorkUnit,
                    idPos: this.idPos,
                    idServiceType: this.idServiceType,
                    idCompanies: idCompanies,
                }
            };

            let url = this.url + "DownLoadFastExcel";

            axios({ url, method: 'POST', data, responseType: 'blob' }).then((response) => {
                let nameFile = response.headers["content-disposition"].split("=").reverse();
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', nameFile[0]);
                document.body.appendChild(link);
                link.click();
            }).catch((errors) => {
                toastr.error("Error al descargar el archivo.", 'Error Inesperado!');
            }).finally((response) => {
                preloader("hidden");
            });

        }
    }
});
