import Datepicker from 'vuejs-datepicker';
import { en, es } from 'vuejs-datepicker/dist/locale';

var saleReport = new Vue({
    el: '#productReport',
    components: {
        Datepicker
    },
    data: {
        salesToReport: [],
        listPos: [],
        emailsSent: [],
        listServiceType: [],
        startDateInput: "",
        endDateInput: "",
        startDate: null,
        endDateObject: null,
        startDateObject: null,
        endDate: null,

        employSales: [],
        workerSales: [],
        totalPriceEmploy: 0,
        totalAccountPaymentEmploy: 0,
        totalPriceWorker: 0,
        totalAccountPaymentWorker: 0,

        idPos: 0,
        idServiceType: 0,
        typeClient: 0,
        day: "",
        month: "",
        year: "",
        email: "",
        asunto: "",
        name: null,
        limitRegister: "",
        url: $("#baseUrl").val() + 'reports/',
        en: en,
        es: es,

        emailToDestiny: "dsandovalh15@unc.edu.pe",
        subject: "Reporte de entregas",
        Mr: "Dennis Edison Sandoval Huaman",
        message: "Remito el reporte de entregas de las fechas siguientes ",

    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            preloader('hidden');
            let url = this.url + "SalesJson"
            preloader("show");
            axios.get(url).then((response) => {
                this.listPos = response.data.listPos;
                this.listServiceType = response.data.listServiceTypes;
            }).finally((response) => {
                preloader("hidden");
            });
        },
        sendReportToEmail: function () {
            if (this.startDate == null || this.endDate == null) {
                showAlert("Es necesario una fecha !!!", 'warning', "Por favor revise la fecha de inicio y/o fecha fin... ");
                return;
            } else if (this.startDateObject > this.endDateObject) {
                showAlert("Fechas inválidas !!!", 'warning', "La fecha de inicio es mayor a la fecha fin... ");
                return;
            }

            this.salesToReport = this.salesToReport.concat(this.employSales, this.workerSales);
            let url = this.url + "SendEmail"
            let data = {
                saleToReportViewModels: this.salesToReport,
                emailToDestiny: this.emailToDestiny,
                subject: this.subject,
                Mr: this.Mr,
                body: this.message,
                startDate: this.startDate,
                endDate: this.endDate,
            };
            preloader('show');
            axios.post(url, data).then((response) => {
                if (response.data.state) {
                    showAlert("Correo enviado !!!", 'success', 'El correo ha sido enviado satisfactoriamente... ');
                    this.emailsSent.push({ Mr: this.Mr, emailToDestiny: this.emailToDestiny, subject: this.subject, message: this.message });
                } else {
                    showAlert("Correo rechazado!!!", 'error', 'El correo ha sido rechazado, porfavor revise le informacio ingresada... ');
                }
            }).finally((response) => {
                preloader('hidden');
            });
        },
        filterSales: function () {
            if (this.startDate == null || this.endDate == null) {
                showAlert("Es necesario una fecha !!!", 'warning', "Por favor revise la fecha de inicio y/o fecha fin... ");
                return;
            } else if (this.startDateObject > this.endDateObject) {
                showAlert("Fechas inválidas !!!", 'warning', "La fecha de inicio es mayor a la fecha fin... ");
                return;
            }
            let url = this.url + "ClassicSalesReportJson"
            let data = {
                classicSaleFilterViewModel: {
                    initDate: this.startDate,
                    endDate: this.endDate,
                    typeClient: this.typeClient,
                    idPos: this.idPos,
                }
            };
            preloader("show");
            axios.post(url, data).then((response) => {

                this.employSales = response.data.employSales;
                this.workerSales = response.data.workerSales;

                this.totalPriceEmploy = response.data.totalPriceEmploy;
                this.totalAccountPaymentEmploy = response.data.totalAccountPaymentEmploy;

                this.totalPriceWorker = response.data.totalPriceWorker;
                this.totalAccountPaymentWorker = response.data.totalAccountPaymentWorker;

            }).finally((response) => {
                preloader("hidden");
            });
        },

        changeStartDate: function (e) {
            this.startDateObject = new Date(e.getYear() + 1900, e.getMonth() + 1, e.getDate());
            this.startDate = (e.getYear() + 1900) + "-" + (e.getMonth() + 1) + "-" + e.getDate();
            this.startDateInput = e.getDate() + "/" + (e.getMonth() + 1) + "/" + (e.getYear() + 1900);
            this.day = e.getDate();
            this.month = (e.getMonth() + 1);
            this.year = (e.getYear() + 1900);

            if (this.endDateObject != null) {
                if (this.endDateObject < this.startDateObject) showAlert("Fechas inválidas !!!", 'error', 'Por favor revise las ambas fechas antes de continuar ...');
            }

            this.disabledInput();
        },
        changeEndDate: function (e) {
            this.endDateObject = new Date(e.getYear() + 1900, e.getMonth() + 1, e.getDate());

            if (this.endDateObject >= this.startDateObject) {
                this.endDate = (e.getYear() + 1900) + "-" + (e.getMonth() + 1) + "-" + (e.getDate());
                this.endDateInput = e.getDate() + "/" + (e.getMonth() + 1) + "/" + (e.getYear() + 1900);
                this.filterSales();
            } else {
                showAlert("Fechas inválidas !!!", 'error', 'Por favor revise ambas fechas antes de continuar ...');
            }
        },

        cleanInput: function () {
            this.startDateInput = '';
            this.endDateInput = '';
            this.startDate = null;
            this.endDate = null;
            this.name = null;
        },
        disabledInput() {
            if (this.startDateInput == "") {
                //document.getElementById("endDate").disabled = true;
            } else {
                //document.getElementById("endDate").disabled = false;
            }
        },
        getClientsOfInputAutocomplete: function () {
            let url = this.url + 'getCustomer';
            window.axios.get((url)).then((response) => {
                this.fillInputAutocompleteCustomers(response.data);
            }).catch((error) => {
            }).finally((response) => {
            });
        },
        fillInputAutocompleteCustomers: function (data) {
            var vue = this;
            $(function () {
                $('input.autocompleteCustomers').autocomplete({
                    data: data,
                    onAutocomplete: function (customer) {
                        let numberOfDocument = customer.split(',')[0];
                        vue.name = numberOfDocument;
                    },
                    limit: 5,
                    // The max amount of results that can be shown at once. Default: Infinity.
                });
            });
        },
        isNumber: function (events) {
            var key = window.event ? events.which : events.keyCode;
            if (key < 48 || key > 57) {
                events.preventDefault();
            }
        },
    },
    created: function () {
        this.disabledInput();
        preloader('hidden');
        this.getClientsOfInputAutocomplete();
    }
});
