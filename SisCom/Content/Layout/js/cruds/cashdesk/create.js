var cashdeskCreate = new Vue({
    el: '#cashdeskCreate',
    data: {
        url: $('#baseUrl').val() + "cashdesk/",
        cashdeskVM: {},
        validations: {},
        errors: false
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            let url = this.url + "createjson";
            preloader('show');
            window.axios.get(url).then((response) => {
                this.cashdeskVM = response.data;
            }).catch((errors) => {
            }).finally((response) => {
                preloader('hidden');
            });
        },
        createCashdesk: function ($opcion) {
            let url = this.url + "store";
            let cashdeskVM = this.cashdeskVM;
            preloader('show');
            window.axios.post(url, cashdeskVM).then((response) => {
                if ($opcion === 1) {
                    disableButtonByID('save');
                    showAlertSetTimeout('La caja ha sido registrada !!! ', 'success', '', this.url);
                } else {
                    this.cleanForm();
                    showAlert('La caja ha sido registrada !!! ', 'success', '');
                }
            }).catch((error) => {
                if (error.response.status === 422) {
                    this.validations = error.response.data.errors;
                    this.errors = true;
                }
            }).finally((response) => {
                preloader('hidden');
            });
        },
        cleanForm: function () {
            this.cashdeskVM.openDate = null;
            this.cashdeskVM.closeDate = null;
            this.cashdeskVM.beginningCash = null;
            this.cashdeskVM.endingCash = null;
            this.cashdeskVM.goal = 0;
        },
    },
});
