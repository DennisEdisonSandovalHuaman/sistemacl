var cashdeskIndex = new Vue({
    el: '#cashdeskIndex',
    data: {
        url: $('#baseUrl').val() + 'cashdesk/',
        cashdesksVM: [],
        cashdeskVMdetail: {},
        detail: true,
        validations: {},
        errors: false,
        existCashdeskOpen: false,
        filterText: "",
        pagesNumber: 0,
        paginate: {},
    },
    computed: {
        isActived: function() {
            return this.paginate.current_page;
        }
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function(page) {
            let url = this.url + 'indexjson/' + this.filterText + '?page=' + page;
            preloader('show');
            window.axios.get(url).then((response) => {
                this.cashdesksVM = response.data.cashdesk;
                this.paginate = response.data.paginate;
                this.findCashdeskOpenInCashdesksVM(this.cashdesksVM);
            }).catch((errors) => {}).finally((response) => {
                this.pagesNumber = pagesNumber(this.paginate, 3);
                preloader('hidden');
            });
        },
        findCashdeskOpenInCashdesksVM: function() {
            let opens = 0;
            this.cashdesksVM.forEach(function(cashdesk) {
                if (cashdesk.state === 'open') {
                    opens++;
                }
            });
            this.existCashdeskOpen = opens > 0;
        },
        getCashdesk: function(idCashdesk, value) {
            this.detail = value;
            let url = this.url + 'detailjson';
            let data = {
                'idCashdesk': idCashdesk
            }
            window.axios.get(url, { params: data }).then((response) => {
                this.cashdeskVMdetail = response.data;
            })
        },
        showAlertToCloseCashdesk: function(idCashdesk) {
            showAlertWithOptions(' ¿ Está seguro ? ', 'warning', '', 'Cerrar caja', 'Cancelar').then((response) => {
                if (response) {
                    this.closeCashdesk(idCashdesk);
                }
            });
        },
        closeCashdesk: function(idCashdesk) {
            let url = this.url + "closeCashdesk/" + idCashdesk;
            preloader('show');
            window.axios.get(url).then((response) => {
                if (response.data) {
                    showAlert('Caja cerrada !!!', 'success');
                    this.initForm();
                }
            }).catch((errors) => {}).finally((response) => {
                preloader('hidden');
            });
        },
        changePage: function(page) {
            this.paginate.current_page = page;
            this.initForm(page);
        }
    },
});