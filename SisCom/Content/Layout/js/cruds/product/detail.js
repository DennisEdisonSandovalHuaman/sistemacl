var productDetail = new Vue({
    el: '#productDetail',
    data: {
        url: $('#baseUrl').val() + "product/",
        productVM: {},
        idProduct: $('#idProduct').val()
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            preloader('show');
            let url = this.url + "detailjson";
            let data = {
                'idProduct' : this.idProduct
            };
            window.axios.get(url, {params: data}).then((response) => {
                this.productVM = response.data;
            }).catch((errors) => {
            }).finally((response) => {
                preloader('hidden');
            });
        }
    },
});
