(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#productVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: { WorkUnit: [] },
			dropdowns: [],
			msjError: ""
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataEdit':
						this.form = response.productViewModel;
						this.dropdowns = response.dropdowns;
						break;
					case 'editProduct':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Producto actualizado', "success", 'El producto se ha actualizado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al guardar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					case 'deleteProduct':
						showAlert(response.Message, 'Notificación de Sistema!');
						location.href = response.urlRedirect;
						break;
					default:
						break;
				}
			},
			getDataEdit: function () {
				let request = {
					Type: 'GET',
					URL: $('#productVue').data('url'),
					Action: "getDataEdit",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			editProduct: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Products/EditProductJson',
					Action: 'editProduct',
					Data: {
						productViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			},
			deleteProduct: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Products/DeleteProductJson',
					Action: 'deleteProduct',
					Data: {
						productViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			},
			resetStock: function (value) {
				var instanceVue = this;
				if (value == 0) {
					this.form.currentStock = 0;
					this.form.minimunStock = 0;
				}
			}
		},
		created() {
			this.getDataEdit();
		}
	});
})();
