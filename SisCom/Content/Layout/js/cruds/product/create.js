(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#productVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			dropdowns: [],
			msjError: ""
		},
		mounted() {
			this.getDataProduct();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurri� un error comun�quese con el admnistrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataProduct':
						this.form = response.productViewModel;
						this.dropdowns = response.dropdowns;
						break;
					case 'createProduct':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Producto registrado', "success", 'El producto se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al guardar, recargar la p�gina o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					default:
						break;
				}
			},
			getDataProduct: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'Products/CreateJson',
					Action: "getDataProduct"
				};
				this.makeRequestServer(request);
			},
			createProduct: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Products/CreateProductJson',
					Action: 'createProduct',
					Data: {
						productViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			},
			resetStock: function (value) {
				var instanceVue = this;
				if (value == 0) {
					this.form.currentStock = 0;
					this.form.minimunStock = 0;
				}
			}
		},
		created() {

		}
	});
})();
