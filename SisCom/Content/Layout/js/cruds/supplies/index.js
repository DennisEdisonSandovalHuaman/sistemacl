import Axios from "axios";
import vuePagination from 'vue-bs-pagination';

(function () {
    'use strict';
    var vueIndex = new Vue({
        el: '#suppliesIndex',
        components: {
			vuePagination
        },
        data: {
            baseURL: $('#baseUrl').val(),
			list: {
				PageCount: 1
			},
			supplyVM: {},
			paginatorList: 1,
			PageSizeList: 10,
			FilterByName: '',
			FilterByBranch: ''
		},
		watch: {
			paginatorList: function (newVal, oldVal) {
				this.list.Page = this.initTables(newVal, 1);
			}
		},
        mounted() {
            this.initTables();
        },
        methods: {
            makeRequestServer: function (request) {
                var instanceVue = this;
                preloader('show');
                $.ajax
                    ({
                        type: request.Type,
                        url: request.URL,
                        content: "application/json; charset=utf-8",
                        dataType: 'json',
                        data: request.Data,
                        success: function (response) {
                            instanceVue.actionRequest(response, request.Action);
                        },
                        error: function (ex, response) {
                            showAlert('Error del sistema!', 'error', 'Ocurrió un error comuníquese con el administrador del sistema.');
                        },
                        complete: function () {
                            preloader('hidden');
                        }
                    });
            },
            actionRequest: function (response, type) {
                switch (type) {
                    case "GetListProducts":
						this.list = response.list;
						FilterByName: '';
						FilterByBranch: '';
                        break;
                }
            },
			initTables: function (page = 1) {
                let request = {
                    Type: 'POST',
                    URL: this.baseURL + "Supplies/IndexJson",
					Action: "GetListProducts",
					Data: {
						filters: {
							Page: page,
							PageSize: this.PageSizeList,
							FilterByName: this.FilterByName,
							FilterByBranch: this.FilterByBranch
						}
					}
                };
                this.makeRequestServer(request);
            },
            confirmationModal: function (idSupplies) {
                showAlertWithOptions('Desactivar insumo !!!', 'warning', 'Desea desactivar el insumo seleccionado ?', 'Desactivar', 'Atras').then((response) => {
                    if (response)
                        this.deleteSupply(idSupplies);
                });
            },
            deleteSupply: function (idSupplies) {
                preloader("show");
                var url = vueIndex.baseURL + "Supplies/Destroy?id=" + idSupplies;
                Axios.get(url).then((response) => {
                    showAlert("Insumo eliminado", 'success', 'El insumo se ha desactivado correctamente ... !!! ');
                    this.initTables();
                }).finally((response) => {
                    preloader("hidden");
                });
            },
            verifyNumber: function (e) {
                var key = window.event ? e.which : e.keyCode;
                if (key < 48 || key > 57) {
                    e.preventDefault();
                }
            },
            bringSupplyByID: function (idSupplies) {
                preloader("show");
                var url = vueIndex.baseURL + "Supplies/BringSupplyByID?id=" + idSupplies;
                Axios.post(url).then((response) => {
                    this.supplyVM = response.data;
                }).finally((response) => {
                    preloader("hidden");
                });
            },
            updateSupply: function () {
                preloader("show");
                var url = vueIndex.baseURL + "Supplies/Update";
                var data = {
                    suppliesViewModel: this.supplyVM
                };
                Axios.post(url, data).then((response) => {
                    if (response.data.state) {
                        this.initTables();
                        showAlert('Insumo actualizado !!!', response.data.typeMessage, response.data.message);
                        $('#modal-control-insume').modal('hide');
                    } else {
                        showAlert('Ocurrió un errror !!!', 'error', 'Ocurrió un error al actualizar, intente de nuevo ...');
                    }
                }).finally((response) => {
                    preloader("hidden");
                });
            }
        }
    });
})();
