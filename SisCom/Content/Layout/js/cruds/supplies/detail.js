var productDetail = new Vue({
    el: '#supplyDetail',
    data: {
        url: $('#baseUrl').val() + "product/",
        productVM: {},
        idProduct: $('#idProduct').val()
    },
    mounted() {
        //this.initForm();
        preloader('hidden');
    },
    methods: {
        initForm: function () {
            preloader('show');
            let url = this.url + "detailjson";
            let data = {
                'idProduct': this.idProduct
            };
            axios.get(url, { params: data }).then((response) => {
                this.productVM = response.data;
            }).catch((errors) => {
            }).finally((response) => {
                preloader('hidden');
            });
        },
    },
});
