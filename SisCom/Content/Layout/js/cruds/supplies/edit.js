(function () {
    'use strict';
    var vuePropuesta = new Vue({
        el: '#supplyEdit',
        components: {

        },
        data: {
            baseURL: $('#baseUrl').val(),
            supplyVM: {},
            branchsVM: [],
            dropdowns: [],
        },
        mounted() {

        },
        methods: {
            makeRequestServer: function (request) {
                var instanceVue = this;
                preloader('show');
                $.ajax
                    ({
                        type: request.Type,
                        url: request.URL,
                        content: 'application/json; charset=utf-8',
                        dataType: 'json',
                        data: request.Data,
                        success: function (response) {
                            instanceVue.actionRequest(response, request.Action);
                        },
                        error: function (ex, response) {
                            showAlert('Error del Sistema', 'error', 'Ocurrió un error comuníquese con el administrador del sistema.');
                        },
                        complete: function () {
                            preloader('hidden');
                        }
                    });
            },
            actionRequest: function (response, type) {
                switch (type) {
                    case 'getDataEdit':
                        this.supplyVM = response.supplyVM;
                        this.branchsVM = response.branchsVM;
                        break;
                    case 'updateSupply':
                        showAlert('Insumo actualizado !!!', response.typeMessage, response.message);
                        break;
                    case 'deleteProduct':
                        showAlert(response.Message, 'Notificación de Sistema!');
                        //location.href = response.urlRedirect;
                        break;
                    default:
                        break;
                }
            },
            getDataEdit: function () {
                let request = {
                    Type: 'GET',
                    URL: $('#supplyEdit').data('url'),
                    Action: "getDataEdit",
                    Data: {
                    }
                };
                this.makeRequestServer(request);
            },
            save: function () {
                var instanceVue = this;
                let request = {
                    Type: 'POST',
                    URL: this.baseURL + 'Supplies/Update',
                    Action: 'updateSupply',
                    Data: {
                        suppliesViewModel: instanceVue.supplyVM,
                    }
                };
                this.makeRequestServer(request);
            },
        },
        created() {
            this.getDataEdit();
        }
    });
})();
