(function () {
    'use strict';
    var vuePropuesta = new Vue({
        el: '#suppliesCreate',
        data: {
            baseURL: $('#baseUrl').val(),
            form: {},
            dropdowns: [],
            supplyVM: {},
            branchsVM: [],
        },
        mounted() {
            this.getDataProduct();
        },
        methods: {
            getDataProduct: function () {
                preloader("show");
                var url = this.baseURL + 'Supplies/CreateJson';
                axios.post(url).then((response) => {
                    this.supplyVM = response.data.supplyVM;
                    this.branchsVM = response.data.branchsVM;
                }).catch((errors) => {
                }).finally((response) => {
                    preloader("hidden");
                });
            },
            storeSupplies: function () {
                preloader("show");
                var url = this.baseURL + 'Supplies/Store';
                this.supplyVM.branch.idBranch = this.supplyVM.idBranch;
                var data = {
                    suppliesViewModel: this.supplyVM,
                };
                axios.post(url, data).then((response) => {
                    if (response.data.state) {
                        showAlert('Insumo registrado', "success", 'El insumo se ha registrado exitosamente ... !!!');
                        $('#save').prop('hidden', true);
                    }
                    else
                        showAlert('Error en la solicitud', 'error', response.data.message);
                }).catch((errors) => {
                }).finally((response) => {
                    preloader("hidden");
                });
            },
            verifyNumber: function (e) {
                var key = window.event ? e.which : e.keyCode;
                if (key == 44)
                    return;
                if (key < 48 || key > 57) {
                    e.preventDefault();
                }
            },
        },
        created() {

        }
    });
})();
