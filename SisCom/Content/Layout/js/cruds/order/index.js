var orderIndex = new Vue({
    el: '#orderIndex',
    data: {
        url: $('#baseUrl').val() + 'order/',
        orderVM: {},
        ordersVM: [],
        paginate: {},
        filterText: "",
        pagesNumber: 0,
    },
    computed: {
        isActived: function() {
            return this.paginate.current_page;
        },
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function(page) {
            let url = this.url + 'indexjson/' + this.filterText + '?page=' + page;
            preloader('show');
            window.axios.get(url).then((response) => {
                this.ordersVM = response.data.order;
                this.paginate = response.data.paginate;
            }).catch((errors) => {}).finally((response) => {
                this.pagesNumber = pagesNumber(this.paginate, 3);
                preloader('hidden');
            })
        },
        edit: function(idOrder) {
            window.location.href = this.url + "edit/" + idOrder;
        },
        detail: function(idOrder) {
            window.location.href = this.url + "detail/" + idOrder;
        },
        changePage: function(page) {
            this.paginate.current_page = page;
            this.initForm(page);
        }
    },
});
