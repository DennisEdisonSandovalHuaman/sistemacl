var orderEdit = new Vue({
    el: '#orderEdit',
    data: {
        url: $('#baseUrl').val() + "order/",
        idOrder: $('#idOrder').val(),
        orderVM: {},
        currentOrder: [],
        columnNames: [],
        columnNamesSale: [],
        dataRow: [],
        tableClass: "table",
        validations: {},
        errors: false
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            this.columnNames = [
                'Código',
                'Descripción',
                'Cantidad',
                'Peso',
                'Precio',
                'Desct %',
                'Stock',
                ''
            ];
            this.columnNamesSale = [
                'Código',
                'Descripción',
                'Cantidad',
                'Peso',
                'Precio',
                'Desct %',
                'Nuevo Stock',
                ''
            ];
            this.getAllData();
        },
        updateRow: function (event) {
            let position = (event.target.attributes.po.value);
            let button = this.dataRow[event.target.id][8];
            let valueOfInput = event.target.value;

            // Evento input descuento
            if (position === '5') {
                if (valueOfInput < 0) {
                    showToast('Descuento no permitido !!! ', 'error');
                    this.dataRow[event.target.id][5].data = 0;
                } else {
                    this.dataRow[event.target.id][5].data = valueOfInput;
                }
            }
            //Evento input cantidad
            else if (position === '2') {
                if (valueOfInput < 0) {
                    showToast('Cantidad no permitido !!! ', 'error');
                    this.dataRow[event.target.id][2].data = 0;
                } else {
                    this.dataRow[event.target.id][2].data = valueOfInput;
                }
            }//Evento input Peso
            else if (position === '3') {
                if (valueOfInput < 0) {
                    showToast('Peso no permitido !!! ', 'error');
                    this.dataRow[event.target.id][3].data = 0;
                } else {
                    this.dataRow[event.target.id][3].data = valueOfInput;
                }
            }
            // Evento input precio
            else if (position === '4') {
                if (valueOfInput < 0) {
                    showToast('Precio no permitido !!! ', 'error');
                    this.dataRow[event.target.id][4].data = 0;
                } else {
                    this.dataRow[event.target.id][4].data = valueOfInput;
                }
            }

        },
        //Agrega el producto a la venta actual:
        addToSale: function (event) {
            if (event.target.localName === "button") {
                let id = event.target.id;
                let idProduct = event.target.attributes.po.value;
                if (isNaN(this.dataRow[id][2].data) || isNaN(this.dataRow[id][3].data) || isNaN(this.dataRow[id][4].data) || isNaN(this.dataRow[id][5].data)) {
                    showToast(" ¡¡¡ Dato incorrecto !!! ", 'warning', 'Por favor, verifique los campo, tiene un valor que no es un número.');
                    return;
                } else if ((this.dataRow[id][2].data <= 0) || (this.dataRow[id][3].data <= 0) || (this.dataRow[id][4].data <= 0) || (this.dataRow[id][5].data < 0)) {
                    showToast(" ¡¡¡ Dato incorrecto !!! ", 'warning', 'Por favor, verifique los datos ingresados.');
                    return;
                }
                let data = [
                    // Codigo 0
                    {
                        'type': 'text',
                        'data': this.dataRow[id][0].data,
                    },
                    // Descripcion 1
                    {
                        'type': 'text',
                        'data': this.dataRow[id][1].data,
                    },
                    // Cantidad 2
                    {
                        'type': 'text',
                        'data': this.dataRow[id][2].data,
                    },
                    // Peso total del producto 3
                    {
                        'type': 'text',
                        'data': this.dataRow[id][3].data,
                    },
                    // Precio 4
                    {
                        'type': 'text',
                        'data': this.dataRow[id][4].data,
                        'class': 'red',
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': event.target.attributes.po.value,
                    },
                    // Descuento 6
                    {
                        'type': 'text',
                        'data': this.dataRow[id][6].data,
                    },
                    // totalPrice 7
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][7].data,
                    },
                    // Stock 8
                    {
                        'type': 'text',
                        'data': parseFloat(this.dataRow[id][8].data) + parseFloat(this.dataRow[id][2].data),
                    },
                    // Boton 9
                    {
                        'type': 'button',
                        'data': "<i class='material-icons'>delete</i>",
                        'id': id,
                        'po': event.target.attributes.po.value,
                        'class': "btn btn-small red d-block"
                    },
                ];
                let found = this.currentOrder.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    data[8].data = parseFloat(data[8].data) - 1;
                    this.currentOrder.splice(found, 1, data);
                } else {
                    this.currentOrder.push(data);
                }
                this.calculateTotalProducts(this.currentOrder);
                this.calculateTotalDiscount(this.currentOrder);
                this.calculateSubTotal(this.currentOrder);
                this.calculateIGV(this.orderVM.subTotal);
                this.calculateTotalPrice(this.orderVM.subTotal, this.orderVM.IGV);
            }
        },
        // Funciones para calcular los campos de OrderVM
        calculateTotalProducts: function (currentOrder) {
            let totalProducts = 0;
            currentOrder.forEach(function (detail) {
                totalProducts += parseFloat(detail[2]['data']);
            });
            this.orderVM.totalProducts = Math.round(totalProducts * 100) / 100;
        },
        calculateTotalDiscount: function (currentOrder) {
            let discount = 0;
            currentOrder.forEach(function (item) {
                discount += item[4]['data'] * (item[6]['data'] / 100);
            });
            this.orderVM.totalDiscount = Math.round(parseFloat(discount) * 100) / 100;
        },
        calculateSubTotal: function (currentOrder) {
            let subTotal = 0;
            currentOrder.forEach(function (item) {
                subTotal += parseFloat(item[4]['data']);
            });
            this.orderVM.subTotal = subTotal - this.orderVM.totalDiscount;
        },
        calculateIGV: function (subTotal) {
            let IGV = Math.round((subTotal * 0.18) * 100) / 100;
            this.orderVM.IGV = IGV;
        },
        calculateTotalPrice: function (subTotal, IGV) {
            this.orderVM.totalPrice = Math.round((parseFloat(subTotal) + parseFloat(IGV)) * 100) / 100;
        },
        // END
        rmvFromSale: function (event) {
            let idProduct = 0;
            if (event.target.localName === "button") {
                idProduct = event.target.attributes.po.value;
            } else if (event.target.localName === "svg") {
                idProduct = event.target.parentElement.attributes.po.value;
            }
            if (idProduct !== 0) {
                let found = this.currentOrder.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.orderVM.subTotal = Math.round((this.orderVM.subTotal - this.currentOrder[found][7].data) * 100) / 100;
                    this.orderVM.IGV = Math.round((this.orderVM.subTotal * 0.18) * 100) / 100;
                    this.orderVM.totalPrice = Math.round((this.orderVM.subTotal + this.orderVM.IGV) * 100) / 100;
                    this.currentOrder.splice(found, 1);
                    this.calculateTotalDiscount(this.currentOrder);
                }
            }
        },
        //Obtiene toda la data necesaria para iniciar una venta
        getAllData: function () {
            preloader('show');
            let url = this.url + "editjson";
            var data = {
                'idOrder': this.idOrder,
            };
            window.axios.get(url, {params: data}).then((response) => {
                let products = response.data.products;
                this.orderVM = response.data.orderVM;
                this.fillDataRow(products);
                this.fillCurrentOrder(response.data.detailsAndProducts);
            }).catch((error) => {
            }).finally((response) => {
                preloader('hidden');
            });
        },
        //Carga los productos de la venta a editar en la tabla de currentOrder:
        fillCurrentOrder: function (detailsVM) {
            let currentOrder = [];
            detailsVM.forEach(function (detail, index) {
                let data = [
                    // Codigo 0
                    {
                        'type': 'text',
                        'data': detail.idProduct.toString(),
                    },
                    // Descripcion 1
                    {
                        'type': 'text',
                        'data': detail.productDescription,
                    },
                    // Cantidad 2
                    {
                        'type': 'text',
                        'data': detail.quantity,
                    },
                    // Peso 3
                    {
                        'type': 'text',
                        'data': detail.weightProduct,
                    },
                    // Precio 4
                    {
                        'type': 'text',
                        'data': detail.price,
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': detail.idProduct.toString(),
                    },
                    // Descuento 6
                    {
                        'type': 'text',
                        'data': detail.unitDiscount,
                    },
                    // Subtotal 7
                    {
                        'type': 'hidden',
                        'data': detail.price * detail.quantity,
                    },
                    // Stock 8
                    {
                        'type': 'text',
                        'data': detail.productStock,
                    },
                    // Boton 9
                    {
                        'type': 'button',
                        'data': "<i class='material-icons'>delete</i>",
                        'id': index,
                        'po': detail.idProduct,
                        'class': "btn btn-small red d-block"
                    },
                    // Field 10 type of unity
                    {
                        'type': 'hidden',
                        'data': detail.typeUnity,
                    },
                ];
                currentOrder.push(data);
            });
            this.currentOrder = currentOrder;
        },
        //Carga toda la data de productos al componente tabla:
        fillDataRow: function (response) {
            let dataRow = [];
            for (let i = 0; i < response.length; i++) {
                let data = [
                    // Field 0 Codigo
                    {
                        'type': 'text',
                        'data': response[i].idProduct.toString()
                    },
                    // Field 1 Description
                    {
                        'type': 'text',
                        'data': response[i].description
                    },
                    // Field 2 Cantidad
                    {
                        'type': 'input',
                        'data': 1,
                        'id': i,
                    },
                    // Field 3 Peso del Product
                    {
                        'type': 'input',
                        'data': response[i].weight,
                        'id': i,
                    },
                    // Field 4 Precio de compra del producto
                    {
                        'type': 'input',
                        'data': response[i].purchasePrice,
                        'id': i,
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': response[i].idProduct.toString(),
                    },
                    // Field 6 Descuento
                    {
                        'type': 'input',
                        'data': 0,
                        'id': i
                    },
                    // Field 7 Precio de compra del producto
                    {
                        'type': 'hidden',
                        'data': response[i].purchasePrice
                    },
                    // Field 8 Stock
                    {
                        'type': 'text',
                        'data': response[i].stock,
                    },
                    // Field 9
                    {
                        'type': 'button',
                        'data': "+",
                        'id': i,
                        'po': response[i].idProduct,
                        'class': "btn btn-small btn-primary d-block"
                    },
                    // Field 10 type of unity
                    {
                        'type': 'hidden',
                        'data': response[i].typeUnity,
                    },
                ];
                let typeStock = data[10].data;
                if (typeStock === 'Unidad') {
                    data[3].type = 'text';
                }
                dataRow.push(data);
            }
            this.dataRow = dataRow;
        },
        updateOrder: function () {
            let url = this.url + "update";
            let data = {
                'orderVM': this.orderVM,
                'items': this.currentOrder
            };
            preloader('show');
            window.axios.put(url, data).then((response) => {
                if (response.data) {
                    showToast('Compra actualizada !!!', 'success');
                    this.resetForm();
                    this.getAllData();
                } else {
                    showAlert('Ocurrió un error, intente de nuevo !!! ', 'error');
                }
            }).catch((error) => {
                this.validations = error.response.data.errors;
                this.validations['supplierName'] = error.response.data.errors['orderVM.supplierName'];
                this.validations['voucherNumber'] = error.response.data.errors['orderVM.voucherNumber'];
                this.errors = true;
            }).finally((response) => {
                preloader('hidden');
            });
        },
        resetForm: function () {
            this.orderVM = {};
            this.currentOrder = [];
        },
    }

});
