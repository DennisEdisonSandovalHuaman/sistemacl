var orderCreate = new Vue({
    el: '#orderCreate',
    data: {
        url: $('#baseUrl').val() + "order/",
        orderVM: {},
        customerVM: {},
        filteredCustomers: [],
        currentOrder: [],
        columnNames: [],
        columnNamesSale: [],
        dataRow: [],
        tableClass: "table",
        validations: {},
        errors: false
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function () {
            this.columnNames = [
                'Código',
                'Descripción',
                'Cantidad',
                'Peso',
                'Precio',
                'Desct %',
                'Stock',
                ''
            ];
            this.columnNamesSale = [
                'Código',
                'Descripción',
                'Cantidad',
                'Peso',
                'Precio',
                'Desct %',
                'Nuevo Stock',
                ''
            ];
            this.getAllData();
        },
        updateRow: function (event) {

            let position = (event.target.attributes.po.value);
            let button = this.dataRow[event.target.id][8];
            let valueOfInput = event.target.value;

            // Evento input descuento
            if (position === '5') {
                if (valueOfInput < 0) {
                    showToast('Descuento no permitido !!! ', 'error');
                    this.dataRow[event.target.id][5].data = 0;
                } else {
                    this.dataRow[event.target.id][5].data = valueOfInput;
                }
            }
            //Evento input cantidad
            else if (position === '2') {
                if (valueOfInput < 0) {
                    showToast('Cantidad no permitido !!! ', 'error');
                    this.dataRow[event.target.id][2].data = 0;
                } else {
                    this.dataRow[event.target.id][2].data = valueOfInput;
                }
            }//Evento input Peso
            else if (position === '3') {
                if (valueOfInput < 0) {
                    showToast('Peso no permitido !!! ', 'error');
                    this.dataRow[event.target.id][3].data = 0;
                } else {
                    this.dataRow[event.target.id][3].data = valueOfInput;
                }
            }
            // Evento input precio
            else if (position === '4') {
                if (valueOfInput < 0) {
                    showToast('Precio no permitido !!! ', 'error');
                    this.dataRow[event.target.id][4].data = 0;
                } else {
                    this.dataRow[event.target.id][4].data = valueOfInput;
                }
            }
        },
        addToSale: function (event) {
            if (event.target.localName === "button") {
                let id = event.target.id;
                let idProduct = event.target.attributes.po.value;
                if (this.dataRow[id][4].data <= 0) {
                    return;
                } else if (isNaN(this.dataRow[id][4].data)) {
                    return;
                }
                let data = [
                    // Codigo 0
                    {
                        'type': 'text',
                        'data': this.dataRow[id][0].data,
                    },
                    // Descripcion 1
                    {
                        'type': 'text',
                        'data': this.dataRow[id][1].data,
                    },
                    // Cantidad 2
                    {
                        'type': 'text',
                        'data': this.dataRow[id][2].data,
                    },
                    // Peso 3
                    {
                        'type': 'text',
                        'data': this.dataRow[id][3].data,
                    },
                    // Precio 4
                    {
                        'type': 'text',
                        'data': this.dataRow[id][4].data,
                    },
                    // IdProduct 5
                    {
                        'type': 'hidden',
                        'data': event.target.attributes.po.value,
                    },
                    // Descuento 6
                    {
                        'type': 'text',
                        'data': this.dataRow[id][5].data,
                    },
                    // totalPrice 7
                    {
                        'type': 'hidden',
                        'data': this.dataRow[id][6].data,
                    },
                    // Stock 9
                    {
                        'type': 'text',
                        'data': parseFloat(this.dataRow[id][7].data) + parseFloat(this.dataRow[id][2].data),
                    },
                    // Boton 8
                    {
                        'type': 'button',
                        'data': "<i class='material-icons'>delete</i>",
                        'id': id,
                        'po': event.target.attributes.po.value,
                        'class': "btn btn-small red d-block"
                    },
                ];
                let found = this.currentOrder.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.currentOrder.splice(found, 1, data);
                } else {
                    this.currentOrder.push(data);
                }
                this.calculateTotalProducts(this.currentOrder);
                this.calculateTotalDiscount(this.currentOrder);
                this.calculateSubTotal(this.currentOrder);
                this.calculateIGV(this.orderVM.subTotal);
                this.calculateTotalPrice(this.orderVM.subTotal, this.orderVM.IGV);
            }
        },

        // Funciones para calcular los campos de OrderVM
        calculateSubTotal: function (currentOrder) {
            let subTotal = 0;
            currentOrder.forEach(function (item) {
                subTotal += parseFloat(item[4]['data']);
            });
            this.orderVM.subTotal = subTotal - this.orderVM.totalDiscount;
        },
        calculateIGV: function (subTotal) {
            let IGV = Math.round((subTotal * 0.18) * 100) / 100;
            this.orderVM.IGV = IGV;
        },
        calculateTotalPrice: function (subTotal, IGV) {
            this.orderVM.totalPrice = Math.round((parseFloat(subTotal) + parseFloat(IGV)) * 100) / 100;
        },
        calculateTotalDiscount: function (currentOrder) {
            let discount = 0;
            currentOrder.forEach(function (item) {
                discount += item[4]['data'] * (item[6]['data'] / 100);
            });
            this.orderVM.totalDiscount = Math.round(parseFloat(discount) * 100) / 100;
        },
        calculateTotalProducts: function (currentOrder) {
            let totalProducts = 0;
            currentOrder.forEach(function (detail) {
                totalProducts += parseFloat(detail[2]['data']);
            });
            this.orderVM.totalProducts = Math.round(totalProducts * 100) / 100;
        },
        // END

        rmvFromSale: function (event) {
            let idProduct = 0;
            if (event.target.localName === "button") {
                idProduct = event.target.attributes.po.value;
            } else if (event.target.localName === "svg") {
                idProduct = event.target.parentElement.attributes.po.value;
            }
            if (idProduct !== 0) {
                let found = this.currentOrder.findIndex(function (element) {
                    return element[5].data === idProduct;
                });
                if (found !== -1) {
                    this.currentOrder.splice(found, 1);
                    this.calculateTotalProducts(this.currentOrder);
                    this.calculateTotalDiscount(this.currentOrder);
                    this.calculateSubTotal(this.currentOrder);
                    this.calculateIGV(this.orderVM.subTotal);
                    this.calculateTotalPrice(this.orderVM.subTotal, this.orderVM.IGV);
                }
            }
        },
        getAllData: function () {
            let url = this.url + "getAllProducts";
            preloader('show');
            window.axios.get(url).then((response) => {
                this.products = response.data['products'];
                this.orderVM = response.data['orderVM'];
                this.fillDataRow(this.products);
            }).catch((error) => {
            }).finally((response) => {
                preloader('hidden');
            })
        },
        //Carga toda la data de productos al componente tabla:
        fillDataRow: function (response) {
            let dr = [];
            for (let i = 0; i < response.length; i++) {
                let data = [
                    // Field 0 Codigo
                    {
                        'type': 'text',
                        'data': response[i].idProduct.toString()
                    },
                    // Field 1 Description
                    {
                        'type': 'text',
                        'data': response[i].description
                    },
                    // Field 2 Cantidad
                    {
                        'type': 'input',
                        'data': 1,
                        'id': i,
                    },
                    // Field 3 Peso del Product
                    {
                        'type': 'input',
                        'data': response[i].weight,
                        'id': i,
                    },
                    // Field 4 Precio de compra del producto
                    {
                        'type': 'input',
                        'data': response[i].purchasePrice,
                        'id': i,
                    },
                    // Field 5 Descuento
                    {
                        'type': 'input',
                        'data': 0,
                        'id': i
                    },
                    // Field 6 Precio de compra del producto * Cantidad
                    {
                        'type': 'hidden',
                        'data': response[i].purchasePrice
                    },
                    // Field 7 Stock
                    {
                        'type': 'text',
                        'data': response[i].stock,
                    },
                    // Field 8
                    {
                        'type': 'button',
                        'data': "+",
                        'id': i,
                        'po': response[i].idProduct,
                        'class': "btn btn-small btn-primary d-block"
                    },
                    // Field 9 type of unity
                    {
                        'type': 'hidden',
                        'data': response[i].typeUnity,
                    },
                ];
                let typeUnity = data[9].data;
                if (typeUnity === 'Unidad') {
                    data[3].type = 'text';
                }
                dr.push(data);
            }
            this.dataRow = dr;
        },
        cleanForm: function () {
            this.customerVM.dni = null;
            this.customerVM.ruc = null;
            this.customerVM.name = null;
            this.customerVM.nameCorp = null;
            this.customerVM.address = null;
            this.customerVM.phone = null;
            this.customerVM.email = null;
            this.typeDoc = !this.typeDoc;
        },
        saveOrder: function () {
            preloader('show');
            let url = this.url + "store";
            let data = {
                'orderVM': this.orderVM,
                'items': this.currentOrder
            };
            window.axios.post(url, data).then((response) => {
                if (response.data) {
                    disableButtonByID('saveorder');
                    showAlertSetTimeout('Compra registrada !!! ', 'success', '', this.url);
                } else {
                    showToast('Ocurrió un error, intente de nuevo !!!', 'error')
                }
            }).catch((error) => {
                if (error.response.status === 422) {
                    showAlert('Necesita corregir los datos !!! ', 'warning', 'Por favor, revise los datos ingresados.');
                    this.validations = error.response.data.errors;
                    this.validations['supplierName'] = error.response.data.errors['orderVM.supplierName'];
                    this.validations['voucherNumber'] = error.response.data.errors['orderVM.voucherNumber'];
                    this.errors = true;
                }
            }).finally((response) => {
                preloader('hidden');
            });
        },
        resetForm: function () {
            this.orderVM = {};
            this.customerVM = {};
            this.currentOrder = [];
            this.validations = {};
        },
    }

});
