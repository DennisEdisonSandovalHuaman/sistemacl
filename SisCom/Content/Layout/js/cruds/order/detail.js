var orderDetail = new Vue({
    el: '#orderDetail',
    data: {
        url: $('#baseUrl').val() + 'order/',
        idOrder: $('#idOrder').val(),
        orderVM: {},
        details: [],
    },
    mounted: function() {
        this.initForm();
    },
    methods: {
        initForm: function() {
            preloader('show');
            let url = this.url + "detailjson";
            var data = {
                'idOrder': this.idOrder
            };
            window.axios.get(url, { params: data }).then((response) => {
                this.orderVM = response.data.orderVM;
                this.details = response.data.details;
            }).catch((errors) => {}).finally((response) => {
                preloader('hidden');
            });
        },
    }
});