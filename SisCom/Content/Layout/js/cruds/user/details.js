var userDetail = new Vue({
    el: '#userDetail',
    data: {
        url: $('#baseUrl').val() + "user/",
        userVM: {},
        idUser: $('#idUser').val()
    },
    mounted() {
        this.initForm();
    },
    methods: {
        initForm: function() {
            preloader('show');
            let url = this.url + "detailsjson";
            let data = {
                'idUser': this.idUser
            }
            window.axios.get(url, { params: data }).then((response) => {
                this.userVM = response.data;
            }).catch((errors) => {}).finally((response) => {
                preloader('hidden')
            });
        }
    },
});