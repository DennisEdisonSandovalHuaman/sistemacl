(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#userVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			dropdowns: [],
			msjError: ""
		},
		mounted() {
			this.getDataUser();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');		
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurri� un error comun�quese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataUser':
						this.form = response.usersViewModel;
						this.dropdowns = response.dropdowns;
						break;
					case 'createUser':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Usuario registrado', "success", 'El usuario se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al actualizar, recargar la p�gina o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					default:
						break;
				}
			},
			getDataUser: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'User/CreateJson',
					Action: "getDataUser"
				};
				this.makeRequestServer(request);
			},
			createUser: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'User/CreateUserJson',
					Action: 'createUser',
					Data: {
						usersViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
