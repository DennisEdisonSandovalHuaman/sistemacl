﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#companyVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			msjError: ""
		},
		mounted() {
			this.getDataCompany();
		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
							preloader('hidden');
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataCompany':
						this.form = response.companyViewModel;
						break;
					case 'createCompany':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Empresa registrada', "success", 'La empresa se ha registrado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al actualizar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					default:
						break;
				}
			},
			getDataCompany: function () {
				let request = {
					Type: 'GET',
					URL: this.baseURL + 'Company/CreateJson',
					Action: "getDataCompany"
				};
				this.makeRequestServer(request);
			},
			createCompany: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Company/CreateCompanyJson',
					Action: 'createCompany',
					Data: {
						companyViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {

		}
	});
})();
