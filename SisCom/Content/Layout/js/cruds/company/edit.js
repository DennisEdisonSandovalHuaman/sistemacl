﻿﻿(function () {
	'use strict';
	var vuePropuesta = new Vue({
		el: '#companyVue',
		components: {

		},
		data: {
			baseURL: $('#baseUrl').val(),
			form: {},
			msjError: ""
		},
		mounted() {

		},
		methods: {
			makeRequestServer: function (request) {
				var instanceVue = this;
				preloader('show');
				$.ajax
					({
						type: request.Type,
						url: request.URL,
						content: 'application/json; charset=utf-8',
						dataType: 'json',
						data: request.Data,
						success: function (response) {
							instanceVue.actionRequest(response, request.Action);
						},
						error: function (ex, response) {
							showAlert('Ocurrió un error comuníquese con el administrador del sistema.', 'Error del Sistema!');
						},
						complete: function () {
							preloader('hidden');
						}
					});
			},
			actionRequest: function (response, type) {
				switch (type) {
					case 'getDataEdit':
						this.form = response.companyViewModel;
						break;
					case 'editCompany':
						if (response.formSuccess) {
							if (response.state) {
								showAlert('Unidad de trabajo actualizado', "success", 'La unidad de trabajo se ha actualizado exitosamente');
								this.msjError = "";
								location.href = response.urlRedirect;
							} else {
								showAlert('Error en la solicitud', 'error', "Error al actualizar, recargar la página o comunicarse con el administrador del sistema");
							}
						} else {
							this.msjError = response.msjError
						}
						break;
					case 'deleteCompany':
						showAlert(response.Message, 'Notificación de Sistema!');
						location.href = response.urlRedirect;
						break;
					default:
						break;
				}
			},
			getDataEdit: function () {
				let request = {
					Type: 'GET',
					URL: $('#companyVue').data('url'),
					Action: "getDataEdit",
					Data: {

					}
				};
				this.makeRequestServer(request);
			},
			editCompany: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Company/EditCompanyJson',
					Action: 'editCompany',
					Data: {
						companyViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			},
			deleteCompany: function () {
				var instanceVue = this;
				let request = {
					Type: 'POST',
					URL: this.baseURL + 'Company/DeleteCompanyJson',
					Action: 'deleteCompany',
					Data: {
						companyViewModel: instanceVue.form
					}
				};
				this.makeRequestServer(request);
			}
		},
		created() {
			this.getDataEdit();
		}
	});
})();
